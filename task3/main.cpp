#include <vector>
#include <iostream>
#include "expression/C.h"
#include "expression/V.h"
#include "expression/Expr.h"
#include "operator/operators.h"
#include "operator/integrate.h"
#include <assert.h>

using namespace std;

int main()
{
    double val = (V() + V() + C(5)).eval(2);
    cout << "x + x + 5 when x = 2: " << val << endl;
    assert(val == 9);
    val = (V() + V() - C(5)).eval(2);
    cout << "x + x - 5 when x = 2: " <<  val << endl;
    assert(val == -1);
    val = ((C(7) - C(5)) * V()).eval(2);
    cout << "(7 - 5) * x when x = 2: " << val << endl;
    assert(val == 4);
    val = (V() * V() - C(5)).eval(2);
    cout << "(x * x) - 5 when x = 2: " << val << endl;
    assert(val == -1);
    val = (V() / V() - C(1)).eval(2);
    cout << "(x / x) - 1 when x = 2: " << val <<  endl;
    assert(val == 0);
    val = (((V() * V()) + C(2)*V() - C(3))).eval(1);
    cout << "(x * x) - 1 when x = 2: " << val << endl;
    assert(val == 0);

    double integ = integrate(((V() * V()) + C(2)*V() - C(3)),1,2,10000);
    cout << "Integrating x^2+2x-3 over 1<x<2: " << integ << endl;

    integ = integrate(((V() * V()) + V() - C(5)),1,2,100);
    cout << "Integrating x^2+x-5 over 1<x<2: " << integ << endl;

    return 0;
}