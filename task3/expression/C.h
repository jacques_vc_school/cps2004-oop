#pragma once

// class for constants
class C {
    double c;
public:
    explicit C(double d) : c(d) {}
    double eval(double) { return c; }
};
