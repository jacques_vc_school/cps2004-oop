#pragma once

// General expression class
template <class L, class BinOp, class R>
class Expr {
    L left; R right;
public:
    Expr( L l, R r) : left(l), right(r) {};
    double eval(double i)
    {
        return BinOp::apply(left.eval(i), right.eval(i));
    }
};