#pragma once

// method to calculate integral of an expresion
template <class Expresion>
inline double integrate (Expresion e, double from, double to, int n)
{
    // calculate step
    double s = (to-from)/n;

    // Calculating [0.5 * (y(from)+y(to)))]
    double integ = 0.5*(e.eval(from)+e.eval(to));

    // calculating f(from + step * n) for n from 1 to n-1
    for (int i = 1; i < n; i++)
        integ += e.eval(from+(s*i));

    // return integral * step
    return s*integ;
};