#pragma once

// class which performs the multiplication
class Multiply {
public:
    inline static double apply(double a, double b);
};
