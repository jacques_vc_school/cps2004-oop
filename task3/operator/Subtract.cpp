#include "Subtract.h"

inline double Subtract::apply(double a, double b)
{
    return (a - b);
}