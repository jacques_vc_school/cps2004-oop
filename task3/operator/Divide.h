#pragma once

// class which performs the division
class Divide {
public:
    inline static double apply(double a, double b);
};

