#pragma once

// class which performs the subtraction
class Subtract {
public:
    inline static double apply(double a, double b);
};