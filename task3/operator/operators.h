#include "Plus.cpp"
#include "Multiply.cpp"
#include "Divide.cpp"
#include "Subtract.cpp"
#include "../expression/Expr.h"

// Multiply operator
template <class L, class R>
Expr<L, Multiply, R> operator*(const L l, const R r)
{
    return Expr<L, Multiply, R>(l, r);
};


// Subtraction operator
template <class L, class R>
Expr<L, Subtract, R> operator-(const L l, const R r)
{
    return Expr<L, Subtract, R>(l, r);
};

// Divide operator
template <class L, class R>
Expr<L, Divide, R> operator/(const L l, const R r)
{
    return Expr<L, Divide, R>(l, r);
};

// Addition operator
template <class L, class R>
Expr<L, Plus, R> operator+(const L l, const R r)
{
    return Expr<L, Plus, R>(l, r);
};
