cd src/main/java
find -name "*.java" > sourceFiles.txt
javac @sourceFiles.txt
find -name "*.class" > sourceClass.txt
jar -cvfm ../../../Task2.jar META-INF/MANIFEST.MF @sourceClass.txt
jar -cvf ../../test/java/Testjar.jar @sourceClass.txt
rm sourceClass.txt
rm sourceFiles.txt
cd ../..
find . -name '*.class' -exec rm -f {} \;

