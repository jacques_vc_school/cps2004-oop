package user;

abstract class GeneralUser {
    public String name;
    protected String email;
    protected String password;

    //empty constructor
    public GeneralUser()
    {
        this.name = "";
        this.email = "";
        this.password = "";
    }

    //constructor
    public GeneralUser(String name, String email, String password)
    {
        this.name = name;
        this.email = email;
        this.password = password;
    }

    /**
     * Method to authenticate for login
     * @param email email to check
     * @param password password to check
     * @return true if successful
     */
    public boolean authenticate(String email, String password)
    {
        //if they match
        return this.email.equals(email) && this.password.equals(password);
    }

    /**
     * Method to chaneg password
     * @param password password to check
     */
    public void changePassword(String password)
    {
        this.password = password;
    }
}
