package user;

import exceptions.MoreThanBalanceException;
import order.SellOrder;

import java.time.LocalDate;
import java.util.*;

public abstract class User extends GeneralUser{
    private String id;
    LocalDate dob;
    private boolean approved;
    protected HashMap<String, Double> balances= new HashMap<String, Double>();
    protected HashMap<String, Double> sellOrdersBalances= new HashMap<String, Double>();
    protected HashMap<String, SellOrder> sellOrders = new HashMap<String, SellOrder>();

    //empty constructor
    public User()
    {
        this.name = "";
        this.email = "";
        this.password = "";
        this.dob = null;
        this.id = null;
        this.approved = false;
    }

    //constructor
    public User(String name, String email, String password, LocalDate dob)
    {
        this.name = name;
        this.email = email;
        this.password = password;
        this.dob = dob;
        this.approved = false;
        this.id = UUID.randomUUID().toString();
    }

    /**
     * Method to top up
     * @param value value to put
     */
    public void topUp(double value)
    {
        balances.put("Euro", value);
    }

    /**
     * Getter for approved field
     * @return get approved
     */
    public boolean getApproved()
    {
        return this.approved;
    }

    /**
     * Setter for approved field
     * @param approve approve flag to set
     */
    public void setApproved(boolean approve)
    {
        this.approved = approve;
    }

    /**
     * Method to add sell order
     * @param sellOrder order to add
     */
    public void addSellOrder(SellOrder sellOrder)
    {
        sellOrders.put(sellOrder.getId(), sellOrder);
    }

    /** Method to get sell orders - USED FOR TESTING ONLY
     * @return sellOrders
     */
    public HashMap<String, SellOrder> getSellOrders()
    {
        return sellOrders;
    }

    /**
     * Method to get sell orders in an unmodifiable map
     * @return an unmodifiable map
     */
    public Map<String, SellOrder> getSellOrdersUnmodifiable()
    {
        return Collections.unmodifiableMap(sellOrders);
    }

    /**
     * Method to get id
     * @return id
     */
    public String getId()
    {
        return id;
    }

    /**
     * Method to get id
     * @return id
     */
    public String getEmail()
    {
        return email;
    }

    /**
     * Method to get balances
     * @return balances
     */
    public HashMap<String, Double> getBalances()
    {
        return balances;
    }

    /**
     * Method to get balances in an unmodifiable map
     * @return an unmodifiable map
     */
    public Map<String, Double> getBalancesUnmodifiable()
    {
        return Collections.unmodifiableMap(balances);
    }

    /**
     * Method to get sell order balances
     * @return balances
     */
    public HashMap<String, Double> getSellOrdersBalances()
    {
        return sellOrdersBalances;
    }

    /**
     * Method to get sell order balances in an unmodifiable map
     * @return an unmodifiable map
     */
    public Map<String, Double> getSellOrdersBalancesUnmodifiable()
    {
        return Collections.unmodifiableMap(sellOrdersBalances);
    }

    /**
     * Method to update balance
     * @param security security to update
     * @param newValue new value
     */
    public void updateBalance(String security, Double newValue)
    {
        if(newValue == 0)
            balances.remove(security);
        else
            balances.put(security, newValue);
    }

    /**
     * Method to update balance
     * @param security security to update
     * @param newValue new value
     */
    public void updateSellOrderBalances(String security, Double newValue)
    {
        if(newValue == 0)
            sellOrdersBalances.remove(security);
        else
            sellOrdersBalances.put(security, newValue);
    }

    /**
     * Method to set password
     * @param password new password
     */
    public void setPassword(String password)
    {
        this.password = password;
    }

    /**
     * Method to set id
     */
    public void setId()
    {
        this.id = UUID.randomUUID().toString();
    }

    /**
     * Method to reduce euro balance
     * @param value vlaue to reduce
     * @throws MoreThanBalanceException
     */
    public void reduceEuroBalance(double value) throws MoreThanBalanceException {

        double currentVal = 0;
        if(balances.containsKey("Euro"))
            currentVal = balances.get("Euro");

        if(value > currentVal)
            throw new MoreThanBalanceException("You cannot reduce more than available balance!");
        else
        {
            balances.put("Euro", currentVal - value);
        }
    }

    /**
     * Method to increment balance
     * @param security security to increment
     * @param newValue new value to add
     */
    public void incrementSellOrderBalances(String security, Double newValue)
    {
        if(sellOrdersBalances.containsKey(security))
            sellOrdersBalances.put(security, sellOrdersBalances.get(security) + newValue);
        else
        {
            if (newValue != 0)
                sellOrdersBalances.put(security, newValue);
        }
    }

    /**
     * Method to reduce balance
     * @param security security to reducd
     * @param value value to reduce
     * @throws MoreThanBalanceException when the value to reduce is more than the current balance
     */
    public void reduceBalance(String security, double value) throws MoreThanBalanceException {
        double currentVal = 0;
        if(balances.containsKey(security))
            currentVal = balances.get(security);

        if(value > currentVal)
            throw new MoreThanBalanceException("You cannot reduce more than available balance!");
        else
        {
            //if to reduce all amount, remove the entry
            if(value == currentVal)
                balances.remove(security);
            else
                balances.put(security, currentVal - value);

        }
    }

    /**
     * Method to increment euro balance
     * @param value to increment
     */
    public void incrementEuroBalance(double value){

        if(balances.containsKey("Euro"))
        {
            double currentVal = balances.get("Euro");
            balances.put("Euro", currentVal + value);
        }
        else
        {
            balances.put("Euro", value);
        }
    }

    /**
     * Method to increment balance
     * @param security security to increment
     * @param newValue new value to add
     */
    public void incrementBalance(String security, Double newValue)
    {

        if(balances.containsKey(security))
            balances.put(security, balances.get(security) + newValue);
        else
            balances.put(security, newValue);
    }

    /**
     * Method to get idle balances
     * @return balances
     */
    public HashMap<String, Double> getIdleBalances()
    {
        return balances;
    }

    /**
     * Method to get idle balances in an unmodifiable map
     * @return an unmodifiable map
     */
    public Map<String, Double> getIdleBalancesUnmodifiable()
    {
        return Collections.unmodifiableMap(balances);
    }

    /**
     * Method to show idle balances
     * @return a list of security names in the user's balances
     */
    public ArrayList<String> showIdleBalances()
    {

        ArrayList<String> securities = new ArrayList<>();

        //if there is another balance apart from Euro one
        if(balances.size() > 1)
        {
            String format = "%-30s%-30s%n";
            System.out.printf(format, "Security", "Quantity Available");
            for (Map.Entry entry : balances.entrySet()) {
                if(entry.getKey() != "Euro")
                {
                    System.out.printf(format, entry.getKey(), entry.getValue());
                    securities.add((String) entry.getKey());
                }
            }
        }
        return securities;
    }
}

