package user;

import order.SellOrder;
import order.Status;
import security.Security;

import java.time.LocalDate;
import java.util.*;

public class Lister extends User{
    // a list with all the listed securities of a lister
    private List<Security> listedSecurities = new ArrayList<>();

    //empty constructor
    public Lister() {
        super();
    }

    //constructor
    public Lister(String name, String email, String password, LocalDate dob)
    {
        super(name, email, password, dob);
    }

    /**
     * Method to list a security
     * @param security security to list
     */
    public void listSecurity(Security security)
    {
        //Add security to Lister's list
        listedSecurities.add(security);
        //add balance
        sellOrdersBalances.put(security.getName(), (double) security.getTotalSupply());
    }

    /**
     * Getter for listedsecurities - just for testing
     */
    public List<Security> getListedSecurities(){
        return listedSecurities;
    }

    /**
     * Method to get listed securities in an unmodifiable list
     * @return an unmodifiable list
     */
    public List<Security> getListedSecuritiesUnmodifiable()
    {
        return Collections.unmodifiableList(listedSecurities);
    }

    /**
     * Method to update a security
     * @param security security to update
     * @param amount new amount
     * @param price new price
     * @return true if successful, false if not
     */
    public boolean updateSecurity(String security, int amount, double price)
    {
        //flag for change
        boolean changed = false;

        //update security
        for(int i=0; i < listedSecurities.size(); i++)
        {
            Security currentSecurity = listedSecurities.get(i);
            if(currentSecurity.getName().equals(security))
            {
                currentSecurity.setPrice(price);
                currentSecurity.incrementSupply(amount);
                changed = true;

                listedSecurities.set(i, currentSecurity);
                break;
            }
        }

        if(!changed)
            return false;

        if(balances.containsKey(security))
        {
            double currentVal = balances.get(security);
            currentVal+= amount;
            balances.put(security, currentVal);
        }
        else
            balances.put(security, (double)amount);



        return changed;
    }

    /**
     * Method to view orders
     */
    public void viewMyOrders()
    {
        if(sellOrders.size()!= 0)
        {
            System.out.println("---------Sell Orders---------");
            String format = "%-50s%-30s%-30s%-30s%-30s%-30s%-30s%n";
            System.out.printf(format, "Id", "Security", "Price", "Quantity", "Quantity Available", "Status", "Timestamp");

            for (Map.Entry entry : sellOrders.entrySet()) {
                SellOrder currentOrder = (SellOrder) entry.getValue();
                System.out.printf(format, currentOrder.getId(), currentOrder.getSecurity(), currentOrder.getPrice(), currentOrder.getQuantity(), currentOrder.getQuantityAvailable(), currentOrder.getStatus(), currentOrder.getTimestamp().toLocalDateTime().toString());

            }
        }
        else
            System.out.println("No orders found!");

    }

    /**
     * Method to print user's listed securities
     * @return the number of listed securities
     */
    public int viewMyListedSecurities()
    {
        if(listedSecurities.size() == 0)
        {
            System.out.println("You have no listed securities");
        }

        else
        {
            String format = "%-30s%-30s%-30s%-50s%n";
            System.out.printf(format, "Name", "Price", "Total Supply", "Desc");
            for(int i=0; i < listedSecurities.size(); i++)
            {
                Security currentSecurity = listedSecurities.get(i);
                System.out.printf(format, currentSecurity.getName(), currentSecurity.getPrice(), currentSecurity.getTotalSupply(), currentSecurity.getDesc());
            }
        }

        return listedSecurities.size();
    }

    /**
     * Method to print user's balances
     */
    public void viewMyBalances()
    {
        //if balances size is 0
        if(this.balances.size() == 0)
        {
            System.out.println("You have no idle balances");
        }
        else
        {
            //format balances and print them
            System.out.println("----------Idle Balances----------");
            String format = "%-30s%-30s%n";
            System.out.printf(format, "Name", "Quantity");
            this.balances.entrySet().forEach(entry->{
                System.out.printf(format, entry.getKey(), entry.getValue());
            });
        }

        //if balances size is 0
        if(this.getSellOrdersBalances().size() != 0)
        {
            //format balances and print them
            System.out.println("----------Sell Order Balances----------");
            String format = "%-30s%-30s%n";
            System.out.printf(format, "Name", "Quantity");
            sellOrdersBalances.entrySet().forEach(entry->{
                System.out.printf(format, entry.getKey(), entry.getValue());
            });
        }
    }

    /**
     * Method to cancel order
     * @param id id to cancel
     */
    public void cancelOrder(String id)
    {
        //set status to cancelled
        SellOrder original = sellOrders.get(id);
        original.setStatus(Status.CANCELLED);
        sellOrders.put(id, original);

        //calculate amount to restore
        double restoreAmt = original.getQuantityAvailable();

        //if balances contain the security
        if(balances.containsKey(original.getSecurity()))
        {

            //get security balance
            Double oldAmt = balances.get(original.getSecurity());
            //add amount in balance
            oldAmt += restoreAmt;
            //update balance
            balances.put(original.getSecurity(), oldAmt);


        }
        //if it does not exist just add the balance
        else
            balances.put(original.getSecurity(), original.getQuantityAvailable());


        //get security sell order balance
        Double sellOrderSecurityBalance = sellOrdersBalances.get(original.getSecurity());
        //if security sell order balance is equal to the amount to remove, remove the Euro entry
        if(sellOrderSecurityBalance == restoreAmt)
            sellOrdersBalances.remove(original.getSecurity());
            //else subtract the value
        else
        {
            sellOrderSecurityBalance -= restoreAmt;
            sellOrdersBalances.put(original.getSecurity(), sellOrderSecurityBalance);
        }
    }

    /**
     * Method to show active orders
     * @return number of available orders
     */
    public int showActiveOrders()
    {

        //format for display
        String format = "%-50s%-30s%-30s%-30s%-30s%-30s%-30s%n";
        boolean activeOrders = false;
        int counter = 0;

        for (Map.Entry entry : sellOrders.entrySet()) {
            SellOrder currentOrder = (SellOrder) entry.getValue();

            if(currentOrder.getStatus() == Status.IN_PROGRESS)
            {
                //if first
                if(counter == 0)
                {
                    System.out.println("---------Active Orders---------");
                    System.out.printf(format, "Id", "Security", "Price", "Quantity", "Quantity Available", "Status", "Timestamp");
                }
                System.out.printf(format, currentOrder.getId(), currentOrder.getSecurity(), currentOrder.getPrice(), currentOrder.getQuantity(), currentOrder.getQuantityAvailable(), currentOrder.getStatus(), currentOrder.getTimestamp().toLocalDateTime().toString());
                counter++;
                activeOrders = true;
            }

        }
        if(!activeOrders)
        {
            System.out.println("No active orders available!");
        }
        return counter;

    }

}
