package user;

import order.BuyOrder;
import order.SellOrder;
import order.Status;

import java.time.LocalDate;
import java.util.*;

public class Trader extends User{


    private HashMap<String, BuyOrder> buyOrders = new HashMap<>();

    protected HashMap<String, Double> buyOrdersBalances= new HashMap<>();
    public Trader() {
        super();
    }

    public Trader(String name, String email, String password, LocalDate dob)
    {
        super(name, email, password, dob);
    }

    /**
     * Method to add buy order
     * @param buyOrder order to add
     */
    public void addBuyOrder(BuyOrder buyOrder)
    {
        buyOrders.put(buyOrder.getId(), buyOrder);
    }

    /** Method to get buy orders - USED FOR TESTING ONLY
     * @return sellOrders
     */
    public HashMap<String, BuyOrder> getBuyOrders()
    {
        return buyOrders;
    }

    /**
     * Method to get buy orders in an unmodifiable map
     * @return an unmodifiable map
     */
    public Map<String, BuyOrder> getBuyOrdersUnmodifiable()
    {
        return Collections.unmodifiableMap(buyOrders);
    }

    /**
     * Method to view orders
     */
    public void viewMyOrders()
    {
        if(buyOrders.size() == 0 && sellOrders.size() == 0)
            System.out.println("No orders found!");
        else
        {
            if(buyOrders.size() != 0)
            {
                System.out.println("---------Buy Orders---------");
                String format = "%-50s%-30s%-30s%-30s%-30s%-30s%-30s%n";
                System.out.printf(format, "Id", "Security", "Price", "Quantity", "Quantity Available", "Status", "Timestamp");

                for (Map.Entry entry : buyOrders.entrySet()) {
                    BuyOrder currentOrder = (BuyOrder) entry.getValue();
                    System.out.printf(format, currentOrder.getId(), currentOrder.getSecurity(), currentOrder.getPrice(), currentOrder.getQuantity(), currentOrder.getQuantityAvailable(), currentOrder.getStatus(), currentOrder.getTimestamp().toLocalDateTime().toString());

                }
            }
            if(sellOrders.size() != 0)
            {
                System.out.println("---------Sell Orders---------");
                String format = "%-50s%-30s%-30s%-30s%-30s%-30s%-30s%n";
                System.out.printf(format, "Id", "Security", "Price", "Quantity", "Quantity Available", "Status", "Timestamp");

                for (Map.Entry entry : sellOrders.entrySet()) {
                    SellOrder currentOrder = (SellOrder) entry.getValue();
                    System.out.printf(format, currentOrder.getId(), currentOrder.getSecurity(), currentOrder.getPrice(), currentOrder.getQuantity(), currentOrder.getQuantityAvailable(), currentOrder.getStatus(), currentOrder.getTimestamp().toLocalDateTime().toString());

                }
            }
        }

    }

    /**
     * Method to show active orders
     * @param type: 1 if BuyOrder, 2 if SellOrder
     * @return number of available orders
     */
    public int showActiveOrders(int type)
    {

        //format for display
        String format = "%-50s%-30s%-30s%-30s%-30s%-30s%-30s%n";
        boolean activeOrders = false;
        int counter =0;
        //if buy orders
        if(type == 1)
        {
            //if no orders
            if(buyOrders.size() < 1)
            {
                System.out.println("No active orders!");
            }
            else
            {
                for (Map.Entry entry : buyOrders.entrySet()) {

                    BuyOrder currentOrder = (BuyOrder) entry.getValue();
                    if(currentOrder.getStatus() == Status.IN_PROGRESS)
                    {
                        //if first
                        if(counter == 0)
                        {
                            System.out.println("---------Active Orders---------");
                            System.out.printf(format, "Id", "Security", "Price", "Quantity", "Quantity Available", "Status", "Timestamp");
                        }
                        System.out.printf(format, currentOrder.getId(), currentOrder.getSecurity(), currentOrder.getPrice(), currentOrder.getQuantity(), currentOrder.getQuantityAvailable(), currentOrder.getStatus(), currentOrder.getTimestamp().toLocalDateTime().toString());
                        counter++;
                        activeOrders = true;
                    }

                }

                if(!activeOrders)
                {
                    System.out.println("No active orders available!");
                }

            }
            return counter;
        }
        //if sell orders
        else if(type ==2 )
        {
            //if no orders
            if(sellOrders.size() < 1)
            {
                System.out.println("No active orders!");
            }
            else
            {

                for (Map.Entry entry : sellOrders.entrySet()) {
                    SellOrder currentOrder = (SellOrder) entry.getValue();

                    if(currentOrder.getStatus() == Status.IN_PROGRESS)
                    {
                        //if first
                        if(counter == 0)
                        {
                            System.out.println("---------Active Orders---------");
                            System.out.printf(format, "Id", "Security", "Price", "Quantity", "Quantity Available", "Status", "Timestamp");
                        }
                        System.out.printf(format, currentOrder.getId(), currentOrder.getSecurity(), currentOrder.getPrice(), currentOrder.getQuantity(), currentOrder.getQuantityAvailable(), currentOrder.getStatus(), currentOrder.getTimestamp().toLocalDateTime().toString());
                        counter++;
                        activeOrders = true;
                    }

                }
            }
            if(!activeOrders)
            {
                System.out.println("No active orders available!");
            }

        }
        return counter;

    }

    /**
     * Method to update balance
     * @param security security to update
     * @param newValue new value
     */
    public void updateBuyOrderBalances(String security, Double newValue)
    {
        if(newValue == 0)
            buyOrdersBalances.remove(security);
        else
            buyOrdersBalances.put(security, newValue);
    }

    /**
     * Method to increment balance
     * @param security security to increment
     * @param newValue new value
     */
    public void incrementBuyOrderBalances(String security, Double newValue)
    {

        if(buyOrdersBalances.containsKey(security))
            buyOrdersBalances.put(security, buyOrdersBalances.get(security) + newValue);
        else
        {
            if(newValue!=0)
                buyOrdersBalances.put(security, newValue);
        }
    }

    /**
     * Method to get buy order balances
     * @return balances
     */
    public HashMap<String, Double> getBuyOrdersBalances()
    {
        return buyOrdersBalances;
    }

    /**
     * Method to get buy order balances in an unmodifiable map
     * @return an unmodifiable map
     */
    public Map<String, Double> getBuyOrdersBalancesUnmodifiable()
    {
        return Collections.unmodifiableMap(buyOrdersBalances);
    }

    /**
     * Method to print user's balances
     */
    public void viewMyBalances()
    {
        //if balances size is 0
        if(this.balances.size() == 0)
        {
            System.out.println("You have no idle balances");
        }
        else
        {
            //format balances and print them
            System.out.println("----------Idle Balances----------");
            String format = "%-30s%-30s%n";
            System.out.printf(format, "Name", "Quantity");
            this.balances.entrySet().forEach(entry->{
                System.out.printf(format, entry.getKey(), entry.getValue());
            });
        }

        //if balances size is 0
        if(this.getSellOrdersBalances().size() != 0)
        {
            //format balances and print them
            System.out.println("----------Sell Order Balances----------");
            String format = "%-30s%-30s%n";
            System.out.printf(format, "Name", "Quantity");
            sellOrdersBalances.entrySet().forEach(entry->{
                System.out.printf(format, entry.getKey(), entry.getValue());
            });
        }

        //if balances size is 0
        if(this.getBuyOrdersBalances().size() != 0)
        {
            //format balances and print them
            System.out.println("----------Buy Order Balances----------");
            String format = "%-30s%-30s%n";
            System.out.printf(format, "Name", "Quantity");
            buyOrdersBalances.entrySet().forEach(entry->{
                System.out.printf(format, entry.getKey(), entry.getValue());
            });
        }
    }

    /**
     * Method to cancel order
     * @param type 1 if BuyOrder, 2 if SellOrder
     * @param id id to cancel
     */
    public void cancelOrder(int type, String id)
    {
        //if buy order
        if(type == 1)
        {
            //set status to cancelled
            BuyOrder original = buyOrders.get(id);
            original.setStatus(Status.CANCELLED);
            buyOrders.put(id, original);
            //calculate amount to restore
            double restoreAmt = original.getQuantityAvailable() * original.getPrice();

            //get euro balance
            Double oldAmt = balances.get("Euro");
            //add amount in balance
            oldAmt += restoreAmt;
            //update balance
            balances.put("Euro", oldAmt);

            //get Euro buy order balance
            Double buyOrderEuroBalance = buyOrdersBalances.get("Euro");
            //if Euro buy order balance is equal to the amount to remove, remove the Euro entry
            if(buyOrderEuroBalance == restoreAmt)
                buyOrdersBalances.remove("Euro");
                //else subtract the value
            else
            {
                buyOrderEuroBalance -= restoreAmt;
                buyOrdersBalances.put("Euro", buyOrderEuroBalance);
            }
        }
        //if sell order
        else if(type == 2)
        {
            //set status to cancelled
            SellOrder original = sellOrders.get(id);
            original.setStatus(Status.CANCELLED);
            sellOrders.put(id, original);

            //calculate amount to restore
            double restoreAmt = original.getQuantityAvailable();

            //if balances contain the security
            if(balances.containsKey(original.getSecurity()))
            {

                //get security balance
                Double oldAmt = balances.get(original.getSecurity());
                //add amount in balance
                oldAmt += restoreAmt;
                //update balance
                balances.put(original.getSecurity(), oldAmt);


            }
            //if it does not exist just add the balance
            else
                balances.put(original.getSecurity(), original.getQuantityAvailable());


            //get security sell order balance
            Double sellOrderSecurityBalance = sellOrdersBalances.get(original.getSecurity());
            //if security sell order balance is equal to the amount to remove, remove the Euro entry
            if(sellOrderSecurityBalance == restoreAmt)
                sellOrdersBalances.remove(original.getSecurity());
            //else subtract the value
            else
            {
                sellOrderSecurityBalance -= restoreAmt;
                sellOrdersBalances.put(original.getSecurity(), sellOrderSecurityBalance);
            }
        }
    }

}
