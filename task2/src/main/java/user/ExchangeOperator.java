package user;

import java.util.*;

public class ExchangeOperator extends GeneralUser {

    private HashMap<String, User> toBeApproved = new HashMap<String, User>();

    public ExchangeOperator(String name, String email, String password)
    {
        super(name, email, password);
    }

    public ExchangeOperator()
    {
        super();
    }

    /**
     * Method to show all the users to be approved
     * @return the number of users to be approved
     */
    public int showUsersToBeApproved()
    {
        if(toBeApproved.size() == 0)
        {
            System.out.println("No users to approve!");
            return 0;
        }
        else
        {
            System.out.println("---------Users awaiting approval---------");
            String format = "%-50s%-50s%-50s%-30s%n";
            System.out.printf(format, "Id", "Name", "Email", "Date of Birth");

            for (Map.Entry entry : toBeApproved.entrySet()) {
                User currentUser = (User) entry.getValue();
                System.out.printf(format, currentUser.getId(), currentUser.name, currentUser.getEmail(), currentUser.dob.toString());
            }
            return toBeApproved.size();
        }
    }

    /**
     * Method to get users to be approved which cannot be modified
     * @return get users to be approved
     */
    public List<User> getToBeApprovedUnmodifiable()
    {
        return Collections.unmodifiableList(new ArrayList<>(toBeApproved.values()));

    }

    /**
     * Method to get users to be approved
     * @return unmodifiableMap of users to be approved
     */
    public Map<String, User> getToBeApproved()
    {
        return Collections.unmodifiableMap(toBeApproved);
    }

    /**
     * Method to approve users
     * @param id: id to approve
     */
    public User approve(String id)
    {
        if(!toBeApproved.containsKey(id))
            return null;

        //remove user from list
        User user = toBeApproved.get(id);
        //set the user's field of approved to true;
        user.setApproved(true);

        //remove
        toBeApproved.remove(id);

        return user;
    }

    /**
     * Method to add to the approved list
     * @param user to add
     */
    public void addToApprove(User user)
    {
        toBeApproved.put(user.getId(), user);
    }

    /**
     * Method to approve all users in list
     */
    public ArrayList<User> approveAll()
    {
        ArrayList<User> approved = new ArrayList<>();
        for (Map.Entry entry : toBeApproved.entrySet())
        {
            User currentApprovedUser = (User)entry.getValue();
            currentApprovedUser.setApproved(true);
            //if trader top up initial 50euro
            if(currentApprovedUser instanceof Trader)
                currentApprovedUser.topUp(70);
            approved.add(currentApprovedUser);
        }

        toBeApproved.clear();

        return approved;
    }



}
