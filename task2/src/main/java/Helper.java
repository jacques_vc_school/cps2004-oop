import platform.ExchangePlatform;

import java.util.ArrayList;
import java.util.InputMismatchException;
import java.util.Scanner;

public class Helper {

    public static String securityInputAdd(Scanner scanner, ExchangePlatform platform)
    {
        String securityName = "";
        boolean securityExists;
        do
        {
            if(securityName.isEmpty())
                System.out.println("Enter a name for the security. Enter 0 to cancel");

            securityName = scanner.nextLine();

            if(securityName.equals("0"))
                break;

            securityExists = platform.checkSecurityName(securityName);
            if(securityExists)
            {
                System.out.println("Security with that name already exists! Enter a new name");
            }
        }while(securityName.isEmpty() || securityExists);

        return securityName;
    }

    public static int chooseOrderType(Scanner scanner)
    {
        int typeOfOrder = -1;
        do {
            try {
                typeOfOrder = scanner.nextInt();

                if(typeOfOrder ==0)
                    break;

            }catch(InputMismatchException e) {
                System.out.println("Please input 1 or 2");
                scanner.next();
            }

        }while(typeOfOrder != 1 && typeOfOrder != 2);

        return typeOfOrder;
    }

    public static String securityInputChoice(Scanner scanner, ExchangePlatform platform, String message)
    {
        String securityName = "";
        boolean securityExists;
        do
        {
            if(securityName.isEmpty())
                System.out.println(message);

            securityName = scanner.nextLine();

            if(securityName.equals("0"))
                break;

            securityExists = platform.checkSecurityName(securityName);
            if(!securityExists)
            {
                System.out.println("Security does not exist! Enter a correct name");
            }
        }while(securityName.isEmpty() || !securityExists);

        return securityName;
    }

    public static String securityInputFromBalances(Scanner scanner, ArrayList<String> balances, String message)
    {
        String securityName = "";
        do
        {
            if(securityName.isEmpty())
                System.out.println(message);

            securityName = scanner.nextLine();

            if(!balances.contains(securityName) && !securityName.equals(""))
            {
                System.out.println("Security not in balances! Please enter a security which you already own.");
            }

            if(securityName.equals("0"))
                break;


        }while(securityName.equals("") || !balances.contains(securityName));

        return securityName;
    }

    public static int intInput(Scanner scanner, String message, String notANumberError, int min, int max)
    {
        int intInput = 0;

        do {
            System.out.println(message);

            try {
                intInput = scanner.nextInt();

                if(intInput == 0)
                    break;
            }catch(InputMismatchException e) {
                System.out.println(notANumberError);
                scanner.next();
            }
        }while(intInput <min || intInput > max);

        return intInput;
    }

    public static double doubleInput(Scanner scanner, String message, String notANumberError, int min)
    {
        double doubleInput = -1;

        do {
            System.out.println(message);

            try {
                doubleInput = scanner.nextDouble();

                if(doubleInput == 0)
                    break;
            }catch(InputMismatchException e) {
                System.out.println(notANumberError);
                scanner.next();
            }
        }while(doubleInput <min);

        return doubleInput;
    }

    public static String stringInput(Scanner scanner, String message)
    {
        String input = "";
        do
        {
            System.out.println(message);
            input = scanner.nextLine();

            if(input.equals("0"))
                break;
        }while(input.isEmpty());

        return input;
    }

    public static String emailInput(Scanner scanner, ExchangePlatform platform)
    {
        String email = "";
        int correct;
        do
        {
            if(email.isEmpty())
                System.out.println("Enter an email address. Enter 0 to abort");
            email = scanner.nextLine();

            if(email.equals("0"))
                break;

            correct = platform.checkEmail(email);

            if(correct == 1)
            {
                System.out.println("Email not in correct format! Enter a new e-mail address");
            }
            else if(correct == 2)
            {
                System.out.println("Email already exists! Enter a new e-mail address");
            }
        }while(email.isEmpty() || correct != 0);

        return email;
    }

    public static void flushEOL(Scanner scanner)
    {
        scanner.nextLine();
    }

}
