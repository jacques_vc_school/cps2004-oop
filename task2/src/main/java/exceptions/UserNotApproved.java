package exceptions;

public class UserNotApproved extends Exception
{
    public UserNotApproved(String message)
    {
        super(message);
    }
}
