package exceptions;

public class IncorrectUserDetailException extends Exception
{
    public IncorrectUserDetailException(String message)
    {
        super(message);
    }
}
