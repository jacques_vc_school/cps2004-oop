package exceptions;

public class MoreThanBalanceException extends Exception
{
    public MoreThanBalanceException(String message)
    {
        super(message);
    }
}
