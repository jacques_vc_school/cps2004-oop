package security;

public class Security {
    private String desc;
    private String name;
    private double price;
    private int totalSupply;

    //constructor
    public Security(String name, String desc, double price, int totalSupply)
    {
        this.name = name;
        this.desc = desc;
        this.price = price;
        this.totalSupply = totalSupply;
    }

    /**
     * Getter for description
     * @return security's description
     */
    public String getDesc() {
        return desc;
    }

    /**
     * Getter for name
     * @return security's name
     */
    public String getName() {
        return name;
    }

    /**
     * Getter for price
     * @return security's price
     */
    public double getPrice() {
        return price;
    }

    /**
     * Getter for total supply
     * @return security's total supply
     */
    public int getTotalSupply() {
        return totalSupply;
    }

    /**
     * Setter for price
     * @param newPrice new price to add
     */
    public void setPrice(double newPrice)
    {
        price = newPrice;
    }

    /**
     * Method to increment amoumt
     * @param amount amount to add
     */
    public void incrementSupply(int amount)
    {
        totalSupply += amount;
    }
}


