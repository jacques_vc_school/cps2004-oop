package platform;

import order.BuyOrder;
import order.SellOrder;
import order.Order;
import order.Status;

import java.util.*;

public class OrderBook {
    /**
     * These are used to locate search through orders easily.
     * The structure is as follows:
     * There are two Hashmaps, one for buy orders and another for sell orders.
     * The hashmaps has the security name as the key,
     * while they hold a treemap with the price as key and an arraylist of buy orders or sell orders
     * with that price as value for the hashmap.
     */
    protected HashMap<String, TreeMap<Double, ArrayList<SellOrder>>> sellOrders = new HashMap<String, TreeMap<Double, ArrayList<SellOrder>>>();
    protected HashMap<String, TreeMap<Double, ArrayList<BuyOrder>>> buyOrders = new HashMap<String, TreeMap<Double, ArrayList<BuyOrder>>>();


    /**
     /**
     * Method to get buyOrders in an unmodifiable map
     * @return an unmodifiable map
     */
    public Map<String, TreeMap<Double, ArrayList<BuyOrder>>> getBuyOrdersUnmodifiable()
    {
        return Collections.unmodifiableMap(buyOrders);
    }

    /**
     * Method to get sellOrders in an unmodifiable map
     * @return an unmodifiable map
     */
    public Map<String, TreeMap<Double, ArrayList<SellOrder>>> getSellOrdersUnmodifiable()
    {
        return Collections.unmodifiableMap(sellOrders);
    }

    /**
     * Method to return modifiable sell orders
     * @return modifiable sell orders
     */
    public HashMap<String, TreeMap<Double, ArrayList<SellOrder>>> getSellOrders() {
        return sellOrders;
    }

    /**
     * Method to return modifiable buy orders
     * @return modifiable buy orders
     */
    public HashMap<String, TreeMap<Double, ArrayList<BuyOrder>>> getBuyOrders() {
        return buyOrders;
    }

    /**
     * Method to get all orders of a security
     * @param security security for which orders to show
     */
    @SuppressWarnings("unchecked")
    public void showAllOrders(String security)
    {
        System.out.println();
        System.out.println();
        if(buyOrders.size() == 0 && sellOrders.size() == 0)
        {
            System.out.println("No orders available!");
        }
        else
        {
            if(buyOrders.size() > 0)
            {

                System.out.println("---------Buy Orders---------");
                String format = "%-50s%-30s%-30s%-30s%-30s%-30s%-30s%n";
                System.out.printf(format, "Id", "Security", "Price", "Quantity", "Quantity Available", "Status", "Timestamp");

                for (Map.Entry entry : buyOrders.get(security).entrySet()) {
                    ArrayList<BuyOrder> currentVal = (ArrayList<BuyOrder>) entry.getValue();

                    for(int i=0; i < currentVal.size(); i++)
                    {
                        BuyOrder currentOrder = currentVal.get(i);
                        System.out.printf(format, currentOrder.getId(), currentOrder.getSecurity(), currentOrder.getPrice(), currentOrder.getQuantity(), currentOrder.getQuantityAvailable(), currentOrder.getStatus(), currentOrder.getTimestamp());
                    }
                }
                //padding
                System.out.println();
                System.out.println();
            }
            if(sellOrders.size() > 0)
            {

                System.out.println("---------Sell Orders---------");
                String format = "%-50s%-30s%-30s%-30s%-30s%-30s%-30s%n";
                System.out.printf(format, "Id", "Security", "Price", "Quantity", "Quantity Available", "Status", "Timestamp");

                for (Map.Entry entry : sellOrders.get(security).entrySet()) {
                    ArrayList<SellOrder> currentVal = (ArrayList<SellOrder>) entry.getValue();


                    for(int i=0; i < currentVal.size(); i++)
                    {
                        SellOrder currentOrder = currentVal.get(i);
                        System.out.printf(format, currentOrder.getId(), currentOrder.getSecurity(), currentOrder.getPrice(), currentOrder.getQuantity(), currentOrder.getQuantityAvailable(), currentOrder.getStatus(), currentOrder.getTimestamp());
                    }
                }

                //padding
                System.out.println();
                System.out.println();
            }
        }

    }

    /**
     * Method to get all orders of a security
     * @param security security for which orders to show
     */
    @SuppressWarnings("unchecked")
    public ArrayList<Order> getAllOrders(String security)
    {
        ArrayList<Order> orders = new ArrayList<Order>();

        if(buyOrders.size() > 0)
        {

            for (Map.Entry entry : buyOrders.get(security).entrySet()) {
                ArrayList<BuyOrder> currentVal = (ArrayList<BuyOrder>) entry.getValue();

                for(int i=0; i < currentVal.size(); i++)
                {
                    orders.add(currentVal.get(i));
                }
            }
        }
        if(sellOrders.size() > 0)
        {


            for (Map.Entry entry : sellOrders.get(security).entrySet()) {
                ArrayList<SellOrder> currentVal = (ArrayList<SellOrder>) entry.getValue();

                for(int i=0; i < currentVal.size(); i++)
                {
                    orders.add(currentVal.get(i));
                }
            }

        }

        return orders;

    }

    /**
     * Method to get all orders of a security unmodifiable
     * @param security security for which orders to show
     */
    @SuppressWarnings("unchecked")
    public List<Order> getAllOrdersUnmodifiable(String security)
    {
        List<Order> orders = new ArrayList<>();

        if(buyOrders.size() > 0)
        {

            for (Map.Entry entry : buyOrders.get(security).entrySet()) {
                ArrayList<BuyOrder> currentVal = (ArrayList<BuyOrder>) entry.getValue();

                for(int i=0; i < currentVal.size(); i++)
                {
                    orders.add(currentVal.get(i));
                }
            }
        }
        if(sellOrders.size() > 0)
        {


            for (Map.Entry entry : sellOrders.get(security).entrySet()) {
                ArrayList<SellOrder> currentVal = (ArrayList<SellOrder>) entry.getValue();

                for(int i=0; i < currentVal.size(); i++)
                {
                    orders.add(currentVal.get(i));
                }
            }

        }

        return Collections.unmodifiableList(orders);
    }

    /**
     * Method to place a Buy Order
     * @param newBuyOrder new buy order to place
     */
    public void placeBuyOrder(BuyOrder newBuyOrder)
    {
        //If the buy orderbook array contaning buy orders contains orders for this security
        if(buyOrders.containsKey(newBuyOrder.getSecurity()))
        {
            //Get the map of this security
            TreeMap<Double, ArrayList<BuyOrder>> buyOrdersList = buyOrders.get(newBuyOrder.getSecurity());

            //If there exists a buy order for this security with the same price
            if(buyOrdersList.containsKey(newBuyOrder.getPrice()))
            {
                //get original arraylist of buyorders
                ArrayList<BuyOrder> originalBuyOrders = buyOrdersList.get(newBuyOrder.getPrice());

                //add the new buy order to the treemap
                originalBuyOrders.add(newBuyOrder);
                buyOrdersList.put(newBuyOrder.getPrice(), originalBuyOrders);

                buyOrders.put(newBuyOrder.getSecurity(), buyOrdersList);
            }
            //If not the same price
            else
            {
                //Create a new array list containign buy orders
                ArrayList<BuyOrder> newBuyOrders = new ArrayList<>();
                //add the new order
                newBuyOrders.add(newBuyOrder);
                //put the list in the treemap containing prices and arraylists of buy orders
                buyOrdersList.put(newBuyOrder.getPrice(), newBuyOrders);
                //Place the updated treemap in the buy orders hashmap in the order book
                buyOrders.put(newBuyOrder.getSecurity(), buyOrdersList);
            }
        }
        else
        {
            //Create a new tree map and arraylist with the new buy in reverse order (highest priced first);
            TreeMap<Double, ArrayList<BuyOrder>> newBuyTreeMap = new TreeMap<>(Collections.reverseOrder());
            ArrayList<BuyOrder> newBuyOrders = new ArrayList<>();
            newBuyOrders.add(newBuyOrder);
            newBuyTreeMap.put(newBuyOrder.getPrice(), newBuyOrders);
            buyOrders.put(newBuyOrder.getSecurity(), newBuyTreeMap);
        }
    }

    /**
     * Method to place a Sell Order
     * @param newSellOrder sell order to place
     */
    public void placeSellOrder(SellOrder newSellOrder)
    {
        //If the buy orderbook array contaning sell orders contains orders for this security
        if(sellOrders.containsKey(newSellOrder.getSecurity()))
        {
            //Get the map of this security
            TreeMap<Double, ArrayList<SellOrder>> sellOrdersList = sellOrders.get(newSellOrder.getSecurity());

            //If there exists a buy order for this security with the same price
            if(sellOrdersList.containsKey(newSellOrder.getPrice()))
            {
                //get original arraylist of buyorders
                ArrayList<SellOrder> originalSellOrders = sellOrdersList.get(newSellOrder.getPrice());

                //add the new sell order to the treemap
                originalSellOrders.add(newSellOrder);
                sellOrdersList.put(newSellOrder.getPrice(), originalSellOrders);

                sellOrders.put(newSellOrder.getSecurity(), sellOrdersList);
            }
            //If not the same price
            else
            {
                //Create a new array list containing sell orders
                ArrayList<SellOrder> newSellOrders = new ArrayList<>();
                //add the new order
                newSellOrders.add(newSellOrder);
                //put the list in the treemap containing prices and arraylists of sell orders
                sellOrdersList.put(newSellOrder.getPrice(), newSellOrders);
                //Place the updated treemap in the sell orders hashmap in the order book
                sellOrders.put(newSellOrder.getSecurity(), sellOrdersList);
            }
        }
        else
        {
            //Create a new tree map and arraylist with the sell buy;
            TreeMap<Double, ArrayList<SellOrder>> newSellTreeMap = new TreeMap<>();
            ArrayList<SellOrder> newSellOrders = new ArrayList<>();
            newSellOrders.add(newSellOrder);
            newSellTreeMap.put(newSellOrder.getPrice(), newSellOrders);
            sellOrders.put(newSellOrder.getSecurity(), newSellTreeMap);
        }
    }


    /**
     * Method to cancelOrder
     * @param type 1 if BuyOrder, 2 if SellOrder
     * @param id id to cancel
     * @return true if successful, false if not
     */
    public boolean cancelOrder(int type, String id)
    {
        //if buy order
        if(type == 1)
        {
            for(Map.Entry<String, TreeMap<Double, ArrayList<BuyOrder>>> outerEntry : buyOrders.entrySet())
            {
                TreeMap<Double, ArrayList<BuyOrder>> securityVal = outerEntry.getValue();

                for(Map.Entry<Double, ArrayList<BuyOrder>> innerEntry : securityVal.entrySet()) {
                    ArrayList<BuyOrder> orders = innerEntry.getValue();

                    for(int i=0; i < orders.size(); i++)
                    {
                        //if id matches, set to cancelled
                        if(id.equals(orders.get(i).getId()))
                        {
                            orders.get(i).setStatus(Status.CANCELLED);
                            return true;
                        }
                    }

                }
            }

        }
        //if sell order
        else if(type == 2)
        {
            for(Map.Entry<String, TreeMap<Double, ArrayList<SellOrder>>> outerEntry : sellOrders.entrySet())
            {
                TreeMap<Double, ArrayList<SellOrder>> securityVal = outerEntry.getValue();

                for(Map.Entry<Double, ArrayList<SellOrder>> innerEntry : securityVal.entrySet()) {
                    ArrayList<SellOrder> orders = innerEntry.getValue();

                    for(int i=0; i < orders.size(); i++)
                    {
                        //if id matches, set to cancelled
                        if(id.equals(orders.get(i).getId()))
                        {
                            orders.get(i).setStatus(Status.CANCELLED);
                            return true;
                        }
                    }

                }
            }
        }

        return false;
    }

    /**
     * Method to get order's maker
     * @param type 1 if BuyOrder, 2 if SellOrder
     * @param id id to of order
     * @return true if successful, false if not
     */
    public String getOrderMaker(int type, String id)
    {
        //if buy order
        if(type == 1)
        {
            for(Map.Entry<String, TreeMap<Double, ArrayList<BuyOrder>>> outerEntry : buyOrders.entrySet())
            {
                TreeMap<Double, ArrayList<BuyOrder>> securityVal = outerEntry.getValue();

                for(Map.Entry<Double, ArrayList<BuyOrder>> innerEntry : securityVal.entrySet()) {
                    ArrayList<BuyOrder> orders = innerEntry.getValue();

                    for(int i=0; i < orders.size(); i++)
                    {
                        //if id matches, set to cancelled
                        if(id.equals(orders.get(i).getId()))
                        {
                            return orders.get(i).getTrader();
                        }
                    }

                }
            }

        }
        //if sell order
        else if(type == 2)
        {
            for(Map.Entry<String, TreeMap<Double, ArrayList<SellOrder>>> outerEntry : sellOrders.entrySet())
            {
                TreeMap<Double, ArrayList<SellOrder>> securityVal = outerEntry.getValue();

                for(Map.Entry<Double, ArrayList<SellOrder>> innerEntry : securityVal.entrySet()) {
                    ArrayList<SellOrder> orders = innerEntry.getValue();

                    for(int i=0; i < orders.size(); i++)
                    {
                        //if id matches, set to cancelled
                        if(id.equals(orders.get(i).getId()))
                        {
                            orders.get(i).setStatus(Status.CANCELLED);
                            return orders.get(i).getTrader();
                        }
                    }

                }
            }
        }

        return null;
    }

}