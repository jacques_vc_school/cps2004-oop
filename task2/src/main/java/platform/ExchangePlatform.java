package platform;

import java.sql.Timestamp;
import java.time.LocalDate;
import java.time.Period;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import exceptions.*;
import order.BuyOrder;
import order.SellOrder;
import order.Status;
import user.ExchangeOperator;
import user.Lister;
import user.Trader;
import user.User;
import order.Order;
import security.Security;

public class ExchangePlatform {

    static class MatchingEngine {
        /**
         * Method to match orders
         * @param logged logged user
         * @param newOrder new order to match
         * @param orderType 1 if buy order, 2 if sell order
         * @return true if successful
         * @throws MoreThanBalanceException if it is more than the avaulable balance
         */
        protected static boolean matchingEngine(User logged, Order newOrder, int orderType, OrderBook orderBook, HashMap<String, User> users) throws MoreThanBalanceException {

            boolean done = false;

            //if buy order
            if(orderType == 1)
            {
                BuyOrder newBuy = (BuyOrder)newOrder;
                //If there are no sell orders for the security to buy
                if(!orderBook.sellOrders.containsKey(newBuy.getSecurity()))
                {
                    //add the new buy order
                    orderBook.placeBuyOrder(newBuy);
                    //calculate amount
                    double amt = newBuy.getQuantity() * newBuy.getPrice();
                    //reduce euro value from idle balances
                    logged.reduceEuroBalance(amt);
                    //add euro amt to buy order balances
                    ((Trader) logged).incrementBuyOrderBalances("Euro", amt);
                    //add users buy order
                    ((Trader) logged).addBuyOrder(newBuy);
                }
                else
                {
                    //get tree map of security
                    TreeMap<Double, ArrayList<SellOrder>> originalTreeMap = orderBook.sellOrders.get(newBuy.getSecurity());
                    //If the lowest priced sell order is more than the price of the new buy order
                    if(newBuy.getPrice() < originalTreeMap.firstKey())
                    {
                        //place the new buy order
                        orderBook.placeBuyOrder(newBuy);
                        //add order to personal orders
                        ((Trader) logged).addBuyOrder(newBuy);
                    }
                    //if the lowest priced sell order can be sold
                    else
                    {

                        //loop through all the entries in the TreeMap containing the price and an array of sell orders with that price
                        //until the buy order is fulfilled as much as possible according to the sell orders present in the order book
                        for(Map.Entry<Double,ArrayList<SellOrder>> entry : originalTreeMap.entrySet())
                        {
                            //if the flag done is set to true, break
                            if(done)
                                break;

                            //get the current entry's price
                            Double price = entry.getKey();
                            //get the current entry's list of sell orders for the current price
                            ArrayList<SellOrder> sellOrderList = entry.getValue();

                            //if the selling price is equal to the buying price
                            if(price == newBuy.getPrice())
                            {
                                //get the amount still needed to buy
                                double quantityLeftToBuy = newBuy.getQuantityAvailable();

                                //loop through all the sell orders selling at the current price
                                for(int i=0; i< sellOrderList.size(); i++)
                                {

                                    //get the sell order
                                    SellOrder currentSellOrder = sellOrderList.get(i);

                                    //if seller is not buyer and the order is active
                                    if(!logged.getEmail().equals(currentSellOrder.getTrader()) &&  currentSellOrder.getStatus() == Status.IN_PROGRESS)
                                    {

                                        //if the quantity to buy is less than the quantity available to sell in the sell order
                                        //this means that the buy order can be fulfilled
                                        if(quantityLeftToBuy  < currentSellOrder.getQuantityAvailable())
                                        {
                                            //make the new order fulfilled
                                            newBuy.setStatus(Status.FULFILLED);

                                            //Update seller's balance
                                            User seller = users.get(currentSellOrder.getTrader());

                                            //get original quantity
                                            Double original_quantity = seller.getSellOrdersBalances().get(newBuy.getSecurity());
                                            //Reduce new quantity from it
                                            double new_balance = original_quantity - newBuy.getQuantity();
                                            //update the seller's sell balance
                                            seller.updateSellOrderBalances(newBuy.getSecurity(), new_balance);
                                            double amt = newBuy.getQuantity() * currentSellOrder.getPrice();
                                            //add proceeds to seller's account
                                            seller.incrementEuroBalance( amt);

                                            //Update the buy order
                                            newBuy.setQuantityAvailable(0);
                                            newBuy.setStatus(Status.FULFILLED);

                                            //Adding buy order to orderBook
                                            orderBook.placeBuyOrder(newBuy);

                                            //Update the sell order in the orderBook
                                            currentSellOrder.reduceQuantityAvailable(newBuy.getQuantity());
                                            entry.setValue(sellOrderList);

                                            //set the done flag to true
                                            done = true;
                                            //break from loop
                                            break;

                                        }
                                        //quantity needed is exactly the same as the available quantity of the sell order found
                                        else if(quantityLeftToBuy == currentSellOrder.getQuantityAvailable())
                                        {
                                            //make the new order fulfilled
                                            newBuy.setStatus(Status.FULFILLED);

                                            //Update seller's balance
                                            User seller = users.get(currentSellOrder.getTrader());
                                            //If seller already has balance

                                            //get original quantity
                                            Double original_quantity = seller.getSellOrdersBalances().get(newBuy.getSecurity());
                                            //Add new quantity to it
                                            double new_balance = original_quantity - newBuy.getQuantity();
                                            //update the seller's sell balance
                                            seller.updateSellOrderBalances(newBuy.getSecurity(), new_balance);
                                            double amt = newBuy.getQuantity() * currentSellOrder.getPrice();
                                            //add proceeds to sellers's account
                                            seller.incrementEuroBalance(amt);

                                            //Add the buy order
                                            newBuy.setQuantityAvailable(0);
                                            newBuy.setStatus(Status.FULFILLED);

                                            //Adding buy order to orderBook
                                            orderBook.placeBuyOrder(newBuy);

                                            //Update the sell order in the orderBook
                                            currentSellOrder.setQuantityAvailable(0);
                                            currentSellOrder.setStatus(Status.FULFILLED);
                                            entry.setValue(sellOrderList);
                                            done = true;
                                            break;


                                        }
                                        //quantity needed is larger than the available quantity of the sell order found
                                        else
                                        {
                                            //make the sell order fulfilled
                                            double quantitySold = currentSellOrder.getQuantityAvailable();
                                            currentSellOrder.setQuantityAvailable(0);
                                            currentSellOrder.setStatus(Status.FULFILLED);
                                            entry.setValue(sellOrderList);

                                            //Update seller's balance
                                            User seller = users.get(currentSellOrder.getTrader());

                                            //get original quantity
                                            Double original_quantity = seller.getSellOrdersBalances().get(newBuy.getSecurity());
                                            //Add new quantity to it
                                            seller.updateSellOrderBalances(newBuy.getSecurity(), original_quantity - quantitySold);
                                            seller.incrementEuroBalance(quantitySold*currentSellOrder.getPrice());

                                            //Add the buy order
                                            newBuy.reduceQuantityAvailable(quantitySold);
                                            done = false;


                                        }
                                    }

                                }
                            }

                        }
                    }

                    //If the order is not fulfilled
                    if(!done) {
                        //add the buy Order
                        orderBook.placeBuyOrder(newBuy);

                    }

                    //calculate the amount bought
                    double amtBought = newBuy.getQuantity() - newBuy.getQuantityAvailable();
                    //increment the balance
                    logged.incrementBalance(newBuy.getSecurity(), amtBought);
                    //decrement the eurobalance
                    logged.reduceEuroBalance(newBuy.getQuantity() * newBuy.getPrice());
                    //increment buy order balance by whats left of the order
                    ((Trader) logged).incrementBuyOrderBalances("Euro", newBuy.getQuantityAvailable() * newBuy.getPrice());
                    //add the user's order to his orders
                    ((Trader) logged).addBuyOrder(newBuy);

                }
            }
            //if sell order
            else
            {
                SellOrder newSell = (SellOrder)newOrder;
                //If there are no buy orders for the security to sell
                if(!orderBook.buyOrders.containsKey(newSell.getSecurity()))
                {
                    //add the new buy order
                    orderBook.placeSellOrder(newSell);
                    logged.reduceBalance(newSell.getSecurity(), newSell.getQuantity());
                    //add sell order balance
                    logged.incrementSellOrderBalances(newSell.getSecurity(), newSell.getQuantity());
                    //add users sell order
                    logged.addSellOrder(newSell);
                }
                else
                {
                    //get tree map of security
                    TreeMap<Double, ArrayList<BuyOrder>> originalTreeMap = orderBook.buyOrders.get(newSell.getSecurity());
                    //If the highest priced buy order is less than the price of the new sell order
                    if(newSell.getPrice() > originalTreeMap.firstKey())
                    {
                        //place the new buy order
                        orderBook.placeSellOrder(newSell);
                        //add order to personal orders
                        logged.addSellOrder(newSell);
                    }
                    //if the lowest priced sell order can be sold
                    else
                    {

                        //loop through all the entries in the TreeMap containing the price and an array of sell orders with that price
                        //until the buy order is fulfilled as much as possible according to the sell orders present in the order book
                        for(Map.Entry<Double,ArrayList<BuyOrder>> entry : originalTreeMap.entrySet())
                        {
                            //if the flag done is set to true, break
                            if(done)
                                break;

                            //get the current entry's price
                            Double price = entry.getKey();
                            //get the current entry's list of sell orders for the current price
                            ArrayList<BuyOrder> buyOrderList = entry.getValue();


                            //if the selling price is more than the highest buying price, break as the order cannot take place
                            if(newSell.getPrice() != price)
                                break;
                                //price of selling is not more than price of buying, hence can be bought
                            else
                            {
                                //get the amount still needed to buy
                                double quantityLeftToSell = newSell.getQuantityAvailable();

                                //loop through all the sell orders selling at the current price
                                for(int i=0; i< buyOrderList.size(); i++)
                                {

                                    //get the buy order
                                    BuyOrder currentBuyOrder = buyOrderList.get(i);


                                    //if seller is not buyer and the order is active
                                    if(!logged.getEmail().equals(currentBuyOrder.getTrader()) && currentBuyOrder.getStatus() == Status.IN_PROGRESS)
                                    {
                                        //if the quantity to less is less than the quantity available to buy in the buy order
                                        //this means that the buy order can be fulfilled
                                        if(quantityLeftToSell  < currentBuyOrder.getQuantityAvailable())
                                        {
                                            //make the new order fulfilled
                                            newSell.setStatus(Status.FULFILLED);

                                            //Update buyer's balance
                                            User buyer = users.get(currentBuyOrder.getTrader());

                                            //get euro balance from buyer buy order balance
                                            Double original_quantity = ((Trader) buyer).getBuyOrdersBalances().get("Euro");
                                            //Reduce new quantity from it
                                            double new_balance = original_quantity - (quantityLeftToSell*newSell.getPrice());
                                            //update the seller's sell balance
                                            ((Trader) buyer).updateBuyOrderBalances("Euro", new_balance);
                                            //add proceeds to sellers's account
                                            buyer.incrementBalance(newSell.getSecurity(), quantityLeftToSell);

                                            //Update the sell order
                                            newSell.setQuantityAvailable(0);
                                            newSell.setStatus(Status.FULFILLED);

                                            //Adding sell order to orderBook
                                            orderBook.placeSellOrder(newSell);

                                            //Update the sell order in the orderBook
                                            currentBuyOrder.reduceQuantityAvailable(newSell.getQuantity());
                                            entry.setValue(buyOrderList);

                                            //set the done flag to true
                                            done = true;
                                            //break from loop
                                            break;

                                        }
                                        //quantity needed is exactly the same as the available quantity of the sell order found
                                        else if(quantityLeftToSell == currentBuyOrder.getQuantityAvailable())
                                        {
                                            //make the new order fulfilled
                                            newSell.setStatus(Status.FULFILLED);

                                            //Update seller's balance
                                            User buyer = users.get(currentBuyOrder.getTrader());

                                            //get original quantity
                                            Double original_quantity = ((Trader)buyer).getBuyOrdersBalances().get("Euro");
                                            //Add new quantity to it
                                            double new_balance = (double)original_quantity - (quantityLeftToSell*newSell.getPrice());
                                            //update the seller's sell balance
                                            ((Trader) buyer).updateBuyOrderBalances("Euro", new_balance);
                                            //add bought securities to sellers's account
                                            buyer.incrementBalance(newSell.getSecurity(), quantityLeftToSell);

                                            //Add the buy order
                                            newSell.setQuantityAvailable(0);
                                            newSell.setStatus(Status.FULFILLED);

                                            //Adding buy order to orderBook
                                            orderBook.placeSellOrder(newSell);

                                            //Update the sell order in the orderBook
                                            currentBuyOrder.setQuantityAvailable(0);
                                            currentBuyOrder.setStatus(Status.FULFILLED);
                                            entry.setValue(buyOrderList);
                                            done = true;
                                            break;


                                        }
                                        //quantity needed is larger than the available quantity of the buy order found
                                        else
                                        {
                                            //make the sell order fulfilled
                                            double quantitySold = currentBuyOrder.getQuantityAvailable();
                                            currentBuyOrder.setQuantityAvailable(0);
                                            currentBuyOrder.setStatus(Status.FULFILLED);
                                            entry.setValue(buyOrderList);

                                            //Update buyer's balance
                                            User buyer = users.get(currentBuyOrder.getTrader());

                                            //get original quantity
                                            Double original_quantity = ((Trader) buyer).getBuyOrdersBalances().get("Euro");
                                            //Add new quantity to it
                                            ((Trader) buyer).updateBuyOrderBalances("Euro", original_quantity - (quantitySold*newSell.getPrice()));
                                            buyer.incrementBalance(newSell.getSecurity(), quantitySold);

                                            //Add the buy order
                                            newSell.reduceQuantityAvailable(quantitySold);

                                            done = false;


                                        }
                                    }
                                }


                            }

                        }
                    }

                    //If the order is not fulfilled
                    if(!done) {
                        //add the buy Order
                        orderBook.placeSellOrder(newSell);

                    }

                    //calculate the amount sold
                    double amtSold = newSell.getQuantity() - newSell.getQuantityAvailable();
                    //reduce the balance
                    logged.reduceBalance(newSell.getSecurity(), newSell.getQuantity());
                    //increase euro balance
                    logged.incrementEuroBalance(amtSold * newSell.getPrice());
                    //increment buy order balance
                    logged.incrementSellOrderBalances(newSell.getSecurity(), newSell.getQuantityAvailable());
                    logged.addSellOrder(newSell);

                }
            }

            return true;


        }
    }

    private static HashMap<String, User> users = new HashMap<>();
    private HashMap<String, String> tokens = new HashMap<>();
    private HashMap<String, Security> listedSecurities = new HashMap<>();
    private OrderBook orderBook = new OrderBook();
    private String operatorToken = "";
    private ExchangeOperator operator = new ExchangeOperator("Admin 1", "admin@platform.com", "password123");

    //create an object of SingleObject
    private static ExchangePlatform instance = new ExchangePlatform();

    //make the constructor private so that this class cannot be
    //instantiated
    private ExchangePlatform(){}

    //Get the only object available
    public static ExchangePlatform getInstance(){
        return instance;
    }

    /**
     * Method to reset platform - THIS SHOULD BE USED ONLY FOR TESTING
     */
    public void reset()
    {
        instance = new ExchangePlatform();
    }


    /**
     * Method to get buy orders for testing
     * @return list of buy orders ids
     *
     */
    public ArrayList<String> getBuyOrders()
    {
        ArrayList<String> toReturn = new ArrayList<>();

        for(Map.Entry<String, TreeMap<Double, ArrayList<BuyOrder>>> outerEntry : orderBook.getBuyOrdersUnmodifiable().entrySet())
        {
            TreeMap<Double, ArrayList<BuyOrder>> securityVal = outerEntry.getValue();

            for(Map.Entry<Double, ArrayList<BuyOrder>> innerEntry : securityVal.entrySet()) {
                ArrayList<BuyOrder> orders = innerEntry.getValue();

                for(int i=0; i < orders.size(); i++)
                {
                    toReturn.add(orders.get(i).getId());
                }

            }
        }

        return toReturn;


    }

    /**
     * Method to get sell orders for testing
     * @return list of sell orders ids
     */
    public ArrayList<String> getSellOrders()
    {
        ArrayList<String> toReturn = new ArrayList<>();

        for(Map.Entry<String, TreeMap<Double, ArrayList<SellOrder>>> outerEntry : orderBook.getSellOrdersUnmodifiable().entrySet())
        {
            TreeMap<Double, ArrayList<SellOrder>> securityVal = outerEntry.getValue();

            for(Map.Entry<Double, ArrayList<SellOrder>> innerEntry : securityVal.entrySet()) {
                ArrayList<SellOrder> orders = innerEntry.getValue();

                for(int i=0; i < orders.size(); i++)
                {
                    toReturn.add(orders.get(i).getId());
                }

            }
        }

        return toReturn;
    }


    /**
     * Method to get unapproved users
     * @param token operator's token
     * @return amount of unapproved users
     */
    public int getUnapprovedUsers(String token) throws UserNotAuthenticated {
        checkOperatorToken(token);

        return operator.showUsersToBeApproved();
    }

    /**
     * Method to approve
     * @param token operator's token
     * @param id id of user to approve
     * @return true if successfull
     */
    public boolean approve(String token, String id) throws UserNotAuthenticated {
        checkOperatorToken(token);

        User approved = operator.approve(id);
        if(approved == null)
            return false;
        else
        {
            //get user
            users.put(approved.getEmail(), approved);
            approved.topUp(50);

            return true;
        }

    }

    /**
     * Method to approve all users
     * @param token operator's token
     * @return true if successfull
     */
    public boolean approveAll(String token) throws UserNotAuthenticated {
        checkOperatorToken(token);
        ArrayList<User> approved = operator.approveAll();

        for(int i=0; i <approved.size(); i++)
        {
            User currentUser = approved.get(i);
            users.put(currentUser.getEmail(), currentUser);
            currentUser.topUp(50);
        }

        return true;


    }

    /**
     * Method to register
     * @param name new name
     * @param email new email
     * @param password new password
     * @param dob new date of birth
     * @param type account type, 1 if trader, 2 if lister
     * @return true if successfull
     * @throws IncorrectUserDetailException if details are empty
     */
    public boolean register(String name, String email, String password, LocalDate dob, int type) throws IncorrectUserDetailException {

        //if empty data
        if(name.isEmpty() || email.isEmpty() || password.isEmpty() ||dob == null)
            throw new IncorrectUserDetailException("All required data must be filled in");

        LocalDate now = LocalDate.now();
        Period p = Period.between(dob, now);
        if(p.getYears() < 18)
        {
            //if not disapprove
            return false;
        }

        //if trader
        if(type == 1)
        {
            //create trader and register
            Trader newTrader = new Trader(name, email, password, dob);

            users.put(email, newTrader);
            operator.addToApprove(newTrader);
        }
        //if lister
        else if(type == 2)
        {
            //create lister and register
            Lister newLister = new Lister(name, email, password, dob);

            users.put(email, newLister);
            operator.addToApprove(newLister);
        }
        else
            return false;
        return true;
    }

    /**
     * Method to login
     * @param email email to check
     * @param password password to check
     * @return Token
     */
    public String login(String email, String password, int accType) {

        //get email from hashmap of users
        User to_check = users.get(email);

        //if user does not exist
        if(to_check == null)
            return null;
            //if exists
        else
        {
            if(!to_check.getApproved())
            {
                System.out.println("User not approved by the operator. Kindly wait for approval or contact the administrator");
                return null;
            }
            else
            {
                //check password
                if(to_check.authenticate(email, password))
                {
                    //check login if in correct login
                    if((to_check instanceof Trader && accType != 1) || (to_check instanceof Lister && accType != 2))
                        return null;
                    //generate token
                    String token = UUID.randomUUID().toString();
                    //add token to hashmap of tokens
                    tokens.put(token, email);
                    System.out.println("Welcome back, "+to_check.name);
                    return token;
                }
                else
                {
                    System.out.println("Incorrect email or password!");
                    return null;
                }
            }

        }

    }

    /**
     * Method to login operator
     * @param email email to check
     * @param password password to check
     * @return Token
     */
    public String operatorLogin(String email, String password) {

        if(operator.authenticate(email, password))
        {
            //add token
            operatorToken = UUID.randomUUID().toString();
            //add token to hashmap of tokens
            System.out.println("Welcome back Operator");
            return operatorToken;
        }
        else
            return null;

    }

    /**
     * Method to change operator password
     * @param token operator's token
     * @param password new password
     * @return Token
     */
    public boolean changeAdminPassword(String token, String password) throws UserNotAuthenticated {

        if(!token.equals(operatorToken))
            throw new UserNotAuthenticated("Operator not authenticated");
        else
        {
            operator.changePassword(password);
            return true;
        }

    }

    /**
     * Method to get the user corresponding to the token
     * @param token users token
     * @return User matched to that token
     */
    private User getUserFromToken(String token) throws UserNotAuthenticated {
        //if that token does not exist
        if(!tokens.containsKey(token))
            throw new UserNotAuthenticated("User is not authenticated!");
        else
        {
            //get the user which has the email linked to this token;
            return users.get(tokens.get(token));
        }
    }

    /**
     * Method to check operator token
     * @param token auth token
     */
    private void checkOperatorToken(String token) throws UserNotAuthenticated {
        if(!token.equals(operatorToken))
            throw new UserNotAuthenticated("Operator not authenticated");
    }

    /**
     * Method to get the user corresponding to the token
     * @param user to check
     * @param type if 1 check if trader, if 2 check if lister
     */
    public void checkUserType(User user, int type) throws UserNotAllowedException {
        //if should be trader or listerand is not
        if((type == 1) && !(user instanceof Trader) || (type == 2 && !(user instanceof Lister)))
        {
            throw new UserNotAllowedException("User is not allowed to do this operation");
        }
    }

    /**
     * Method to add a security
     * @param token token of the user performing the action
     * @param securityName name of new security
     * @param securityDesc description of new security
     * @param securityPrice price of new security
     * @param securitySupply total supply of new security
     * @throws UserNotAuthenticated if user is not authenticated
     */
    public boolean addSecurity(String token, String securityName, String securityDesc, double securityPrice, int securitySupply) throws UserNotAuthenticated, UserNotAllowedException {
        //get user linked to that token
        User logged = getUserFromToken(token);

        checkUserType(logged, 2);
        // create security
        Security newSecurity = new Security(securityName, securityDesc, securityPrice, securitySupply);

        //If the key exists, return false
        if(listedSecurities.containsKey(newSecurity.getName()))
        {
            return false;
        }
        else {
            //else add the security to the list of securities
            listedSecurities.put(newSecurity.getName(), newSecurity);

            //create new sell order
            Timestamp ts = new Timestamp(new Date().getTime());
            SellOrder newSell = new SellOrder(logged.getEmail(), securitySupply, securityPrice, ts, Status.IN_PROGRESS, securityName, securitySupply);

            logged.addSellOrder(newSell);
            orderBook.placeSellOrder(newSell);

            //add security
            ((Lister) logged).listSecurity(newSecurity);
            return true;
        }

    }

    /**
     * Method to show a user's securities
     * @param token token of the user
     * @throws UserNotAuthenticated if user is not authenticated
     * @return the number of listed securities
     */
    public int showUserSecurities(String token) throws UserNotAuthenticated, UserNotAllowedException {
        //get user linked to that token
        User logged = getUserFromToken(token);
        checkUserType(logged, 2);
        //show securities
        return ((Lister) logged).viewMyListedSecurities();
    }

    /**
     * Method to show a user's balances
     * @param token token of the user
     * @throws UserNotAuthenticated if user is not authenticated
     */
    public void viewUserBalances(String token, int accType) throws UserNotAuthenticated {
        //get user linked to that token
        User logged = getUserFromToken(token);
        //show securities

        if(accType ==1)
            ((Trader) logged).viewMyBalances();
        else if(accType ==2)
            ((Lister) logged).viewMyBalances();
    }

    /**
     * Method to get a user's idle balances
     * @param token token of the user
     * @throws UserNotAuthenticated if user is not authenticated
     * @return list of idle balances
     */
    public ArrayList<String> getUserIdleBalances(String token) throws UserNotAuthenticated {
        //get user linked to that token
        User logged = getUserFromToken(token);

        //show securities
        return logged.showIdleBalances();
    }

    /**
     * Method to show user's orders
     * @param token token of the user
     * @param accType 1 if Trader, 2 if Lister
     * @throws UserNotAuthenticated if user is not authenticated
     */
    public void viewUserOrders(String token, int accType) throws UserNotAuthenticated {
        //get user linked to that token
        User logged = getUserFromToken(token);
        //if not found, throw exception

        //if trader
        if(accType == 1)
            ((Trader) logged).viewMyOrders();
        else if(accType == 2)
            ((Lister) logged).viewMyOrders();
    }

    public void logOut(String token)
    {
        // if token exist, remove it
        tokens.remove(token);
    }

    /**
     * Method to check if email address exists or is in correct format
     * @param email to check
     * @return 0 if correct, 1 if not in correct format, 2 if exists
     */
    public int checkEmail(String email)
    {
        String regex = "^(.+)@(.+)$";
        Pattern pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher(email);
        //if email incorrect format
        if(!matcher.matches())
            return 1;

        if(users.containsKey(email))
            return 2;

        return 0;
    }

    /**
     * Method used to update a security
     * @param token auth token
     * @param security security to add to
     * @param amount new amount supply to add
     * @param price price of new supply
     * @return true if successfull
     */
    public boolean updateSecurity(String token, String security, int amount, double price) throws UserNotAuthenticated, MoreThanBalanceException, UserNotAllowedException {
        User logged = getUserFromToken(token);

        checkUserType(logged, 2);
        //add value to balances and update user's listing
        boolean updated = ((Lister)logged).updateSecurity(security, amount, price);

        if(updated)
        {
            //create sell order
            return sell(token, security, amount, price);
        }
        else
            return false;

    }

    /**
     * Method which checks if the security name exists for validation on user input
     * @param name name of security to be added
     * @return true if exists
     */
    public boolean checkSecurityName(String name)
    {
        //checks if the listedSecuritites contains the name
        return listedSecurities.containsKey(name);
    }

    /**
     * Method to show the available securities
     * @return the amount of securities available
     */
    public int showSecurities()
    {
        //if there are no securities
        if(listedSecurities.size() ==0)
        {
            System.out.println("There are no available securities");
            return 0;
        }
        else
        {
            String format = "%-10s%-30s%-30s%-30s%-50s%n";
            System.out.printf(format, "Number", "Name", "Price", "Total Supply", "Desc");

            List<String> keys = new ArrayList<>(listedSecurities.keySet());
            for(int i = 0; i < listedSecurities.size(); i++)
            {
                String key = keys.get(i);
                Security value = listedSecurities.get(key);
                System.out.printf(format, i+1, value.getName(), value.getPrice(), value.getTotalSupply(), value.getDesc());
            }

            return listedSecurities.size();
        }
    }


    /**
     * Method to get the available securities
     * @return a list of securities available
     */
    public ArrayList<Security> getSecurities()
    {
        //if there are no securities
        if(listedSecurities.size() ==0)
        {
            return new ArrayList<>();
        }
        else
        {
            return new ArrayList<>(listedSecurities.values());
        }
    }

    /**
     * Method to show securities
     * @return the number of securities available
     */
    public int showListedSecurities()
    {
        //if there are no securities
        if(listedSecurities.size() ==0)
        {
            System.out.println("No securities available!");
            return  0;
        }
        else
        {
            for (Map.Entry entry : listedSecurities.entrySet())
            {
                String format = "%-30s%-30s%-30s%-50s%n";
                System.out.printf(format, "Name", "Price", "Total Supply", "Desc");
                Security currentVal = (Security) entry.getValue();
                System.out.printf(format, currentVal.getName(), currentVal.getPrice(), currentVal.getTotalSupply(), currentVal.getDesc());
            }

            return listedSecurities.size();
        }
    }

    /**
     * Method to show all orders of a security
     * @param security security to show
     */
    public void showSecurityOrders(String security)
    {
        orderBook.showAllOrders(security);
    }

    /**
     * Method to place buy order
     * @param token auth token
     * @param securityName security name
     * @param securityAmt security amount to buy
     * @param securityPrice price at which to buy security
     * @return true if successful
     */
    public boolean buy(String token, String securityName, double securityAmt, double securityPrice) throws UserNotAuthenticated, MoreThanBalanceException, UserNotAllowedException {
        //get user linked to that token
        User logged = getUserFromToken(token);

        checkUserType(logged, 1);

        //create timestamp
        Timestamp ts = new Timestamp(new Date().getTime());
        //create buy order
        BuyOrder newBuy = new BuyOrder(logged.getEmail(), securityAmt, securityPrice, ts, order.Status.IN_PROGRESS, securityName, securityAmt);

        //check amount
        if(logged.getBalances().get("Euro") < securityPrice * securityAmt)
        {
            throw new MoreThanBalanceException("Total order cost exceeds balance!");
        }

        return MatchingEngine.matchingEngine(logged, newBuy, 1, orderBook, users);
    }

    /**
     * Method to place sell order
     * @param token auth token
     * @param securityName security name
     * @param securityAmt security amount to sell
     * @param securityPrice price at which to sell security
     * @return true if successful
     */
    public boolean sell(String token, String securityName, double securityAmt, double securityPrice) throws UserNotAuthenticated, MoreThanBalanceException {
        //get user linked to that token
        User logged = getUserFromToken(token);

        //create timestamp
        Timestamp ts = new Timestamp(new Date().getTime());
        //create buy order
        SellOrder newSell = new SellOrder(logged.getEmail(), securityAmt, securityPrice, ts, order.Status.IN_PROGRESS, securityName, securityAmt);

        //check amount in balances
        if(!logged.getBalances().containsKey(securityName))
        {
            throw new MoreThanBalanceException("Total order exceeds balance!");
        }
        else
        {
            if(logged.getBalances().get(securityName) < securityAmt)
            {
                throw new MoreThanBalanceException("Total order exceeds balance!");
            }
        }


        return MatchingEngine.matchingEngine(logged, newSell, 2, orderBook, users);
    }

    /**
     * Method to cancel an order
     * @param token auth token
     * @param id of order to cancel
     * @param type 1 if BuyOrder, 2 if SellOrder
     * @param accType 1 if Trader, 2 if Lister
     * @return true if successful
     */
    //public boolean cancelOrder(String token, String security, double price, String id, int type, int accType) throws UserNotAuthenticated {
    public boolean cancelOrder(String token, String id, int type, int accType) throws UserNotAuthenticated {
        //get user linked to that token
        User logged = null;

        if(accType == 1 || accType == 2)
            logged = getUserFromToken(token);
        else if(accType ==3)
        {
            checkOperatorToken(token);
        }

        //cancel order from order book
        boolean cancelled = orderBook.cancelOrder(type, id);

        //if cancelled from order book, cancel in user
        if(cancelled)
        {
            //if trader
            if(accType == 1)
                ((Trader)logged).cancelOrder(type, id);
                //if lister
            else if(accType == 2)
                ((Lister)logged).cancelOrder(id);
            else if(accType == 3)
            {
                //get order maker
                String email = orderBook.getOrderMaker(type, id);
                User orderMaker = users.get(email);

                if(orderMaker instanceof Lister)
                    ((Lister)orderMaker).cancelOrder(id);
                else
                    ((Trader)orderMaker).cancelOrder(type, id);
            }
            else
                return false;

            return true;
        }
        else
            return false;
    }

    /**
     * Method to get user active orders
     * @param token auth token
     * @param type 1 if BuyOrder, 2 if SellOrder
     * @param accType 1 if Trader, 2 if Lister
     * @return number of available orders
     */
    public int getUserActiveOrders(String token, int type, int accType) throws UserNotAuthenticated {
        //get user linked to that token
        User logged = getUserFromToken(token);

        //if trader
        if(accType == 1)
            return ((Trader)logged).showActiveOrders(type);
            //if lister
        else if(accType == 2)
            return ((Lister)logged).showActiveOrders();

        return 0;
    }

    /**
     * Getter for listedSecurities for testing
     * @return listed securities
     */
    public HashMap<String, Security> getListedSecurities() {
        return listedSecurities;
    }

    /**
     * Method to getListedSecuities unmodifiable
     * @return listed securities unmodifiable
     */
    public Map<String, Security> getListedSecuritiesUnmodifiable() {
        return Collections.unmodifiableMap(listedSecurities);
    }

    /**
     * Method which gets users to be approved - JUST FOR TESTING
     * @return listed securities
     */
    public List<User> getToBeApproved() {
        return operator.getToBeApprovedUnmodifiable();
    }


}


