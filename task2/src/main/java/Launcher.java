import exceptions.*;
import platform.ExchangePlatform;

import java.time.LocalDate;
import java.util.*;

public class Launcher {

    public static void main(String args[]) {
        ExchangePlatform platform = ExchangePlatform.getInstance();
        int choice = 0;
        String token;
        System.out.println("Welcome to CPS2005 Exchange!");
        Scanner in = new Scanner(System.in);
        int accType = 0;
        do {
            System.out.println("----------Type of Account-----------");
            System.out.println("1.\t Trader");
            System.out.println("2.\t Lister");
            System.out.println("3.\t Operator");
            System.out.println("4.\t View Order Book");
            System.out.println("5.\t Quit");
            try {
                accType = in.nextInt();

                if(accType == 2 || accType == 1)
                {
                    do{
                            if(accType == 1)
                                System.out.println("----------Trader Menu-----------");
                            else if(accType == 2)
                                System.out.println("----------Lister Menu-----------");

                            System.out.println("1.\tRegister");
                            System.out.println("2.\tLogin");
                            System.out.println("3.\tChange account type");
                            System.out.println("4.\tQuit");


                        try{

                            choice = in.nextInt();
                            String name;
                            String email;
                            String password;
                            LocalDate dob;
                            boolean changeType = false;


                            switch(choice)
                            {
                                //register
                                case 1:
                                {

                                    //in.nextLine();
                                    Helper.flushEOL(in);

                                    name = Helper.stringInput(in, "Enter a name. Enter 0 to abort");

                                    if(name.equals("0"))
                                        break;


                                    email = Helper.emailInput(in, platform);

                                    if(email.equals("0"))
                                        break;

                                    System.out.println("Enter your date of birth");
                                    int day;
                                    int month;
                                    int year;

                                    day = Helper.intInput(in, "Enter the day (1-31). Enter 0 to abort", "Please input a number from 1 to 31", 1, 31);

                                    if(day == 0)
                                        break;

                                    month = Helper.intInput(in, "Enter the month (1-12). Enter 0 to abort", "Please input a number from 1 to 12", 1, 12);


                                    if(month == 0)
                                        break;

                                    year = Helper.intInput(in, "Enter the year. Enter 0 to abort", "Please input a correct year", 1900, 3000);


                                    if(year == 0)
                                        break;

                                    //consume new line
                                    Helper.flushEOL(in);

                                    password = Helper.stringInput(in, "Enter the password. Enter 0 to abort");

                                    if(password.equals("0"))
                                        break;

                                    dob = LocalDate.of(year, month, day);
                                    boolean registered = platform.register(name, email, password, dob, accType);


                                    if(registered)
                                    {
                                        System.out.println("You have successfully registered and been approved by our Exchange Platform Operator");
                                    }
                                    else
                                    {
                                        System.out.println("Oops registration failed! Please make sure you entered the correct details and that you're over 18 years of age");
                                    }


                                }break;
                                //login
                                case 2:
                                {
                                    //in.nextLine();
                                    Helper.flushEOL(in);
                                    email = Helper.stringInput(in, "Enter your email address");
                                    password = Helper.stringInput(in, "Enter your password");

                                    token = platform.login(email, password, accType);

                                    int userChoice =0;

                                    if(token!=null)
                                    {

                                        //Lister menu
                                        if(accType == 2)
                                        {
                                            do
                                            {
                                                String securityName;
                                                double securityPrice;
                                                String securityDesc;
                                                int securitySupply;

                                                System.out.println("Choose an option");
                                                System.out.println("1.\tList a Security");
                                                System.out.println("2.\tView my securities");
                                                System.out.println("3.\tView my balances");
                                                System.out.println("4.\tView my orders");
                                                System.out.println("5.\tCancel order");
                                                System.out.println("6.\tAdd supply and update price of security");
                                                System.out.println("7.\tSell a security");
                                                System.out.println("8.\tView Order Book");
                                                System.out.println("9.\tLog out");

                                                try {
                                                    userChoice = in.nextInt();

                                                    switch(userChoice)
                                                    {
                                                        //add securities
                                                        case 1:
                                                        {

                                                            //absorb the newline
                                                            Helper.flushEOL(in);

                                                            securityName = Helper.securityInputAdd(in, platform);

                                                            if(securityName.equals("0"))
                                                                break;

                                                            securityDesc = Helper.stringInput(in, "Enter a description for the security. Enter 0 to abort");

                                                            if(securityDesc.equals("0"))
                                                                break;

                                                            securityPrice = Helper.doubleInput(in, "Enter a price for the security. Enter 0 to abort", "Please input a number", 0);

                                                            if(securityPrice == 0)
                                                                break;

                                                            securitySupply = Helper.intInput(in, "Enter the total supply available for the security. Enter 0 to abort", "Please input a number", 0, 100000);

                                                            if(securitySupply == 0)
                                                                break;

                                                            boolean added = platform.addSecurity(token, securityName, securityDesc, securityPrice, securitySupply);

                                                            if(added)
                                                                System.out.println("Security added successfully");
                                                            else
                                                                System.out.println("Security failed to be added!");

                                                        }break;
                                                        //show securities
                                                        case 2:
                                                        {
                                                            platform.showUserSecurities(token);
                                                        }break;
                                                        //view balances
                                                        case 3:
                                                        {
                                                            platform.viewUserBalances(token, accType);
                                                        }break;
                                                        //view orders
                                                        case 4:
                                                        {
                                                            platform.viewUserOrders(token, accType);
                                                        }break;
                                                        //cancel orders
                                                        case 5:
                                                        {
                                                            //get sell orders
                                                            int numOfOrders = platform.getUserActiveOrders(token, 2, accType);

                                                            if(numOfOrders > 0)
                                                            {
                                                                //flush eol
                                                                Helper.flushEOL(in);
                                                                String orderToCancel;

                                                                orderToCancel = Helper.stringInput(in, "Enter order ID to cancel. Enter 0 to abort");

                                                                if(orderToCancel.equals("0"))
                                                                    break;

                                                                boolean cancelled = platform.cancelOrder(token, orderToCancel, 2, accType);

                                                                if(cancelled)
                                                                    System.out.println("Order cancelled successfully");
                                                                else
                                                                    System.out.println("Order cancellation failed, make sure you entered a correct id");
                                                            }
                                                        }break;
                                                        //Add supply and update price of security
                                                        case 6:
                                                        {
                                                            int securitiesAmt = platform.showUserSecurities(token);
                                                            String securityToUpdate;
                                                            int securityAmtToAdd;
                                                            double securityNewPrice;

                                                            //flush eol
                                                            Helper.flushEOL(in);

                                                            if(securitiesAmt > 0)
                                                            {

                                                                securityToUpdate = Helper.securityInputChoice(in, platform, "Enter the name of the security to update. Enter 0 to abort");

                                                                if(securityToUpdate.equals("0"))
                                                                    break;

                                                                securityAmtToAdd = Helper.intInput(in, "Enter the amount of supply to add to the chosen security. Enter 0 to cancel", "Please input a number", 0, 100000);

                                                                if(securityAmtToAdd == 0)
                                                                    break;

                                                                securityNewPrice = Helper.doubleInput(in, "Enter the new price of the new supply. Enter 0 to cancel", "Please input a number", 0);

                                                                if(securityNewPrice ==0)
                                                                    break;

                                                                boolean updated = platform.updateSecurity(token, securityToUpdate, securityAmtToAdd, securityNewPrice);

                                                                if(updated)
                                                                    System.out.println("Security updated");
                                                                else
                                                                    System.out.println("Security failed to update. Please make sure you entered a correct security name");

                                                            }


                                                        }break;
                                                        //Sell a security
                                                        case 7:
                                                        {

                                                            ArrayList<String> balances = platform.getUserIdleBalances(token);
                                                            String securityToSell;
                                                            double securityAmtToSell;
                                                            double securityPriceToSell;

                                                            if(balances.size() > 0)
                                                            {

                                                                //flush eol
                                                                Helper.flushEOL(in);

                                                                securityToSell = Helper.securityInputFromBalances(in, balances, "Enter the name of the security you want to buy. Enter 0 to cancel");

                                                                if(securityToSell.equals("0"))
                                                                    break;

                                                                securityAmtToSell = Helper.doubleInput(in, "Enter the amount of the security chosen to sell. Enter 0 to cancel", "Please input a number", 0);

                                                                if(securityAmtToSell == 0)
                                                                    break;

                                                                securityPriceToSell = Helper.doubleInput(in, "Enter the price at which you want to sell the chosen security. Enter 0 to cancel", "Please input a number", 0);

                                                                if(securityPriceToSell ==0)
                                                                    break;

                                                                boolean sold = platform.sell(token, securityToSell, securityAmtToSell, securityPriceToSell);
                                                                if(sold)
                                                                    System.out.println("Sell order made!");
                                                                else
                                                                    System.out.println("Sell order failed!");
                                                            }
                                                            else
                                                            {
                                                                System.out.println("No securities available to sell!");
                                                            }
                                                        }break;
                                                        //view order book
                                                        case 8:
                                                        {
                                                            int securitiesSize = platform.showListedSecurities();

                                                            String securityToView;


                                                            if(securitiesSize != 0) {
                                                                //flush eol
                                                                Helper.flushEOL(in);

                                                                securityToView = Helper.securityInputChoice(in, platform,"Enter the name of the security you want to analyse. Enter 0 to cancel");

                                                                if (securityToView.equals("0"))
                                                                    break;

                                                                platform.showSecurityOrders(securityToView);
                                                            }
                                                            else {
                                                                System.out.println("No orders available!");
                                                            }
                                                        }break;
                                                        //log out
                                                        case 9:
                                                        {
                                                            platform.logOut(token);
                                                            break;
                                                        }

                                                    }
                                                }catch(InputMismatchException e) {
                                                    System.out.println("Please input a correct number");
                                                    in.next();
                                                }
                                                catch (UserNotAuthenticated notAuthenticated)
                                                {
                                                    System.out.println("User not authenticated! Please log in again");
                                                    break;
                                                } catch (MoreThanBalanceException e) {
                                                    System.out.println("Not enough balance to perform action");
                                                } catch (UserNotAllowedException e) {
                                                    System.out.println("User not allowed to perform operation");
                                                    break;
                                                }
                                            }while(userChoice != 0 && userChoice != 9);
                                        }
                                        //Trader menu
                                        else if(accType == 1)
                                        {
                                            do
                                            {
                                                System.out.println("Choose an option");
                                                System.out.println("1.\tView my balances");
                                                System.out.println("2.\tView my orders");
                                                System.out.println("3.\tBuy a security");
                                                System.out.println("4.\tSell a security");
                                                System.out.println("5.\tCancel Order");
                                                System.out.println("6.\tView Order Book");
                                                System.out.println("7.\tLog out");

                                                try {
                                                    userChoice = in.nextInt();

                                                    switch(userChoice)
                                                    {
                                                        //view my balances
                                                        case 1:
                                                        {
                                                            platform.viewUserBalances(token, accType);
                                                        }break;
                                                        //view my orders
                                                        case 2:
                                                        {
                                                            platform.viewUserOrders(token, accType);
                                                        }break;
                                                        //buy a security
                                                        case 3:
                                                        {
                                                            int securitiesSize = platform.showListedSecurities();
                                                            String securityToBuy;
                                                            double securityAmtToBuy;
                                                            double securityPrice;


                                                            if(securitiesSize != 0)
                                                            {
                                                                //flush eol
                                                                Helper.flushEOL(in);

                                                                securityToBuy = Helper.securityInputChoice(in, platform, "Enter the name of the security you want to buy. Enter 0 to cancel");

                                                                if(securityToBuy.equals("0"))
                                                                    break;


                                                                securityAmtToBuy = Helper.doubleInput(in, "Enter the amount of the security chosen to buy. Enter 0 to cancel", "Please input a number", 0);

                                                                if(securityAmtToBuy == 0)
                                                                    break;

                                                                securityPrice = Helper.doubleInput(in, "Enter the price at which you want to buy the chosen security. Enter 0 to cancel", "Please input a number", 0);

                                                                if(securityPrice == 0)
                                                                    break;

                                                                boolean placed = platform.buy(token, securityToBuy, securityAmtToBuy, securityPrice);

                                                                if(placed)
                                                                    System.out.println("Order placed successfully");
                                                                else
                                                                    System.out.println("Order failed to send!");
                                                            }
                                                            else
                                                            {
                                                                System.out.println("No securities available to buy!");
                                                            }
                                                        }break;
                                                        //sell a security
                                                        case 4:
                                                        {
                                                            ArrayList<String> balances = platform.getUserIdleBalances(token);
                                                            String securityToSell;
                                                            double securityAmtToSell;
                                                            double securityPriceToSell;

                                                            if(balances.size() > 0)
                                                            {

                                                                //flush eol
                                                                Helper.flushEOL(in);

                                                                securityToSell = Helper.securityInputFromBalances(in, balances, "Enter the name of the security you want to buy. Enter 0 to cancel");

                                                                if(securityToSell.equals("0"))
                                                                    break;

                                                                securityAmtToSell = Helper.doubleInput(in, "Enter the amount of the security chosen to sell. Enter 0 to cancel", "Please input a number", 0);

                                                                if(securityAmtToSell == 0)
                                                                    break;

                                                                securityPriceToSell = Helper.doubleInput(in, "Enter the price at which you want to sell the chosen security. Enter 0 to cancel", "Please input a number", 0);

                                                                if(securityPriceToSell ==0)
                                                                    break;

                                                                boolean sold = platform.sell(token, securityToSell, securityAmtToSell, securityPriceToSell);
                                                                if(sold)
                                                                    System.out.println("Sell order made!");
                                                                else
                                                                    System.out.println("Sell order failed!");
                                                            }
                                                            else
                                                            {
                                                                System.out.println("No securities available to sell!");
                                                            }
                                                        }break;
                                                        //cancel order
                                                        case 5:
                                                        {
                                                            int typeOfOrder;
                                                            String orderToCancel;
                                                            int numOfOrders;
                                                            System.out.println("Choose order to cancel. Enter 0 to abort");
                                                            System.out.println("1.\tBuy Order");
                                                            System.out.println("2.\tSell Order");

                                                            typeOfOrder = Helper.chooseOrderType(in);

                                                            if(typeOfOrder ==0)
                                                                break;
                                                            else
                                                                numOfOrders = platform.getUserActiveOrders(token, typeOfOrder, accType);

                                                            if(numOfOrders > 0)
                                                            {
                                                                //flush eol
                                                                Helper.flushEOL(in);

                                                                orderToCancel = Helper.stringInput(in, "Enter order ID to cancel. Enter 0 to abort");

                                                                if(orderToCancel.equals("0"))
                                                                    break;

                                                                boolean cancelled = platform.cancelOrder(token, orderToCancel, typeOfOrder, accType);

                                                                if(cancelled)
                                                                    System.out.println("Order cancelled successfully");
                                                                else
                                                                    System.out.println("Order cancellation failed, make sure you entered a correct id");
                                                            }



                                                        }break;
                                                        //view order book
                                                        case 6:
                                                        {
                                                            int securitiesSize = platform.showListedSecurities();

                                                            String securityToView;

                                                            if(securitiesSize != 0) {
                                                                //flush eol
                                                                Helper.flushEOL(in);

                                                                securityToView = Helper.securityInputChoice(in, platform,"Enter the name of the security you want to analyse. Enter 0 to cancel");

                                                                if (securityToView.equals("0"))
                                                                    break;

                                                                platform.showSecurityOrders(securityToView);
                                                            }
                                                            else {
                                                                System.out.println("No orders available!");
                                                            }

                                                        }break;
                                                        //log out
                                                        case 7:
                                                        {
                                                            platform.logOut(token);
                                                            break;
                                                        }

                                                    }
                                                }catch(InputMismatchException e) {
                                                    System.out.println("Please input a correct number");
                                                    in.next();
                                                }
                                                catch (UserNotAuthenticated notAuthenticated)
                                                {
                                                    System.out.println("User not authenticated! Please log in again");
                                                    break;
                                                } catch (MoreThanBalanceException e) {
                                                    System.out.println("Not enough balance to perform action");
                                                } catch (UserNotAllowedException e) {
                                                    System.out.println("User not allowed to perform operation");
                                                    break;
                                                }
                                            }while(userChoice != 0 && userChoice != 7);
                                        }

                                    }

                                }break;
                                case 3:
                                {
                                    //set change type flag
                                    changeType = true;
                                    break;
                                }
                                case 4:
                                {
                                    accType = 4;
                                    break;

                                }
                            }

                            //if changetype flag, set choice to -1 to loop again
                            if(changeType)
                            {
                                choice = -1;
                                break;

                            }
                        }catch(NumberFormatException | InputMismatchException | IncorrectUserDetailException e)
                        {
                            System.out.println("Enter a correct number");
                            in.next();
                        }
                    }while(choice!=4);
                }
                else if(accType == 3)
                {

                    do{

                        System.out.println("----------Operator Menu-----------");

                        System.out.println("1.\tLogin");
                        System.out.println("2.\tChange account type");
                        System.out.println("3.\tQuit");

                        try{

                            choice = in.nextInt();
                            String name;
                            String email;
                            String password;
                            LocalDate dob;
                            boolean changeType = false;

                            switch(choice)
                            {
                                    //login
                                    case 1:
                                    {
                                        //in.nextLine();
                                        Helper.flushEOL(in);
                                        email = Helper.stringInput(in, "Enter your email address");
                                        password = Helper.stringInput(in, "Enter your password");

                                        token = platform.operatorLogin(email, password);

                                        int userChoice =0;

                                        if(token!=null)
                                        {

                                            do
                                            {
                                                String securityName;
                                                double securityPrice;
                                                String securityDesc;
                                                int securitySupply;

                                                System.out.println("Choose an option");
                                                System.out.println("1.\tApprove");
                                                System.out.println("2.\tCancel Order");
                                                System.out.println("3.\tChange Password");
                                                System.out.println("4.\tApprove All");
                                                System.out.println("5.\tLog Out");

                                                try {
                                                    userChoice = in.nextInt();

                                                    switch(userChoice)
                                                    {
                                                        //approve
                                                        case 1:
                                                        {

                                                            int usersAmt = platform.getUnapprovedUsers(token);
                                                            String userID;

                                                            if(usersAmt > 0)
                                                            {

                                                                //absorb the newline
                                                                Helper.flushEOL(in);

                                                                userID = Helper.stringInput(in, "Enter the id of the user to approve. Enter 0 to abort");

                                                                if(userID.equals("0"))
                                                                    break;

                                                                boolean approved = platform.approve(token, userID);

                                                                if(approved)
                                                                    System.out.println("User approved successfully");
                                                                else
                                                                    System.out.println("Failed to approve user. Please make sure you entered the id correctly.");

                                                            }
                                                        }break;
                                                        //cancel order
                                                        case 2:
                                                        {
                                                            int typeOfOrder;
                                                            String orderToCancel;
                                                            int numOfOrders;
                                                            System.out.println("Choose order to cancel. Enter 0 to abort");
                                                            System.out.println("1.\tBuy Order");
                                                            System.out.println("2.\tSell Order");

                                                            typeOfOrder = Helper.chooseOrderType(in);

                                                            if(typeOfOrder ==0)
                                                                break;
                                                            else
                                                                numOfOrders = platform.getUserActiveOrders(token, typeOfOrder, accType);

                                                            if(numOfOrders > 0)
                                                            {
                                                                //flush eol
                                                                Helper.flushEOL(in);

                                                                orderToCancel = Helper.stringInput(in, "Enter order ID to cancel. Enter 0 to abort");

                                                                if(orderToCancel.equals("0"))
                                                                    break;

                                                                boolean cancelled = platform.cancelOrder(token, orderToCancel, typeOfOrder, accType);

                                                                if(cancelled)
                                                                    System.out.println("Order cancelled successfully");
                                                                else
                                                                    System.out.println("Order cancellation failed, make sure you entered a correct id");
                                                            }
                                                        }break;
                                                        //change password
                                                        case 3:
                                                        {
                                                            //flush eol
                                                            Helper.flushEOL(in);

                                                            String newPass = Helper.stringInput(in, "Enter a new password. Enter 0 to cancel");

                                                            if(newPass.equals("0"))
                                                                break;

                                                            boolean changed = platform.changeAdminPassword(token, newPass);

                                                            if(changed)
                                                                System.out.println("Password changed successfully");
                                                            else
                                                                System.out.println("Failed to change password!");

                                                        }
                                                        //approve all
                                                        case 4:
                                                        {
                                                            boolean approved = platform.approveAll(token);

                                                            if(approved)
                                                                System.out.println("Users approved successfully");
                                                            else
                                                                System.out.println("Failed to approve users");

                                                        }break;
                                                        //log out
                                                        case 5:
                                                        {
                                                            platform.logOut(token);
                                                            break;
                                                        }

                                                    }
                                                }catch(InputMismatchException e) {
                                                    System.out.println("Please input a correct number");
                                                    in.next();
                                                }
                                                catch (UserNotAuthenticated notAuthenticated)
                                                {
                                                    System.out.println("User not authenticated! Please log in again");
                                                    break;
                                                }
                                            }while(userChoice != 0 && userChoice != 5);


                                        }
                                        else
                                        {
                                            System.out.println("Incorrect email or password.");
                                        }
                                    }break;
                                    //changeacctype
                                    case 2:
                                    {
                                        //set change type flag
                                        changeType = true;
                                        break;
                                    }
                                    case 3:
                                    {
                                        accType = 4;
                                        break;
                                    }
                                }

                            //if changetype flag, set choice to -1 to loop again
                            if(changeType)
                            {
                                choice = -1;
                                break;

                            }
                        }catch(NumberFormatException | InputMismatchException e)
                        {
                            System.out.println("Enter a correct number");
                            in.next();
                        }
                    }while(choice!=3);

                }
                else if(accType == 4)
                {
                    //view order book
                    int securitiesSize = platform.showListedSecurities();

                    String securityToView;


                    if(securitiesSize != 0) {
                        //flush eol
                        Helper.flushEOL(in);

                        securityToView = Helper.securityInputChoice(in, platform,"Enter the name of the security you want to analyse. Enter 0 to cancel");

                        if (securityToView.equals("0"))
                            break;

                        platform.showSecurityOrders(securityToView);
                    }
                    else {
                        System.out.println("No orders available!");
                    }
                }
            }catch(InputMismatchException e) {
                System.out.println("Please input a number");
                in.next();
            }

        }while(((accType== 1 || accType==2 ) && choice != 5) || (accType ==3  && choice!=3) || accType!=5);

    }
}