package order;

import java.sql.Timestamp;
import java.util.UUID;

public class Order {
    private String trader;
    private String security;
    private double quantity;
    private double price;
    private Timestamp timestamp;
    private Status status;
    private String id;
    private double quantityAvailable;

    //constructor
    public Order(String trader, double quantity, double price, Timestamp timestamp, Status status, String security, double quantityAvailable)
    {
        this.id = UUID.randomUUID().toString();
        this.trader = trader;
        this.quantity = quantity;
        this.price = price;
        this.timestamp = timestamp;
        this.status = status;
        this.security = security;
        this.quantityAvailable = quantityAvailable;
    }

    /**
     * Getter for id
     * @return id
     */
    public String getId(){
        return id;
    }

    /**
     * Getter for trader email
     * @return email of trader
     */
    public String getTrader() {
        return trader;
    }

    /**
     * Setter for trader
     * @param trader trader's email
     */
    public void setTrader(String trader) {
        this.trader = trader;
    }

    /**
     * Getter for security
     * @return security's name
     */
    public String getSecurity() {
        return security;
    }

    /**
     * Getter for quantity
     * @return quantity
     */
    public double getQuantity()
    {
        return quantity;
    }

    /**
     * Setter for quantity
     * @param quantity quantity to add
     */
    public void setQuantity(double quantity) {
        this.quantity = quantity;
    }

    /**
     * Setter for price
     * @param price price to set
     */
    public void setPrice(double price) {
        this.price = price;
    }

    /**
     * Getter for timestamp
     * @return timestamp
     */
    public Timestamp getTimestamp()
    {
        return timestamp;
    }

    /**
     * Getter for status
     * @return status
     */
    public Status getStatus()
    {
        return status;
    }

    /**
     * Setter for status
     * @param status to set
     */
    public void setStatus(Status status)
    {
        this.status = status;
    }

    /**
     * Getter for quantity avaialble in order
     * @return quantity available
     */
    public double getQuantityAvailable()
    {
        return quantityAvailable;
    }

    /**
     * Method to reduce the quantity available
     * @param val to reduce
     */
    public void reduceQuantityAvailable(double val)
    {
        if(quantityAvailable < val)
            quantityAvailable = 0;
        else
            quantityAvailable -= val;

    }

    /**
     * Setter for quantitiy available
     * @param quantityAvailable to set
     */
    public void setQuantityAvailable(double quantityAvailable)
    {
        this.quantityAvailable = quantityAvailable;
    }

    /**
     * Getter for price
     * @return price
     */
    public double getPrice()
    {
        return price;
    }
}
