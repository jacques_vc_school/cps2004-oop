package order;

import java.sql.Timestamp;

public class BuyOrder extends Order {

    //constructor
    public BuyOrder(String trader, double quantity, double price, Timestamp timestamp, Status status, String security, double quantityAvailable)
    {
        super(trader, quantity, price, timestamp, status, security, quantityAvailable);
    }
}
