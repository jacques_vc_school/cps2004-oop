package order;

public enum Status {
    CANCELLED,
    FULFILLED,
    IN_PROGRESS
}