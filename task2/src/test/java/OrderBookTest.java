import exceptions.MoreThanBalanceException;
import order.BuyOrder;
import order.SellOrder;
import order.Status;
import org.junit.Test;
import platform.OrderBook;
import user.Trader;

import java.sql.Timestamp;
import java.time.LocalDate;
import java.util.Date;

import static org.junit.Assert.*;

public class OrderBookTest {
    OrderBook orderBook = new OrderBook();

    String traderName = "Alex";
    String traderEmail = "alex@gmail.com";
    String traderPassword = "try2";
    LocalDate traderDob = LocalDate.of(2000,1,1);
    Trader trader = new Trader(traderEmail, traderEmail, traderPassword, traderDob);



    /**
     * Test for placeBuyOrder
     */
    @Test
    public void testPlaceBuyOrder()
    {
        BuyOrder buyOrder = new BuyOrder(trader.getEmail(), 10, 5, new Timestamp(new Date().getTime()), Status.IN_PROGRESS, "sec1", 10);
        orderBook.placeBuyOrder(buyOrder);
        BuyOrder buyOrder2 = new BuyOrder(trader.getEmail(), 10, 10, new Timestamp(new Date().getTime()), Status.IN_PROGRESS, "sec1", 10);
        orderBook.placeBuyOrder(buyOrder2);
        BuyOrder buyOrder3 = new BuyOrder(trader.getEmail(), 10, 10, new Timestamp(new Date().getTime()), Status.IN_PROGRESS, "sec1", 10);
        orderBook.placeBuyOrder(buyOrder3);

        assertEquals("Asserting first placed order", true, orderBook.getBuyOrdersUnmodifiable().get(buyOrder.getSecurity()).get(buyOrder.getPrice()).contains(buyOrder));
        assertEquals("Asserting second placed order", true, orderBook.getBuyOrdersUnmodifiable().get(buyOrder2.getSecurity()).get(buyOrder2.getPrice()).contains(buyOrder2));
        assertEquals("Asserting third placed order", true, orderBook.getBuyOrdersUnmodifiable().get(buyOrder3.getSecurity()).get(buyOrder3.getPrice()).contains(buyOrder3));
        assertEquals("Asserting first placed order", true, orderBook.getBuyOrders().get(buyOrder.getSecurity()).get(buyOrder.getPrice()).contains(buyOrder));
        assertEquals("Asserting second placed order", true, orderBook.getBuyOrders().get(buyOrder2.getSecurity()).get(buyOrder2.getPrice()).contains(buyOrder2));
        assertEquals("Asserting third placed order", true, orderBook.getBuyOrders().get(buyOrder3.getSecurity()).get(buyOrder3.getPrice()).contains(buyOrder3));

    }

    /**
     * Test for placeSellOrder
     */
    @Test
    public void testPlaceSellOrder()
    {
        SellOrder sellOrder = new SellOrder(trader.getEmail(), 10, 5, new Timestamp(new Date().getTime()), Status.IN_PROGRESS, "sec1", 10);
        orderBook.placeSellOrder(sellOrder);
        SellOrder sellOrder2 = new SellOrder(trader.getEmail(), 10, 10, new Timestamp(new Date().getTime()), Status.IN_PROGRESS, "sec1", 10);
        orderBook.placeSellOrder(sellOrder2);
        SellOrder sellOrder3 = new SellOrder(trader.getEmail(), 10, 10, new Timestamp(new Date().getTime()), Status.IN_PROGRESS, "sec1", 10);
        orderBook.placeSellOrder(sellOrder3);

        assertEquals("Asserting first placed order", true, orderBook.getSellOrdersUnmodifiable().get(sellOrder.getSecurity()).get(sellOrder.getPrice()).contains(sellOrder));
        assertEquals("Asserting second placed order", true, orderBook.getSellOrdersUnmodifiable().get(sellOrder2.getSecurity()).get(sellOrder2.getPrice()).contains(sellOrder2));
        assertEquals("Asserting third placed order", true, orderBook.getSellOrdersUnmodifiable().get(sellOrder3.getSecurity()).get(sellOrder3.getPrice()).contains(sellOrder3));
        assertEquals("Asserting first placed order", true, orderBook.getSellOrders().get(sellOrder.getSecurity()).get(sellOrder.getPrice()).contains(sellOrder));
        assertEquals("Asserting second placed order", true, orderBook.getSellOrders().get(sellOrder2.getSecurity()).get(sellOrder2.getPrice()).contains(sellOrder2));
        assertEquals("Asserting third placed order", true, orderBook.getSellOrders().get(sellOrder3.getSecurity()).get(sellOrder3.getPrice()).contains(sellOrder3));

    }

    /**
     * testing show all orders
     */
    @Test
    public void testShowAllOrders() {
        //show active buy orders when there are no orders
        //show active sell orders when there are no orders
        orderBook.showAllOrders("sec1");


        //add buy orders and sell orders
        BuyOrder buyOrder = new BuyOrder(trader.getEmail(), 10, 5, new Timestamp(new Date().getTime()), Status.IN_PROGRESS, "sec1", 10);
        SellOrder sellOrder = new SellOrder(trader.getEmail(), 10, 5, new Timestamp(new Date().getTime()), Status.IN_PROGRESS, "sec1", 10);
        BuyOrder buyOrder2 = new BuyOrder(trader.getEmail(), 10, 5, new Timestamp(new Date().getTime()), Status.IN_PROGRESS, "sec1", 10);
        SellOrder sellOrder2 = new SellOrder(trader.getEmail(), 10, 5, new Timestamp(new Date().getTime()), Status.IN_PROGRESS, "sec1", 10);
        orderBook.placeBuyOrder(buyOrder);
        orderBook.placeSellOrder(sellOrder);
        orderBook.placeBuyOrder(buyOrder2);
        orderBook.placeSellOrder(sellOrder2);

        //show active buy orders when there are orders
        orderBook.showAllOrders("sec1");
        //show active sell orders when there are orders
        orderBook.showAllOrders("sec1");


    }

    /**
     * testing get all orders
     */
    @Test
    public void testGetAllOrders() {

        //asserting normal orders when there are no orders
        assertEquals("Asserting size of orders returned", 0, orderBook.getAllOrders("sec1").size());
        //asserting unmodifiable orders when there are no orders
        assertEquals("Asserting size of orders returned", 0, orderBook.getAllOrdersUnmodifiable("sec1").size());

        //add buy orders and sell orders
        BuyOrder buyOrder = new BuyOrder(trader.getEmail(), 10, 5, new Timestamp(new Date().getTime()), Status.IN_PROGRESS, "sec1", 10);
        SellOrder sellOrder = new SellOrder(trader.getEmail(), 10, 5, new Timestamp(new Date().getTime()), Status.IN_PROGRESS, "sec1", 10);
        BuyOrder buyOrder2 = new BuyOrder(trader.getEmail(), 10, 5, new Timestamp(new Date().getTime()), Status.IN_PROGRESS, "sec1", 10);
        SellOrder sellOrder2 = new SellOrder(trader.getEmail(), 10, 5, new Timestamp(new Date().getTime()), Status.IN_PROGRESS, "sec1", 10);
        orderBook.placeBuyOrder(buyOrder);
        orderBook.placeSellOrder(sellOrder);
        orderBook.placeBuyOrder(buyOrder2);
        orderBook.placeSellOrder(sellOrder2);

        //asserting normal orders
        assertEquals("Asserting size of orders returned", 4, orderBook.getAllOrders("sec1").size());
        //asserting unmodifiable orders
        assertEquals("Asserting size of orders returned", 4, orderBook.getAllOrdersUnmodifiable("sec1").size());


    }

    /**
     * testing showallorders just for coverage for sell only
     */
    @Test
    public void testShowAllOrdersSell() {
        SellOrder sellOrder = new SellOrder(trader.getEmail(), 10, 5, new Timestamp(new Date().getTime()), Status.IN_PROGRESS, "sec1", 10);

        orderBook.placeSellOrder(sellOrder);
        orderBook.showAllOrders("sec1");
    }

    /**
     * testing showallorders just for coverage for sell only
     */
    @Test
    public void testShowAllOrdersBuy() {
        BuyOrder buyOrder = new BuyOrder(trader.getEmail(), 10, 5, new Timestamp(new Date().getTime()), Status.IN_PROGRESS, "sec1", 10);

        orderBook.placeBuyOrder(buyOrder);
        orderBook.showAllOrders("sec1");
    }

    /**
     * testing cancelorder with empty orders
     */
    @Test
    public void testCancelOrderEmpty() {

        BuyOrder buyOrder = new BuyOrder(trader.getEmail(), 10, 1, new Timestamp(new Date().getTime()), Status.IN_PROGRESS, "sec1", 10);
        SellOrder sellOrder = new SellOrder(trader.getEmail(), 10, 1, new Timestamp(new Date().getTime()), Status.IN_PROGRESS, "sec1", 10);

        assertEquals("Asserting false return from cancel order", false, orderBook.cancelOrder(1, buyOrder.getId()));

        assertEquals("Asserting false return from cancel order", false, orderBook.cancelOrder(2, sellOrder.getId()));

    }

    /**
     * testing cancelorder for buyorders
     */
    @Test
    public void testCancelOrderBuyOrders() {

        BuyOrder buyOrder = new BuyOrder(trader.getEmail(), 10, 1, new Timestamp(new Date().getTime()), Status.IN_PROGRESS, "sec1", 10);
        orderBook.placeBuyOrder(buyOrder);
        BuyOrder buyOrder1 = new BuyOrder(trader.getEmail(), 10, 1, new Timestamp(new Date().getTime()), Status.IN_PROGRESS, "sec1", 10);
        orderBook.placeBuyOrder(buyOrder1);
        BuyOrder buyOrder2 = new BuyOrder(trader.getEmail(), 20, 1, new Timestamp(new Date().getTime()), Status.IN_PROGRESS, "sec2", 10);
        orderBook.placeBuyOrder(buyOrder2);
        BuyOrder buyOrder3 = new BuyOrder(trader.getEmail(), 10, 1, new Timestamp(new Date().getTime()), Status.IN_PROGRESS, "sec3", 10);
        orderBook.placeBuyOrder(buyOrder3);
        orderBook.cancelOrder(1, buyOrder3.getId());
        assertEquals("Asserting order status", Status.CANCELLED, orderBook.getBuyOrdersUnmodifiable().get(buyOrder3.getSecurity()).get(buyOrder3.getPrice()).get(0).getStatus());
        assertEquals("Asserting order status", Status.CANCELLED, orderBook.getBuyOrders().get(buyOrder3.getSecurity()).get(buyOrder3.getPrice()).get(0).getStatus());

    }

    /**
     * testing cancelorder for sellorders
     */
    @Test
    public void testCancelOrderSellOrders() {

        SellOrder sellOrder = new SellOrder(trader.getEmail(), 10, 1, new Timestamp(new Date().getTime()), Status.IN_PROGRESS, "sec1", 10);
        orderBook.placeSellOrder(sellOrder);
        SellOrder sellOrder1 = new SellOrder(trader.getEmail(), 10, 1, new Timestamp(new Date().getTime()), Status.IN_PROGRESS, "sec1", 10);
        orderBook.placeSellOrder(sellOrder1);
        SellOrder sellOrder2 = new SellOrder(trader.getEmail(), 20, 1, new Timestamp(new Date().getTime()), Status.IN_PROGRESS, "sec2", 10);
        orderBook.placeSellOrder(sellOrder2);
        SellOrder sellOrder3 = new SellOrder(trader.getEmail(), 10, 1, new Timestamp(new Date().getTime()), Status.IN_PROGRESS, "sec3", 10);
        orderBook.placeSellOrder(sellOrder3);

        orderBook.cancelOrder(2, sellOrder3.getId());
        assertEquals("Asserting order status", Status.CANCELLED, orderBook.getSellOrdersUnmodifiable().get(sellOrder3.getSecurity()).get(sellOrder3.getPrice()).get(0).getStatus());
        assertEquals("Asserting order status", Status.CANCELLED, orderBook.getSellOrders().get(sellOrder3.getSecurity()).get(sellOrder3.getPrice()).get(0).getStatus());



    }

    /**
     * testing cancelOrder for incorrect type -for coverage
     */
    @Test
    public void testCancelOrderIncorrectType() {

        SellOrder sellOrder = new SellOrder(trader.getEmail(), 10, 1, new Timestamp(new Date().getTime()), Status.IN_PROGRESS, "sec1", 10);
        orderBook.placeSellOrder(sellOrder);

        //call cancelOrder with a type not equal to 1 or 2
        orderBook.cancelOrder(3, sellOrder.getId());

    }

    /**
     * testing getOrderMaker with empty orders
     */
    @Test
    public void testGetOrderMakerEmpty() {

        BuyOrder buyOrder = new BuyOrder(trader.getEmail(), 10, 1, new Timestamp(new Date().getTime()), Status.IN_PROGRESS, "sec1", 10);
        SellOrder sellOrder = new SellOrder(trader.getEmail(), 10, 1, new Timestamp(new Date().getTime()), Status.IN_PROGRESS, "sec1", 10);

        assertNull("Asserting false return from getordermaker", orderBook.getOrderMaker(1, buyOrder.getId()));

        assertNull("Asserting false return from getordermaker", orderBook.getOrderMaker(2, sellOrder.getId()));

    }

    /**
     * testing getOrderMaker for buyorders
     */
    @Test
    public void testGetOrderMakerBuyOrders() {

        BuyOrder buyOrder = new BuyOrder(trader.getEmail(), 10, 1, new Timestamp(new Date().getTime()), Status.IN_PROGRESS, "sec1", 10);
        orderBook.placeBuyOrder(buyOrder);
        BuyOrder buyOrder1 = new BuyOrder(trader.getEmail(), 10, 1, new Timestamp(new Date().getTime()), Status.IN_PROGRESS, "sec1", 10);
        orderBook.placeBuyOrder(buyOrder1);
        BuyOrder buyOrder2 = new BuyOrder(trader.getEmail(), 20, 1, new Timestamp(new Date().getTime()), Status.IN_PROGRESS, "sec2", 10);
        orderBook.placeBuyOrder(buyOrder2);
        BuyOrder buyOrder3 = new BuyOrder(trader.getEmail(), 10, 1, new Timestamp(new Date().getTime()), Status.IN_PROGRESS, "sec3", 10);
        orderBook.placeBuyOrder(buyOrder3);
        assertEquals("Asserting order maker", trader.getEmail(), orderBook.getOrderMaker(1, buyOrder.getId()));


    }

    /**
     * testing getOrderMaker for sellorders
     */
    @Test
    public void testGetOrderMakerSellOrders() {

        SellOrder sellOrder = new SellOrder(trader.getEmail(), 10, 1, new Timestamp(new Date().getTime()), Status.IN_PROGRESS, "sec1", 10);
        orderBook.placeSellOrder(sellOrder);
        SellOrder sellOrder1 = new SellOrder(trader.getEmail(), 10, 1, new Timestamp(new Date().getTime()), Status.IN_PROGRESS, "sec1", 10);
        orderBook.placeSellOrder(sellOrder1);
        SellOrder sellOrder2 = new SellOrder(trader.getEmail(), 20, 1, new Timestamp(new Date().getTime()), Status.IN_PROGRESS, "sec2", 10);
        orderBook.placeSellOrder(sellOrder2);
        SellOrder sellOrder3 = new SellOrder(trader.getEmail(), 10, 1, new Timestamp(new Date().getTime()), Status.IN_PROGRESS, "sec3", 10);
        orderBook.placeSellOrder(sellOrder3);

        assertEquals("Asserting order maker", trader.getEmail(), orderBook.getOrderMaker(2, sellOrder.getId()));



    }

    /**
     * testing getOrderMaker for incorrect type -for coverage
     */
    @Test
    public void testGetOrderMakerIncorrectType() {

        SellOrder sellOrder = new SellOrder(trader.getEmail(), 10, 1, new Timestamp(new Date().getTime()), Status.IN_PROGRESS, "sec1", 10);
        orderBook.placeSellOrder(sellOrder);

        //call cancelOrder with a type not equal to 1 or 2
        orderBook.getOrderMaker(3, sellOrder.getId());

    }



}