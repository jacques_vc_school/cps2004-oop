import order.Order;
import order.Status;
import org.junit.Test;
import security.Security;

import java.sql.Timestamp;
import java.util.Date;

import static org.junit.Assert.*;

public class SecurityTest {

    String name = "sec1";
    String desc = "just a security";
    double price = 10;
    int totalSupply = 100;
    Security security = new Security(name, desc, price, totalSupply);

    /**
     * To test security getters
     */
    @Test
    public void testGetters()
    {
        assertEquals("Asserting name", name, security.getName());
        assertEquals("Asserting desc", desc, security.getDesc());
        assertEquals(price, security.getPrice(), 0.001);
        assertEquals("Asserting total supply", totalSupply, security.getTotalSupply());

    }

    /**
     * To test setPrice
     */
    @Test
    public void testSetPrice()
    {
        double newPrice = 25;
        security.setPrice(newPrice);
        assertEquals(newPrice, security.getPrice(), 0.001);

    }

    /**
     * To test incrementSupply
     */
    @Test
    public void testIncremenSupply()
    {
        int toAdd = 10;
        int original = security.getTotalSupply();
        security.incrementSupply(toAdd);
        assertEquals(original + toAdd, security.getTotalSupply(), 0.001);

    }


}