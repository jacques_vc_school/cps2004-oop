import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class)

@Suite.SuiteClasses({
        OrderBookTest.class,
        OrderTest.class,
        PlatformTest.class,
        SecurityTest.class,
        UserTest.class
})

public class TestSuite {
}