import exceptions.MoreThanBalanceException;
import order.BuyOrder;
import order.Order;
import order.SellOrder;
import order.Status;
import org.junit.Test;
import security.Security;
import user.ExchangeOperator;
import user.Lister;
import user.Trader;

import java.sql.Timestamp;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

import static org.junit.Assert.*;

public class UserTest {

    String listerName = "Joe";
    String listerEmail = "joe@gmail.com";
    String listerPassword = "try";
    LocalDate listerDob = LocalDate.of(2000,1,1);

    String traderName = "Alex";
    String traderEmail = "alex@gmail.com";
    String traderPassword = "try2";
    LocalDate traderDob = LocalDate.of(2000,1,1);

    Lister lister = new Lister(listerName, listerEmail, listerPassword, listerDob);
    Trader trader = new Trader(traderEmail, traderEmail, traderPassword, traderDob);


    //just for coverage
    Lister lister2 = new Lister();
    Trader trader2 = new Trader();
    ExchangeOperator operator = new ExchangeOperator();


    /**
     * To test user getters
     */
    @Test
    public void testGetters()
    {
        assertEquals("Asserting email", listerEmail, lister.getEmail());
        assertEquals("Asserting id", lister.getId(), lister.getId());
    }

    /**
     * To test user authenticate
     */
    @Test
    public void testAuth()
    {
        assertEquals("Asserting correct password and email", true, lister.authenticate(listerEmail, listerPassword));
        assertEquals("Asserting incorrect password and email", false, lister.authenticate(traderEmail, traderPassword));
        assertEquals("Asserting incorrect password", false, lister.authenticate(listerEmail, traderPassword));
        assertEquals("Asserting incorrect email", false, lister.authenticate(traderEmail, listerPassword));
    }

    /**
     * To test user topup
     */
    @Test
    public void testTopUp()
    {

        double topUpAmt = 50;
        lister.topUp(topUpAmt);
        assertEquals(topUpAmt, (double)lister.getBalances().get("Euro"), 0.001);
    }

    /**
     * To test user increment euro balance
     */
    @Test
    public void testIncrementEuroBalance()
    {

        double topUpAmt = 50;
        lister.topUp(topUpAmt);
        lister.incrementEuroBalance(20);
        assertEquals(topUpAmt + 20, (double)lister.getBalances().get("Euro"), 0.001);
    }

    /**
     * To test user add sell order and get sell order balances
     */
    @Test
    public void testAddSellOrderAndSellOrderBalances()
    {
        SellOrder sellOrder = new SellOrder(trader.getEmail(), 10, 5, new Timestamp(new Date().getTime()), Status.IN_PROGRESS, "sec1", 10);
        trader.addSellOrder(sellOrder);

        assertEquals("Asserting sell order balance", sellOrder, trader.getSellOrders().get(sellOrder.getId()));
    }

    /**
     * To test user increment balance and get balance
     */
    @Test
    public void testIncrementBalanceAndGetBalances()
    {
        double firstVal = 20;

        trader.incrementBalance("sec1", firstVal);
        assertEquals(firstVal, trader.getIdleBalances().get("sec1"), 0.001);
        assertEquals(firstVal, trader.getIdleBalancesUnmodifiable().get("sec1"), 0.001);
        double secondVal = 30;
        trader.incrementBalance("sec1", secondVal);
        assertEquals(firstVal + secondVal, trader.getIdleBalances().get("sec1"), 0.001);
        assertEquals(firstVal + secondVal, trader.getIdleBalancesUnmodifiable().get("sec1"), 0.001);

    }

    /**
     * To test incrementEuroBalance when it does not exist
     */
    @Test
    public void testIncrementEuroBalanceNoExist()
    {
        double firstVal = 20;
        trader.incrementEuroBalance(firstVal);

        assertEquals(firstVal, trader.getIdleBalances().get("Euro"), 0.001);

    }

    /**
     * To test user decrement balance and get balance
     */
    @Test
    public void testDecrementBalanceAndGetBalances() throws MoreThanBalanceException {
        double firstVal = 20;
        trader.incrementBalance("sec1", firstVal);
        double reduceVal = 10;
        trader.reduceBalance("sec1", reduceVal);
        assertEquals(firstVal - reduceVal, trader.getIdleBalances().get("sec1"), 0.001);

        trader.reduceBalance("sec1", reduceVal);
        assertEquals("Asserting that balance is removed if it is fully reduced", false, trader.getBalances().containsKey("sec1"));

    }

    /**
     * To test user decrement balance with more than available
     */
    @Test(expected = MoreThanBalanceException.class)
    public void testDecrementMoreThanBalance() throws MoreThanBalanceException {
        double firstVal = 20;
        trader.incrementBalance("sec1", firstVal);
        double reduceVal = 40;
        trader.reduceBalance("sec1", reduceVal);

    }

    /**
     * To test user decrement balance of unexistent balance
     */
    @Test(expected = MoreThanBalanceException.class)
    public void testDecrementUnexistentBalance() throws MoreThanBalanceException {
        double reduceVal = 40;
        trader.reduceBalance("sec2", reduceVal);

    }

    /**
     * To test user reduce euro balance
     */
    @Test
    public void testReduceEuroBalance() throws MoreThanBalanceException {
        double firstVal = 20;
        trader.incrementBalance("Euro", firstVal);
        double reduceVal = 10;
        trader.reduceEuroBalance(reduceVal);
        assertEquals(firstVal - reduceVal, trader.getIdleBalances().get("Euro"), 0.001);

    }

    /**
     * To test user reduce euro balance with more than available
     */
    @Test(expected = MoreThanBalanceException.class)
    public void testReduceEuroBalanceMoreThanBalance() throws MoreThanBalanceException {
        double firstVal = 20;
        trader.incrementBalance("Euro", firstVal);
        double reduceVal = 40;
        trader.reduceEuroBalance(reduceVal);

    }

    /**
     * To test user decrement eurp balance of unexistent balance
     */
    @Test(expected = MoreThanBalanceException.class)
    public void testDecrementUnexistentEuroBalance() throws MoreThanBalanceException {
        double reduceVal = 40;
        lister.reduceEuroBalance(reduceVal);

    }

    /**
     * To test user show idle balances
     */
    @Test
    public void testShowIdleBalance() {
        double firstVal = 20;
        ArrayList<String> balances = new ArrayList<String>();
        trader.incrementBalance("sec1", firstVal);
        trader.incrementBalance("Euro", firstVal);
        trader.incrementBalance("sec2", firstVal);
        balances.add("sec2");
        balances.add("sec1");


        assertEquals("Asserting balances returned", balances, trader.showIdleBalances());
    }

    /**
     * To test user show empty idle balances
     */
    @Test
    public void testShowEmptyIdleBalance() {
        ArrayList<String> balances = new ArrayList<String>();

        assertEquals("Asserting balances returned", balances, trader.showIdleBalances());
    }

    /**
     * To test user get idle balances
     */
    @Test
    public void testGetIdleBalance() {
        double firstVal = 20;
        HashMap<String, Double> balances = new HashMap<String, Double>();
        trader.incrementBalance("sec1", firstVal);
        trader.incrementBalance("sec2", firstVal);
        balances.put("sec2", firstVal);
        balances.put("sec1", firstVal);


        assertEquals("Asserting balances returned", balances, trader.getIdleBalances());
    }

    /**
     * Just for coverage
     */
    @Test
    public void testSetId() {
        trader.setId();
        assertEquals("Asserting ids", trader.getId(), trader.getId());
    }


    /**
     * testing update balance
     */
    @Test
    public void testUpdateBalance() {
        double firstVal = 20;
        double secondVal = 50;
        trader.incrementBalance("sec1", firstVal);
        trader.updateBalance("sec1", secondVal);
        assertEquals(secondVal,  trader.getBalances().get("sec1"), 0.001);

        trader.updateBalance("sec1", 0.0);
        assertEquals("Asserting balance is removed if update by 0", false, trader.getBalances().containsKey("sec1"));
    }

    /**
     * testing setPassword
     */
    @Test
    public void testsetPassword() {
        String newPass = "hello";
        trader.setPassword(newPass);
        assertEquals("Asserting password", true,  trader.authenticate(traderEmail, newPass));

    }

    /**
     * To test user increment sell order balance
     */
    @Test
    public void testIncrementSellOrderBalance()
    {
        double firstVal = 20;
        trader.incrementSellOrderBalances("sec1", firstVal);
        assertEquals(firstVal, trader.getSellOrdersBalances().get("sec1"), 0.001);
        assertEquals(firstVal, trader.getSellOrdersBalancesUnmodifiable().get("sec1"), 0.001);
        double secondVal = 30;
        trader.incrementSellOrderBalances("sec1", secondVal);
        assertEquals(firstVal + secondVal, trader.getSellOrdersBalances().get("sec1"), 0.001);
        assertEquals(firstVal + secondVal, trader.getSellOrdersBalancesUnmodifiable().get("sec1"), 0.001);

    }

    /**
     * To test increment sell order balance when it does not exist
     */
    @Test
    public void testIncrementSellOrderBalanceNoExist()
    {
        double firstVal = 20;
        trader.incrementSellOrderBalances("sec1", firstVal);

        assertEquals(firstVal, trader.getSellOrdersBalances().get("sec1"), 0.001);

    }

    /**
     * testing update sell order balance
     */
    @Test
    public void testUpdateSellOrderBalance() {
        double firstVal = 20;
        double secondVal = 50;
        trader.incrementSellOrderBalances("sec1", firstVal);
        trader.updateSellOrderBalances("sec1", secondVal);
        assertEquals(secondVal,  trader.getSellOrdersBalances().get("sec1"), 0.001);

        trader.updateSellOrderBalances("sec1", 0.0);
        assertEquals("Asserting balance is removed if update by 0", false, trader.getSellOrdersBalances().containsKey("sec1"));
    }

    /**
     * To test user add buy order and get buy order balances
     */
    @Test
    public void testAddBuyOrderAndBuyOrderBalances()
    {
        BuyOrder buyOrder = new BuyOrder(trader.getEmail(), 10, 5, new Timestamp(new Date().getTime()), Status.IN_PROGRESS, "sec1", 10);
        trader.addBuyOrder(buyOrder);

        assertEquals("Asserting buy order balance", buyOrder, trader.getBuyOrders().get(buyOrder.getId()));
        assertEquals("Asserting buy order balance", buyOrder, trader.getBuyOrdersUnmodifiable().get(buyOrder.getId()));
    }

    /**
     * To test user increment buy order balance
     */
    @Test
    public void testIncrementBuyOrderBalance()
    {
        double firstVal = 20;
        trader.incrementBuyOrderBalances("sec1", firstVal);
        assertEquals(firstVal, trader.getBuyOrdersBalances().get("sec1"), 0.001);
        assertEquals(firstVal, trader.getBuyOrdersBalancesUnmodifiable().get("sec1"), 0.001);
        double secondVal = 30;
        trader.incrementBuyOrderBalances("sec1", secondVal);
        assertEquals(firstVal + secondVal, trader.getBuyOrdersBalances().get("sec1"), 0.001);
        assertEquals(firstVal + secondVal, trader.getBuyOrdersBalancesUnmodifiable().get("sec1"), 0.001);

    }

    /**
     * To test increment buy order balance when it does not exist
     */
    @Test
    public void testIncrementBuyOrderBalanceNoExist()
    {
        double firstVal = 20;
        trader.incrementBuyOrderBalances("sec1", firstVal);

        assertEquals(firstVal, trader.getBuyOrdersBalances().get("sec1"), 0.001);
        assertEquals(firstVal, trader.getBuyOrdersBalancesUnmodifiable().get("sec1"), 0.001);
    }

    /**
     * testing update buy order balance
     */
    @Test
    public void testUpdateBuyOrderBalance() {
        double firstVal = 20;
        double secondVal = 50;
        trader.incrementBuyOrderBalances("sec1", firstVal);
        trader.updateBuyOrderBalances("sec1", secondVal);
        assertEquals(secondVal,  trader.getBuyOrdersBalances().get("sec1"), 0.001);

        trader.updateBuyOrderBalances("sec1", 0.0);
        assertEquals("Asserting balance is removed if update by 0", false, trader.getBuyOrdersBalances().containsKey("sec1"));
    }

    /**
     * testing viewmyorders just for coverage
     */
    @Test
    public void testViewMyOrders() {
        trader.viewMyOrders();
        BuyOrder buyOrder = new BuyOrder(trader.getEmail(), 10, 5, new Timestamp(new Date().getTime()), Status.IN_PROGRESS, "sec1", 10);
        SellOrder sellOrder = new SellOrder(trader.getEmail(), 10, 5, new Timestamp(new Date().getTime()), Status.IN_PROGRESS, "sec1", 10);

        trader.addBuyOrder(buyOrder);
        trader.addSellOrder(sellOrder);
        trader.viewMyOrders();
    }

    /**
     * testing viewmyorders with only buy orders
     */
    @Test
    public void testViewMyOrdersOnlyBuy() {
        BuyOrder buyOrder = new BuyOrder(trader.getEmail(), 10, 5, new Timestamp(new Date().getTime()), Status.IN_PROGRESS, "sec1", 10);

        trader.addBuyOrder(buyOrder);
        trader.viewMyOrders();
    }

    /**
     * testing viewmyorders with only sell orders
     */
    @Test
    public void testViewMyOrdersOnlySell() {
        SellOrder sellOrder = new SellOrder(trader.getEmail(), 10, 5, new Timestamp(new Date().getTime()), Status.IN_PROGRESS, "sec1", 10);

        trader.addSellOrder(sellOrder);
        trader.viewMyOrders();
    }

    /**
     * testing viewmybalances
     */
    @Test
    public void testViewMyBalancesTrader() {
        //view balances with no balances
        trader.viewMyBalances();
        trader.incrementBalance("sec1", 10.0);
        trader.incrementBuyOrderBalances("sec1", 10.0);
        trader.incrementSellOrderBalances("sec1", 10.0);
        //view balances when there are balances to show
        trader.viewMyBalances();
    }

    /**
     * testing showmyactiveorders
     */
    @Test
    public void testShowActiveOrdersTrader() {
        //show active buy orders when there are no orders
        trader.showActiveOrders(1);
        //show active sell orders when there are no orders
        trader.showActiveOrders(2);


        //add buy orders and sell orders
        BuyOrder buyOrder = new BuyOrder(trader.getEmail(), 10, 5, new Timestamp(new Date().getTime()), Status.IN_PROGRESS, "sec1", 10);
        SellOrder sellOrder = new SellOrder(trader.getEmail(), 10, 5, new Timestamp(new Date().getTime()), Status.IN_PROGRESS, "sec1", 10);
        BuyOrder buyOrder2 = new BuyOrder(trader.getEmail(), 10, 5, new Timestamp(new Date().getTime()), Status.IN_PROGRESS, "sec1", 10);
        SellOrder sellOrder2 = new SellOrder(trader.getEmail(), 10, 5, new Timestamp(new Date().getTime()), Status.IN_PROGRESS, "sec1", 10);
        trader.addBuyOrder(buyOrder);
        trader.addSellOrder(sellOrder);
        trader.addBuyOrder(buyOrder2);
        trader.addSellOrder(sellOrder2);

        //show active buy orders when there are orders
        trader.showActiveOrders(1);
        //show active sell orders when there are orders
        trader.showActiveOrders(2);


    }


    /**
     * testing showmyactiveorders for no active orders
     */
    @Test
    public void testShowActiveOrdersNotActiveTrader() {


        //add cancelled buy orders and sell orders
        BuyOrder buyOrder = new BuyOrder(trader.getEmail(), 10, 5, new Timestamp(new Date().getTime()), Status.CANCELLED, "sec1", 10);
        SellOrder sellOrder = new SellOrder(trader.getEmail(), 10, 5, new Timestamp(new Date().getTime()), Status.CANCELLED, "sec1", 10);
        trader.addBuyOrder(buyOrder);
        trader.addSellOrder(sellOrder);

        //show active buy orders when there are orders
        trader.showActiveOrders(1);
        //show active sell orders when there are orders
        trader.showActiveOrders(2);

    }

    /**
     * testing showmyactiveorders for incorrect type
     */
    @Test
    public void testShowActiveOrdersIncorrectTypeTrader() {

        //call showactiveorders with a type not equal to 1 or 2
        trader.showActiveOrders(3);

    }

    /**
     * testing cancelorder for buyorders
     */
    @Test
    public void testCancelOrderBuyOrders() {

        trader.incrementBalance("Euro", 0.0);
        BuyOrder buyOrder = new BuyOrder(trader.getEmail(), 10, 1, new Timestamp(new Date().getTime()), Status.IN_PROGRESS, "sec1", 10);
        trader.addBuyOrder(buyOrder);
        trader.incrementBuyOrderBalances("Euro", 10.0);

        trader.cancelOrder(1, buyOrder.getId());
        assertEquals("Asserting order status", Status.CANCELLED, trader.getBuyOrders().get(buyOrder.getId()).getStatus());
        assertEquals(10, trader.getBalances().get("Euro"), 0.001);
        assertEquals(10, trader.getBalancesUnmodifiable().get("Euro"), 0.001);
        assertEquals("Asserting deletion of buy order balance", false, trader.getBuyOrdersBalances().containsKey("Euro"));


    }

    /**
     * testing cancelorder for buyorders with more buy order balance
     */
    @Test
    public void testCancelOrderBuyOrdersMoreBalance() {

        trader.incrementBalance("Euro", 0.0);
        BuyOrder buyOrder = new BuyOrder(trader.getEmail(), 10, 1, new Timestamp(new Date().getTime()), Status.IN_PROGRESS, "sec1", 10);
        trader.addBuyOrder(buyOrder);
        trader.incrementBuyOrderBalances("Euro", 50.0);

        trader.cancelOrder(1, buyOrder.getId());
        assertEquals("Asserting order status", Status.CANCELLED, trader.getBuyOrders().get(buyOrder.getId()).getStatus());
        assertEquals(10, trader.getBalances().get("Euro"), 0.001);
        assertEquals(40, trader.getBuyOrdersBalances().get("Euro"), 0.0001);


    }

    /**
     * testing cancelorder for sellorders
     */
    @Test
    public void testCancelOrderSellOrders() {

        trader.incrementBalance("sec1", 0.0);
        SellOrder sellOrder = new SellOrder(trader.getEmail(), 10, 1, new Timestamp(new Date().getTime()), Status.IN_PROGRESS, "sec1", 10);
        trader.addSellOrder(sellOrder);
        trader.incrementSellOrderBalances("sec1", 10.0);

        trader.cancelOrder(2, sellOrder.getId());
        assertEquals("Asserting order status", Status.CANCELLED, trader.getSellOrders().get(sellOrder.getId()).getStatus());
        assertEquals("Asserting order status", Status.CANCELLED, trader.getSellOrdersUnmodifiable().get(sellOrder.getId()).getStatus());
        assertEquals(10, trader.getBalances().get("sec1"), 0.001);
        assertEquals("Asserting deletion of sell order balance", false, trader.getSellOrdersBalances().containsKey("sec1"));


    }

    /**
     * testing cancelorder for sellorders with more sell order balance and no initial balance
     */
    @Test
    public void testCancelOrderSellOrdersMoreBalance() {

        SellOrder sellOrder = new SellOrder(trader.getEmail(), 10, 1, new Timestamp(new Date().getTime()), Status.IN_PROGRESS, "sec1", 10);
        trader.addSellOrder(sellOrder);
        trader.incrementSellOrderBalances("sec1", 20.0);

        trader.cancelOrder(2, sellOrder.getId());
        assertEquals("Asserting order status", Status.CANCELLED, trader.getSellOrders().get(sellOrder.getId()).getStatus());
        assertEquals(10, trader.getBalances().get("sec1"), 0.001);
        assertEquals(10, trader.getSellOrdersBalances().get("sec1"), 0.001);


    }

    /**
     * testing cancelOrder for incorrect type -for coverage
     */
    @Test
    public void testCancelOrderIncorrectType() {

        SellOrder sellOrder = new SellOrder(trader.getEmail(), 10, 1, new Timestamp(new Date().getTime()), Status.IN_PROGRESS, "sec1", 10);
        trader.addSellOrder(sellOrder);
        trader.incrementSellOrderBalances("sec1", 20.0);

        //call cancelOrder with a type not equal to 1 or 2
        trader.cancelOrder(3, sellOrder.getId());

    }

    /**
     * To test user add sell order for lister
     */
    @Test
    public void testAddSellOrderLister()
    {
        SellOrder sellOrder = new SellOrder(lister.getEmail(), 10, 5, new Timestamp(new Date().getTime()), Status.IN_PROGRESS, "sec1", 10);
        lister.addSellOrder(sellOrder);

        assertEquals("Asserting sell order balance", sellOrder, lister.getSellOrders().get(sellOrder.getId()));
    }

    /**
     * testing showmyactiveorders for no active orders for lister
     */
    @Test
    public void testShowActiveOrdersActiveLister() {

        lister.showActiveOrders();
        //add cancelled buy orders and sell orders
        SellOrder sellOrder = new SellOrder(lister.getEmail(), 10, 5, new Timestamp(new Date().getTime()), Status.IN_PROGRESS, "sec1", 10);
        lister.addSellOrder(sellOrder);
        SellOrder sellOrder2 = new SellOrder(lister.getEmail(), 10, 5, new Timestamp(new Date().getTime()), Status.IN_PROGRESS, "sec1", 10);
        lister.addSellOrder(sellOrder2);

        //show active buy orders when there are orders
        lister.showActiveOrders();
    }

    /**
     * testing showmyactiveorders for no active orders
     */
    @Test
    public void testShowActiveOrdersNotActiveLister() {


        //add cancelled  sell orders
        SellOrder sellOrder = new SellOrder(trader.getEmail(), 10, 5, new Timestamp(new Date().getTime()), Status.CANCELLED, "sec1", 10);
        lister.addSellOrder(sellOrder);

        //show active sell orders when there are orders
        lister.showActiveOrders();

    }

    /**
     * testing cancelorder for sellorders FOR LISTER
     */
    @Test
    public void testCancelOrderSellOrdersLister() {

        lister.incrementBalance("sec1", 0.0);
        SellOrder sellOrder = new SellOrder(lister.getEmail(), 10, 1, new Timestamp(new Date().getTime()), Status.IN_PROGRESS, "sec1", 10);
        lister.addSellOrder(sellOrder);
        lister.incrementSellOrderBalances("sec1", 10.0);

        lister.cancelOrder(sellOrder.getId());
        assertEquals("Asserting order status", Status.CANCELLED, lister.getSellOrders().get(sellOrder.getId()).getStatus());
        assertEquals(10, lister.getBalances().get("sec1"), 0.001);
        assertEquals("Asserting deletion of sell order balance", false, lister.getSellOrdersBalances().containsKey("sec1"));


    }

    /**
     * testing cancelorder for sellorders with more sell order balance and no initial balance FOR LISTER
     */
    @Test
    public void testCancelOrderSellOrdersMoreBalanceLister() {

        SellOrder sellOrder = new SellOrder(lister.getEmail(), 10, 1, new Timestamp(new Date().getTime()), Status.IN_PROGRESS, "sec1", 10);
        lister.addSellOrder(sellOrder);
        lister.incrementSellOrderBalances("sec1", 20.0);

        lister.cancelOrder(sellOrder.getId());
        assertEquals("Asserting order status", Status.CANCELLED, lister.getSellOrders().get(sellOrder.getId()).getStatus());
        assertEquals(10, lister.getBalances().get("sec1"), 0.001);
        assertEquals(10, lister.getSellOrdersBalances().get("sec1"), 0.001);


    }

    /**
     * testing viewmybalances for lister
     */
    @Test
    public void testViewMyBalancesLister() {
        //view balances with no balances
        lister.viewMyBalances();
        lister.incrementBalance("sec1", 10.0);
        lister.incrementSellOrderBalances("sec1", 10.0);
        //view balances when there are balances to show
        lister.viewMyBalances();
    }

    /**
     * testing viewmyorders just for coverage for lister
     */
    @Test
    public void testViewMyOrdersLister() {
        lister.viewMyOrders();
        SellOrder sellOrder = new SellOrder(lister.getEmail(), 10, 5, new Timestamp(new Date().getTime()), Status.IN_PROGRESS, "sec1", 10);

        lister.addSellOrder(sellOrder);
        lister.viewMyOrders();
    }

    /**
     * testing viewmyorders with only sell orders for lister
     */
    @Test
    public void testViewMyOrdersOnlySellLister() {
        SellOrder sellOrder = new SellOrder(lister.getEmail(), 10, 5, new Timestamp(new Date().getTime()), Status.IN_PROGRESS, "sec1", 10);

        lister.addSellOrder(sellOrder);
        lister.viewMyOrders();
    }

    /**
     * testing viewmylistedsecurities empty
     */
    @Test
    public void testViewMyListedSecuritiesEmpty() {

        assertEquals("Asserting empty listed securities", 0, lister.viewMyListedSecurities());
    }

    /**
     * testing viewmylistedsecurities
     */
    @Test
    public void testViewMyListedSecurities() {
        Security security = new Security("sec1", "desc", 1.0, 10);
        Security security2 = new Security("sec1", "desc", 1.0, 10);
        lister.listSecurity(security);
        lister.listSecurity(security2);
        assertEquals("Asserting empty listed securities", 2, lister.viewMyListedSecurities());
    }

    /**
     * testing listSecurity
     */
    @Test
    public void testListSecurity() {
        Security security = new Security("sec1", "desc", 1.0, 10);
        lister.listSecurity(security);
        assertEquals("Asserting listed security", true, lister.getListedSecurities().contains(security));
        assertEquals("Asserting listed security", true, lister.getListedSecuritiesUnmodifiable().contains(security));
        assertEquals(10.0, lister.getSellOrdersBalances().get("sec1"), 0.001);
    }

    /**
     * testing updateSecurity with no previous balance of the security updating
     */
    @Test
    public void testUpdateSecurityNoPrevBalance() {
        Security security = new Security("sec1", "desc", 1.0, 10);
        lister.listSecurity(security);

        lister.updateSecurity("sec1", 10, 2.0);
        assertEquals("Asserting updated supply", 20, lister.getListedSecurities().get(0).getTotalSupply());
        assertEquals(2.0, lister.getListedSecurities().get(0).getPrice(), 0.001);
        assertEquals(10, lister.getBalances().get("sec1"), 0.0001);

    }

    /**
     * testing updateSecurity with previous balance of the security updating
     */
    @Test
    public void testUpdateSecurityWithPrevBalance() {
        double initialVal =20;
        lister.incrementBalance("sec1", initialVal);
        Security security = new Security("sec1", "desc", 1.0, 10);
        lister.listSecurity(security);

        lister.updateSecurity("sec1", 10, 2.0);
        assertEquals("Asserting updated supply", 20, lister.getListedSecurities().get(0).getTotalSupply());
        assertEquals(2.0, lister.getListedSecurities().get(0).getPrice(), 0.001);
        assertEquals(initialVal+ 10, lister.getBalances().get("sec1"), 0.0001);

    }

    /**
     * testing updateSecurity when updating a non existent security
     */
    @Test
    public void testUpdateSecurityWithWrongSecurity() {
        Security security = new Security("sec1", "desc", 1.0, 10);
        lister.listSecurity(security);


        assertEquals("Asserting updateSecurity returning false when passing a wrong security not in listed list", false, lister.updateSecurity("sec2", 10, 2.0));

    }

    /**
     * testing changePassword
     */
    @Test
    public void testChangePassword() {
        //change password
        String newPass = "try123";
        lister.changePassword(newPass);

        //assert login with new password
        assertTrue("Asserting login with new password", lister.authenticate(listerEmail, newPass));

    }

    /**
     * testing approve with incorrect ID
     */
    @Test
    public void testApproveIncorrectId() {

        //assert null when id is incorrect
        assertNull("Asserting incorrect id", operator.approve("incorrectId"));
    }

    /**
     * testing approve with correct ID
     */
    @Test
    public void testApproveCorrectId() {

        String newTraderName = "Alex 2";
        String newTraderEmail = "alex2@gmail.com";
        String newTraderPassword = "try2";
        LocalDate newTraderDob = LocalDate.of(2000,1,1);

        Trader newTrader = new Trader(newTraderName, newTraderEmail, newTraderPassword, newTraderDob);

        operator.addToApprove(newTrader);


        assertNotNull("Asserting approve on correct id", operator.approve(newTrader.getId()));
    }


    /**
     * testing showUsersToBeApproved when there is no one to be approved
     */
    @Test
    public void testShowUsersToBeApprovedNoApprovals() {

        //assert there are no approvals
        assertEquals("Asserting no approvals", 0, operator.showUsersToBeApproved());

    }

    /**
     * testing getUsersToBeApproved when there is no one to be approved
     */
    @Test
    public void testGetUsersToBeApprovedEmpty() {

        //assert there are no approvals
        assertEquals("Asserting no approvals", 0, operator.getToBeApproved().size());

    }

    /**
     * testing etUsersToBeApproved
     */
    @Test
    public void testFetUsersToBeApproved() {

        String newTraderName = "Alex 2";
        String newTraderEmail = "alex2@gmail.com";
        String newTraderPassword = "try2";
        LocalDate newTraderDob = LocalDate.of(2000,1,1);

        Trader newTrader = new Trader(newTraderName, newTraderEmail, newTraderPassword, newTraderDob);

        operator.addToApprove(newTrader);


        assertTrue("Asserting approval", operator.getToBeApproved().containsValue(newTrader));
    }

    /**
     * testing showUsersToBeApproved when there is someone to be approved
     */
    @Test
    public void testShowUsersToBeApprovedWithApprovals() {

        String newTraderName = "Alex 2";
        String newTraderEmail = "alex2@gmail.com";
        String newTraderPassword = "try2";
        LocalDate newTraderDob = LocalDate.of(2000,1,1);

        Trader newTrader = new Trader(newTraderName, newTraderEmail, newTraderPassword, newTraderDob);

        operator.addToApprove(newTrader);
        //assert there are no approvals
        assertEquals("Asserting 1 approval", 1, operator.showUsersToBeApproved());

    }




}