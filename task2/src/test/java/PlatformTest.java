import exceptions.*;
import order.BuyOrder;
import order.SellOrder;
import order.Status;
import org.junit.Before;
import org.junit.Test;
import platform.ExchangePlatform;
import security.Security;
import user.Trader;
import user.User;

import java.sql.Timestamp;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

import static org.junit.Assert.*;

public class PlatformTest {

    ExchangePlatform platform = ExchangePlatform.getInstance();
    String name = "Alex";
    String email = "alex@gmail.com";
    String password = "try2";
    LocalDate dob = LocalDate.of(2000,1,1);

    String traderName = "Tom";
    String traderEmail = "tom@gmail.com";
    String traderPassword = "try2";

    String trader2Name = "Pet";
    String trader2Email = "pet@gmail.com";
    String trader2Password = "try2";

    String trader3Name = "Liam";
    String trader3Email = "liam@gmail.com";
    String trader3Password = "try2";


    String token, tokenTrader, tokenTrader2, opToken;
    Trader trader = new Trader(name, email, password, dob);

    @Before
    public void init() throws IncorrectUserDetailException, UserNotAuthenticated {
        platform.reset();
        opToken = platform.operatorLogin("admin@platform.com", "password123");
        platform.register(name, email, password, dob, 2);

        platform.register(traderName, traderEmail, traderPassword, dob, 1);
        platform.register(trader2Name, trader2Email, trader2Password, dob, 1);
        platform.approveAll(opToken);
        token = platform.login(email, password, 2);
        tokenTrader = platform.login(traderEmail, traderPassword, 1);
        tokenTrader2 = platform.login(trader2Email, trader2Password, 1);
        platform.register(trader3Name, trader3Email, trader3Password, dob, 1);
    }

    /**
     * Test show securities with no securities
     */
    @Test
    public void testShowSecuritiesNoSecurities(){
        int before = platform.showSecurities();
        assertEquals("Asserting return same as before if there are no securities", before, platform.showSecurities());
    }

    /**
     * Test for checkSecurityName when security does not exist
     */
    @Test
    public void testCheckSecurityNameNoExist()
    {
        assertEquals("Asserting false when checking for an inexistent security", false, platform.checkSecurityName("namenotexistent"));
    }

    /**
     * Test for checkSecurityName and getUserToken when security exist
     */
    @Test
    public void testCheckSecurityNameExist() throws UserNotAuthenticated, UserNotAllowedException, UserNotApproved, UserNotApproved {
        platform.addSecurity(token, "sec1", "try", 2.0, 10);
        assertEquals("Asserting true when checking for an existent security", true, platform.checkSecurityName("sec1"));
    }

    /**
     * Test for getUserFromToken when token is incorrect
     */
    @Test(expected = UserNotAuthenticated.class)
    public void testGetUserFromTokenIncorrect() throws UserNotAuthenticated, UserNotAllowedException, UserNotApproved {
        platform.addSecurity("BAD TOKEN", "sec1", "try", 2.0, 10);
    }

    /**
     * Test for addSecurity
     */
    @Test
    public void testAddSecurity() throws UserNotAuthenticated, UserNotAllowedException, UserNotApproved {
        platform.addSecurity(token, "sec1", "try", 2.0, 10);
        assertEquals("Asserting listed security after add", true, platform.getListedSecuritiesUnmodifiable().containsKey("sec1"));

        //listing the same security should return false
        assertFalse("Asserting false after adding the same security", platform.addSecurity(token, "sec1", "try", 2.0, 10));
    }

    /**
     * Test for login incorrect email
     */
    @Test
    public void testLoginIncorrectEmail(){
        assertNull("Asserting null when logging in with an incorrect email", platform.login("Incorrectemail", password, 2));
    }

    /**
     * Test for login incorrect password
     */
    @Test
    public void testLoginIncorrectPassword(){
        assertNull("Asserting null when logging in with an incorrect password", platform.login(email, "Incorrect password", 2));
    }

    /**
     * Test for login unapproved account
     */
    @Test
    public void testLoginNotApproved(){
        assertNull("Asserting null when logging in an unapproved account", platform.login(trader3Email, trader3Password, 1));
    }

    /**
     * Test for register correct details
     */
    @Test
    public void testRegister() throws IncorrectUserDetailException, UserNotAuthenticated {
        String Lname = "Dan";
        String Lemail = "dan@gmail.com";
        String Lpassword = "try2";
        LocalDate dob = LocalDate.of(2000,1,1);

        String Tname = "Nic";
        String Temail = "nic@gmail.com";
        String Tpassword = "try2";

        //register users

        assertTrue("Asserting true when registering correct details lister", platform.register(Lname, Lemail, Lpassword, dob, 2));
        assertTrue("Asserting true when registering correct details trader", platform.register(Tname, Temail, Tpassword, dob, 1));
        assertEquals("Asserting there are 2 users added to toBeApproved list", 3, platform.getUnapprovedUsers(opToken));
    }

    /**
     * Test for register incorrect type
     */
    @Test
    public void testRegisterIncorrectType() throws IncorrectUserDetailException {
        String Lname = "Dan";
        String Lemail = "dan@gmail.com";
        String Lpassword = "try2";
        LocalDate dob = LocalDate.of(2000,1,1);


        //register users

        assertFalse("Asserting false when registering with an incorrect type other than 1 or 2", platform.register(Lname, Lemail, Lpassword, dob, 3));
    }

    /**
     * Test for register incorrect dob
     */
    @Test
    public void testRegisterIncorrectDob() throws IncorrectUserDetailException {

        String Lname = "Dan";
        String Lemail = "dan@gmail.com";
        String Lpassword = "try2";
        LocalDate dob = LocalDate.of(2020,1,1);

        String Tname = "Nic";
        String Temail = "nic@gmail.com";
        String Tpassword = "try2";

        assertFalse("Asserting false when registering correct details lister", platform.register(Lname, Lemail, Lpassword, dob, 2));
        assertFalse("Asserting false when registering correct details trader", platform.register(Tname, Temail, Tpassword, dob, 1));
    }

    /**
     * Test for register empty name
     */
    @Test(expected =  IncorrectUserDetailException.class)
    public void testRegisterEmptyName() throws IncorrectUserDetailException {
        platform.register("", email, password, dob, 1);
    }

    /**
     * Test for register empty email
     */
    @Test(expected =  IncorrectUserDetailException.class)
    public void testRegisterEmptyEmail() throws IncorrectUserDetailException {
        platform.register(name, "", password, dob, 1);
    }

    /**
     * Test for register empty password
     */
    @Test(expected =  IncorrectUserDetailException.class)
    public void testRegisterEmptyPassword() throws IncorrectUserDetailException {
        platform.register(name, email, "", dob, 1);
    }

    /**
     * Test for register empty dob
     */
    @Test(expected =  IncorrectUserDetailException.class)
    public void testRegisterEmptyDob() throws IncorrectUserDetailException {
        platform.register(name, email, password, null, 1);
    }

    /**
     * Test for login incorrect type
     */
    @Test
    public void testLoginIncorrectType(){
        assertNull("Asserting null when logging with incorrect account type lister", platform.login(email, password, 3));
        assertNull("Asserting null when logging with incorrect account type trader", platform.login(traderEmail, traderPassword, 3));
    }

    /**
     * Test for logout by trying to call a function with the token which should not be accessible
     */
    @Test(expected = UserNotAuthenticated.class)
    public void testLogout() throws UserNotAuthenticated, UserNotAllowedException, UserNotApproved {
        String tokenNew = platform.login(email, password, 2);
        platform.logOut(tokenNew);
        platform.addSecurity(tokenNew, "sec1", "try", 2.0, 10);
    }

    /**
     * Test for logout on a wrong token by trying to call a function with the token which should not be accessible
     */
    @Test(expected = UserNotAuthenticated.class)
    public void testLogoutWrongToken() throws UserNotAuthenticated, UserNotAllowedException, UserNotApproved {
        String wrongToken = "Bad token";
        platform.logOut(wrongToken);
        platform.addSecurity(wrongToken, "sec1", "try", 2.0, 10);
    }

    /**
     * Test show listed securities with no securities
     */
    @Test
    public void testShowListedSecuritiesNoSecurities(){
        int before = platform.showListedSecurities();
        assertEquals("Asserting return of before if there are no securities", before, platform.showListedSecurities());
    }

    /**
     * Test show listed securities with securities
     */
    @Test
    public void testShowListedSecuritiesWithSecurities() throws UserNotAuthenticated, UserNotAllowedException, UserNotApproved {
        int before = platform.showListedSecurities();
        platform.addSecurity(token, "testSec7", "try", 2.0, 10);
        platform.addSecurity(token, "testSec8", "try", 2.0, 10);
        assertEquals("Asserting return of before+2 if two new securities are added", before+2, platform.showListedSecurities());
    }

    /**
     * Test get listed securities with no securities
     */
    @Test
    public void testGetListedSecuritiesNoSecurities(){
        HashMap<String, Security> before = (HashMap<String, Security>)platform.getListedSecurities().clone();
        assertEquals("Asserting return same as before if there are no securities added", before, platform.getListedSecurities());
    }

    /**
     * Test get listed securities with securities
     */
    @Test
    public void testGetListedSecuritiesWithSecurities() throws UserNotAuthenticated, UserNotAllowedException, UserNotApproved {
        HashMap<String, Security> before = (HashMap<String, Security>)platform.getListedSecurities().clone();
        platform.addSecurity(token, "testSec1", "try", 2.0, 10);
        platform.addSecurity(token, "testSec2", "try", 2.0, 10);
        assertEquals("Asserting return of the amount before +2 after adding 2 securities", before.size()+2, platform.getListedSecurities().size());
    }

    /**
     * Test show security orders
     */
    @Test
    public void testShowSecurityOrders(){
        platform.showSecurityOrders("sec1");
    }



    /**
     * Test show securities with securities
     */
    @Test
    public void testShowSecuritiesWithSecurities() throws UserNotAuthenticated, UserNotAllowedException, UserNotApproved {
        int before = platform.showSecurities();
        platform.addSecurity(token, "testSec3", "try", 2.0, 10);
        platform.addSecurity(token, "testSec4", "try", 2.0, 10);
        assertEquals("Asserting return of 2 + before if two new securities are added", before+2, platform.showSecurities());
    }

    /**
     * Test get securities with no securities
     */
    @Test
    public void testGetSecuritiesNoSecurities(){
        ArrayList<Security> before = (ArrayList<Security>)platform.getSecurities().clone();
        assertEquals("Asserting return of empty list if there are no securities", before, platform.getSecurities());
    }

    /**
     * Test get listed securities with securities
     */
    @Test
    public void testGetSecuritiesWithSecurities() throws UserNotAuthenticated, UserNotAllowedException, UserNotApproved {
        ArrayList<Security> before = (ArrayList<Security>)platform.getSecurities().clone();
        platform.addSecurity(token, "testSec5", "try", 2.0, 10);
        platform.addSecurity(token, "testSec6", "try", 2.0, 10);
        assertEquals("Asserting return of before+2 if two new securities are added", before.size()+2, platform.getSecurities().size());
    }

    /**
     * Test for checkEmail when email does not exist
     */
    @Test
    public void testCheckEmailNoExist()
    {
        assertEquals("Asserting 0 when checking for an inexistent email", 0, platform.checkEmail("unknown@email.com"));
    }

    /**
     * Test for checkEmail when email exists
     */
    @Test
    public void testCheckEmailExist(){
        assertEquals("Asserting 2 when checking for an existent email", 2, platform.checkEmail("tom@gmail.com"));

    }

    /**
     * Test for checkEmail when email is of incorrect format
     */
    @Test
    public void testCheckEmailIncorrectFormat(){
        assertEquals("Asserting 1 when checking for an email in incorrect format", 1, platform.checkEmail("hello"));

    }

    /**
     * Test for viewUserOrders - just for coverage because this called the viewMyOrders method which was tested in the User Test class
     */
    @Test
    public void testViewUserOrders() throws UserNotAuthenticated, UserNotApproved {
        platform.viewUserOrders(token, 2);
        platform.viewUserOrders(tokenTrader, 1);
        platform.viewUserOrders(token, 3);

    }

    /**
     * Test for viewUserBalances - just for coverage because this called the viewMyBalances method which was tested in the User Test class
     */
    @Test
    public void testViewUserBalances() throws UserNotAuthenticated, UserNotApproved {
        platform.viewUserBalances(token, 2);
        platform.viewUserBalances(tokenTrader, 1);
        platform.viewUserBalances(token, 3);

    }

    /**
     * Test for getUserIdleBalances - just for coverage because this called the showIdleBalances method which was tested in the User Test class
     */
    @Test
    public void testGetUserIdleBalances() throws UserNotAuthenticated, UserNotApproved{
        platform.getUserIdleBalances(token);

    }

    /**
     * Test for showUserSecurities - just for coverage because this called the viewMyListedSecurities method which was tested in the User Test class
     */
    @Test
    public void testShowUserSecurities() throws UserNotAuthenticated , UserNotAllowedException, UserNotApproved{
        platform.showUserSecurities(token);

    }

    /**
     * Test for getUserActiveOrders - just for coverage because this called the showActiveOrders method which was tested in the User Test class
     */
    @Test
    public void testGetUserActiveOrders() throws UserNotAuthenticated, UserNotApproved {
        platform.getUserActiveOrders(tokenTrader, 2, 1);
        platform.getUserActiveOrders(token, 2, 2);
        platform.getUserActiveOrders(token, 2, 3);

    }

    /**
     * Test for updateSecurity for a non-existent security
     */
    @Test
    public void testUpdateSecurityNoExist() throws UserNotAuthenticated, MoreThanBalanceException, UserNotAllowedException, UserNotApproved {
        assertFalse("Updating security which does not exist", platform.updateSecurity(token, "sec1", 10, 2.0));

    }

    /**
     * Test for updateSecurity for an existent security
     */
    @Test
    public void testUpdateSecurityExist() throws UserNotAuthenticated, MoreThanBalanceException, UserNotAllowedException, UserNotApproved {
        platform.addSecurity(token, "testSec9", "try", 1, 20);
        assertTrue("Updating security which exists", platform.updateSecurity(token, "testSec9", 10, 2.0));

    }



    /**
     * Test for cancelOrder for a non-existent order
     */
    @Test
    public void testCancelOrderNoExist() throws UserNotAuthenticated, MoreThanBalanceException, UserNotApproved {
        assertFalse("Cancelling order which does not exist", platform.cancelOrder(token, "wrongId", 1, 2));

    }

    /**
     * Test for cancelOrder for an existent order
     */
    @Test
    public void testCancelOrder() throws UserNotAuthenticated, MoreThanBalanceException, UserNotAllowedException, UserNotApproved {
        platform.addSecurity(token, "sec1", "try", 1, 20);

        platform.buy(tokenTrader, "sec1", 10, 2);
        //cancel buy order
        assertTrue("Cancelling buy order", platform.cancelOrder(tokenTrader, platform.getBuyOrders().get(0), 1, 1));

        //cancel sell order
        assertTrue("Cancelling sell order", platform.cancelOrder(token, platform.getSellOrders().get(0), 2, 2));

        platform.buy(tokenTrader, "sec1", 10, 1);
        platform.addSecurity(token, "sec3", "try", 1, 20);

        //cancel buy order by operator
        assertTrue("Cancelling buy order by operator", platform.cancelOrder(opToken, platform.getBuyOrders().get(1), 1, 3));
        assertTrue("Cancelling buy order by operator", platform.cancelOrder(opToken, platform.getSellOrders().get(1), 2, 3));


    }

    /**
     * Test for cancelOrder for an existent order with a wrong acc type
     */
    @Test
    public void testCancelOrderWrongAccType() throws UserNotAuthenticated, UserNotAllowedException, UserNotApproved {
        platform.addSecurity(token, "sec1", "try", 1, 20);

        //cancel sell order
        assertFalse("Cancelling sell order", platform.cancelOrder(token, platform.getSellOrders().get(0), 2, 5));

    }

    /**
     * Test for buy method when we don't have enough balance
     */

    @Test(expected = MoreThanBalanceException.class)
    public void testBuyNotEnoughBalance() throws UserNotAuthenticated, MoreThanBalanceException, UserNotAllowedException, UserNotApproved {
        platform.buy(tokenTrader, "sec1", 100, 2);

    }

    /**
     * Test for buy method when we have enough balance
     */

    @Test
    public void testBuyEnoughBalance() throws UserNotAuthenticated, MoreThanBalanceException, UserNotAllowedException, UserNotApproved {

        //when there are no sell orders for security
        assertTrue("Asserting buy returning true when we have enough balance", platform.buy(tokenTrader, "sec1", 10, 1));

        //when buy is less than sell in quantity
        platform.addSecurity(token, "sec1", "try", 1, 20);
        assertTrue("Asserting buy returning true when we have enough balance", platform.buy(tokenTrader, "sec1", 10, 1));

        //when buy is more than sell in quantity
        assertTrue("Asserting buy returning true when we have enough balance", platform.buy(tokenTrader, "sec1", 20, 1));

    }

    /**
     * Test for buy method price is lower than lowest sell
     */

    @Test
    public void testBuyEnoughLowPrice() throws UserNotAuthenticated, MoreThanBalanceException, UserNotAllowedException, UserNotApproved {


        //when buy is less than sell in quantity
        platform.addSecurity(token, "sec1", "try", 2, 20);
        assertTrue("Asserting buy returning true when we have enough balance but price is too low", platform.buy(tokenTrader, "sec1", 10, 1));


    }

    /**
     * Test for buy method price when buy quantity is equal to sell quantitiy
     */
    @Test
    public void testBuyEqualQuantity() throws UserNotAuthenticated, MoreThanBalanceException, UserNotAllowedException, UserNotApproved {


        //when buy is less than sell in quantity
        platform.addSecurity(token, "sec1", "try", 2, 20);
        platform.updateSecurity(token, "sec1", 10, 3);
        platform.addSecurity(token, "sec2", "try", 2, 20);
        assertTrue("Asserting buy returning true when we have enough balance and buy and sell orders have same quantity", platform.buy(tokenTrader, "sec1", 10, 2));


    }

    /**
     * Test for buy method price when buy er and seller are the same
     */
    @Test
    public void testBuySameBuyerSeller() throws UserNotAuthenticated, MoreThanBalanceException, UserNotAllowedException, UserNotApproved {


        //when buy is less than sell in quantity
        platform.addSecurity(token, "sec1", "try", 1, 10);
        platform.buy(tokenTrader, "sec1", 10, 1);
        platform.sell(tokenTrader, "sec1", 10, 1);
        assertTrue("Asserting buy returning true when we have enough balance and buy and sell orders have same quantity", platform.buy(tokenTrader, "sec1", 10, 1));

    }

    /**
     * Test for buy method when we don't have balance
     */
    @Test(expected = MoreThanBalanceException.class)
    public void testSellNoBalance() throws UserNotAuthenticated, MoreThanBalanceException, UserNotApproved {
        platform.sell(tokenTrader, "sec1", 100, 2);

    }

    /**
     * Test for buy method when we don't have enough balance
     */
    @Test(expected = MoreThanBalanceException.class)
    public void testSellNotEnoughBalance2() throws UserNotAuthenticated, MoreThanBalanceException, UserNotAllowedException, UserNotApproved {
        platform.addSecurity(token, "sec1", "try", 1, 20);

        //when there are no buy orders for security
        platform.buy(tokenTrader, "sec1", 20, 1);
        platform.sell(tokenTrader, "sec1", 100, 2);

    }

    /**
     * Test for buy method when we have enough balance
     */

    @Test
    public void testSellEnoughBalance() throws UserNotAuthenticated, MoreThanBalanceException, UserNotAllowedException , UserNotApproved{

        platform.addSecurity(token, "sec1", "try", 1, 40);

        //when there are no buy orders for security
        platform.buy(tokenTrader, "sec1", 40, 1);


        platform.buy(tokenTrader2, "sec1", 20, 1);
        //this is done for coverage to reach the done condition
        platform.buy(tokenTrader2, "sec1", 10, 1.1);
        assertTrue("Asserting sell returning true when we have enough balance and selling less quantity than buying order", platform.sell(tokenTrader, "sec1", 10, 1));


    }


    /**
     * Test for buy method price is higher than highest buy
     */
    @Test
    public void testSellEnoughHighPrice() throws UserNotAuthenticated, MoreThanBalanceException, UserNotAllowedException, UserNotApproved {


        platform.addSecurity(token, "sec1", "try", 1, 20);

        //when there are no buy orders for security
        platform.buy(tokenTrader, "sec1", 20, 1);
        platform.buy(tokenTrader2, "sec1", 20, 1);
        assertTrue("Asserting sell returning true when we have enough balance but price is too high", platform.sell(tokenTrader, "sec1", 10, 2));


    }

    /**
     * Test for buy method price when buy quantity is equal to sell quantity
     */
    @Test
    public void testSellEqualQuantity() throws UserNotAuthenticated, MoreThanBalanceException, UserNotAllowedException, UserNotApproved {


        //when buy is less than sell in quantity
        platform.addSecurity(token, "sec1", "try", 3, 10);
        platform.buy(tokenTrader, "sec1", 10, 3);
        platform.buy(tokenTrader2, "sec1", 10, 3);
        assertTrue("Asserting sell returning true when selling quantitiy equals buying quantitiy", platform.sell(tokenTrader, "sec1", 10, 3));

    }

    /**
     * Test for buy method price when sell quantity is more than buy quantitiy
     */
    @Test
    public void testSellMoreQuantity() throws UserNotAuthenticated, MoreThanBalanceException, UserNotAllowedException, UserNotApproved {


        //when buy is less than sell in quantity
        platform.addSecurity(token, "sec1", "try", 2, 20);
        platform.buy(tokenTrader, "sec1", 20, 2);
        platform.buy(tokenTrader2, "sec1", 10, 2);
        assertTrue("Asserting sell returning true when selling quantitiy equals buying quantitiy", platform.sell(tokenTrader, "sec1", 20, 2));

    }

    /**
     * Test for buy method price when sell quantity is less than buy quantitiy
     */
    @Test
    public void testSellLessQuantity() throws UserNotAuthenticated, MoreThanBalanceException, UserNotAllowedException, UserNotApproved {


        //when buy is less than sell in quantity
        platform.addSecurity(token, "sec1", "try", 2, 20);
        platform.buy(tokenTrader, "sec1", 20, 2);
        platform.buy(tokenTrader2, "sec1", 10, 2);
        platform.buy(tokenTrader2, "sec1", 10, 2.1);
        assertTrue("Asserting sell returning true when selling quantity is less than buying quantity", platform.sell(tokenTrader, "sec1", 5, 2.1));
    }

    /**
     * Test for buy method price when buy er and seller are the same - just for coverage
     */
    @Test
    public void testSellSameBuyerSeller() throws UserNotAuthenticated, MoreThanBalanceException, UserNotAllowedException, UserNotApproved {


        //when buy is less than sell in quantity
        platform.addSecurity(token, "sec1", "try", 1, 20);
        platform.buy(tokenTrader, "sec1", 20, 1);

        platform.buy(tokenTrader, "sec1", 10, 1);
        assertTrue("Asserting buy returning true when buy and seller are the same",  platform.sell(tokenTrader, "sec1", 10, 1));

        platform.buy(tokenTrader2, "sec1", 10, 1);
        assertTrue("Asserting buy returning true when buy and seller are the same",  platform.sell(tokenTrader, "sec1", 10, 1));

    }

    /**
     * Test for buy method price when buy er and seller are the same - just for coverage
     */
    @Test
    public void testSellSameBuyerSeller2() throws UserNotAuthenticated, MoreThanBalanceException, UserNotAllowedException, UserNotApproved {


        //when buy is less than sell in quantity
        platform.addSecurity(token, "sec1", "try", 1, 10);
        platform.buy(tokenTrader, "sec1", 10, 1);
        platform.buy(tokenTrader, "sec1", 10, 1);
        assertTrue("Asserting buy returning true when buy and seller are the same",  platform.sell(tokenTrader, "sec1", 10, 1));

    }

    /**
     * Test for checkUserType when user is a Lister and checked for Lister
     */
    @Test
    public void testCheckUserTypeListerLister() throws UserNotAuthenticated, MoreThanBalanceException, UserNotAllowedException, UserNotApproved {

        platform.addSecurity(token, "sec1", "try", 1, 10);
    }

    /**
     * Test for checkUserType when user is a Trader and checked for Lister
     */
    @Test(expected =  UserNotAllowedException.class)
    public void testCheckUserTypeTraderLister() throws UserNotAuthenticated, MoreThanBalanceException, UserNotAllowedException, UserNotApproved {

        platform.addSecurity(tokenTrader, "sec1", "try", 1, 10);
    }

    /**
     * Test for checkUserType when user is a Trader and checked for Trader
     */
    @Test
    public void testCheckUserTypeTraderTrader() throws UserNotAuthenticated, MoreThanBalanceException, UserNotAllowedException, UserNotApproved {

        platform.addSecurity(token, "sec1", "try", 1, 10);
        platform.buy(tokenTrader, "sec1", 10, 1);
    }

    /**
     * Test for checkUserType when user is a Lister and checked for Trader
     */
    @Test(expected =  UserNotAllowedException.class)
    public void testCheckUserTypeListerTrader() throws UserNotAuthenticated, MoreThanBalanceException, UserNotAllowedException, UserNotApproved {

        platform.addSecurity(token, "sec1", "try", 1, 10);
        platform.buy(token, "sec1", 10, 1);
    }

    /**
     * Test for checkUserType with incorrect type - just for coverage
     */
    @Test
    public void testCheckUserTypeIncorrectType() throws UserNotAuthenticated, MoreThanBalanceException, UserNotAllowedException {

        platform.checkUserType(trader, 3);
    }

    /**
     * Test for operatorLogin incorrect email
     */
    @Test
    public void testOperatorLoginIncorrectEmail(){
        assertNull("Asserting null when logging in with an incorrect email", platform.operatorLogin("Incorrectemail", "password123"));
    }

    /**
     * Test for operatorLogin incorrect password
     */
    @Test
    public void testOperatorLoginIncorrectPassword(){
        assertNull("Asserting null when logging in with an incorrect password", platform.operatorLogin("admin@platform.com", "Incorrect password"));
    }

    /**
     * Test for operatorLogin correct details
     */
    @Test
    public void testOperatorLoginCorrectEetails(){
        assertNotNull("Asserting not null when logging in with correct details", platform.operatorLogin("admin@platform.com", "password123"));
    }

    /**
     * Test for getUnapprovedUsers incorrect token
     */
    @Test(expected = UserNotAuthenticated.class)
    public void testGetUnapprovedUsersIncorrectToken() throws UserNotAuthenticated {
        platform.getUnapprovedUsers("wrongToken");
    }

    /**
     * Test for getUnapprovedUsers correct token
     */
    @Test
    public void testGetUnapprovedUsersCorrectToken() throws UserNotAuthenticated {
        assertEquals("Asserting 1 when getting unapproved with correct token", 1, platform.getUnapprovedUsers(opToken));
    }

    /**
     * Test for approve incorrect token
     */
    @Test(expected = UserNotAuthenticated.class)
    public void testApproveIncorrectToken() throws UserNotAuthenticated {
        platform.approve("wrongToken", ((User)platform.getToBeApproved().get(0)).getId());
    }

    /**
     * Test for approve correct token
     */
    @Test
    public void testApproveCorrectToken() throws UserNotAuthenticated {
        assertTrue("Asserting true when approving with correct token",  platform.approve(opToken, ((User)platform.getToBeApproved().get(0)).getId()));
    }

    /**
     * Test for approve incorrect Id
     */
    @Test
    public void testApproveIncorrectId() throws UserNotAuthenticated {
        assertFalse("Asserting false when approving with incorrect id",  platform.approve(opToken, "wrongID"));
    }

    /**
     * Test for approveAll incorrect token
     */
    @Test(expected = UserNotAuthenticated.class)
    public void testApproveAllIncorrectToken() throws UserNotAuthenticated {
        platform.approveAll("wrongToken");
    }

    /**
     * Test for approveAll correct token
     */
    @Test
    public void testApproveAllCorrectToken() throws UserNotAuthenticated {
        assertTrue("Asserting true when approving all with correct token", platform.approveAll(opToken));
    }

    /**
     * Test for changeAdminPassword incorrect token
     */
    @Test(expected = UserNotAuthenticated.class)
    public void testChangeAdminPasswordIncorrectToken() throws UserNotAuthenticated {
        platform.changeAdminPassword("wrongToken", "newPass");
    }

    /**
     * Test for changeAdminPassword correct token
     */
    @Test
    public void testChangeAdminPasswordCorrectToken() throws UserNotAuthenticated {
        String newPassword = "newpassword123";
        assertTrue("Asserting true when changing password with correct token", platform.changeAdminPassword(opToken, newPassword));
        assertNotNull("Asserting not null when logging in with correct details", platform.operatorLogin("admin@platform.com", newPassword));
    }


}