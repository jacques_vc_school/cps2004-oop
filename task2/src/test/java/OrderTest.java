import order.Order;
import order.Status;
import org.junit.Test;

import java.sql.Timestamp;
import java.util.Date;

import static org.junit.Assert.*;

public class OrderTest {

    String trader = "try@gmail.com";
    double quantity = 100;
    double price = 10;
    Status status = Status.IN_PROGRESS;
    String security = "sec1";
    double quantityAvailable = 50;
    Timestamp ts = new Timestamp(new Date().getTime());
    Order order = new Order(trader, quantity, price, ts, status, security, quantityAvailable);


    /**
     * To test order getters
     */
    @Test
    public void testGetters()
    {
        assertEquals("Asserting trader", trader, order.getTrader());
        assertEquals(quantity, order.getQuantity(), 0.001);
        assertEquals(price, order.getPrice(), 0.001);
        assertEquals("Asserting status", status, order.getStatus());
        assertEquals("Asserting timestamp", ts, order.getTimestamp());
        assertEquals("Asserting security", security, order.getSecurity());
        assertEquals( quantityAvailable, order.getQuantityAvailable(), 0.001);
        assertEquals("Asseerting id", order.getId(), order.getId());
    }

    /**
     * To test order setters
     */
    @Test
    public void testSetters()
    {
        String newTrader = "new@try.com";
        order.setTrader(newTrader);
        assertEquals("Asserting trader", newTrader, order.getTrader());

        double newQuantity = 40;
        order.setQuantity(40);
        assertEquals(newQuantity, order.getQuantity(), 0.001);

        double newPrice = 2.5;
        order.setPrice(newPrice);
        assertEquals(newPrice, order.getPrice(), 0.001);

        Status newStatus = Status.FULFILLED;
        order.setStatus(newStatus);
        assertEquals("Asserting status", newStatus, order.getStatus());

        double newQtyAvailable = 20;
        order.setQuantityAvailable(newQtyAvailable);
        assertEquals(newQtyAvailable, order.getQuantityAvailable(), 0.001);

        order.reduceQuantityAvailable(10);
        assertEquals(10, order.getQuantityAvailable(), 0.001);

        //reduce more than available
        order.reduceQuantityAvailable(100);
        assertEquals(0, order.getQuantityAvailable(), 0.001);
    }


}