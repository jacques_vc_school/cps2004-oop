import immutableList.LinkedList;
import org.junit.Test;
import exceptions.ValueNotFoundException;


import static org.junit.Assert.assertEquals;


public class LinkedListIntegrationTest {


    /**
     * To test add functionality
     */
    @Test
    @SuppressWarnings("unchecked")
    public void testLinkedListIntegration() throws InterruptedException {
        LinkedList<Integer> original = new LinkedList<Integer>();
        original = original.add(1).add(2);

        final LinkedList<Integer>[] linkedListThreads = new LinkedList[4];
        linkedListThreads[0] = original.copy();
        linkedListThreads[1] = original.copy();
        linkedListThreads[2] = original.copy();
        linkedListThreads[3] = original.copy();

        Thread thread1 = new Thread() {
            public void run() {
                linkedListThreads[0] = linkedListThreads[0].add(3).add(4).add(5);
                linkedListThreads[0].showList();
            }
        };

        Thread thread2 = new Thread() {
            public void run() {
                linkedListThreads[1] = linkedListThreads[1].add(5).add(6);
            }
        };

        Thread thread3 = new Thread() {
            public void run() {
                try {
                    linkedListThreads[2] = linkedListThreads[2].remove(1);
                }catch (ValueNotFoundException e)
                {
                    System.out.println("Cannot remove value which does not exist");
                }
            }
        };

        Thread thread4 = new Thread() {
            public void run() {
                linkedListThreads[3] = linkedListThreads[3].clearList();
            }
        };

        thread1.start();
        thread1.join();
        thread2.start();
        thread2.join();
        thread3.start();
        thread3.join();
        thread4.start();
        thread4.join();

        assertEquals("Original array stays the same by size", 2, original.size());
        assertEquals("Original array stays the same by toString", "[1, 2]", original.toString());
        assertEquals("linkedListThreads[0] array adds 3, 4 and 5 by size", 5, linkedListThreads[0].size());
        assertEquals("linkedListThreads[0] array adds 3, 4 and 5 by toString", "[1, 2, 3, 4, 5]", linkedListThreads[0].toString());
        assertEquals("linkedListThreads[1] array adds 5 and 6 by size", 4, linkedListThreads[1].size());
        assertEquals("linkedListThreads[1] array adds 5 and 6 by toString", "[1, 2, 5, 6]", linkedListThreads[1].toString());
        assertEquals("linkedListThreads[2] array removes 1 by size", 1, linkedListThreads[2].size());
        assertEquals("linkedListThreads[2] array removes 1 by toString", "[2]", linkedListThreads[2].toString());
        assertEquals("linkedListThreads[3] array clears list by size", 0, linkedListThreads[3].size());
        assertEquals("linkedListThreads[3] array clears list by toString", "[]", linkedListThreads[3].toString());
    }


}