import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class)

@Suite.SuiteClasses({
        LinkedListTest.class,
        LinkedListIntegrationTest.class
})

public class TestSuite {
}