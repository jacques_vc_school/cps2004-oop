import exceptions.ValueNotFoundException;
import org.junit.Test;

import java.util.ArrayList;
import immutableList.LinkedList;

import static org.junit.Assert.*;

public class LinkedListTest {


    LinkedList linkedList = new LinkedList();

    //test to check add

    /**
     * To test add functionality
     */
    @Test
    public void testLinkedListAdd()
    {
       linkedList = linkedList.add(1);
       assertEquals("Asserting first add", 1, linkedList.size());
       linkedList = linkedList.add(2);
       assertEquals("Asserting second add", 2, linkedList.size());
       linkedList = linkedList.add(3);
       assertEquals("Asserting third add", 3, linkedList.size());
    }

    /**
     * To test addAll functionality
     */
    @Test
    public void testLinkedListAddAll()
    {
        ArrayList list = new ArrayList();
        list.add(1);
        list.add(2);
        list.add(3);
        linkedList = linkedList.addAll(list);
        assertEquals("Asserting size", 3, linkedList.size());
    }

    /**
     * To test index of functionality
     */
    @Test
    public void testLinkedListIndexOf()
    {
        linkedList = linkedList.add(1);
        linkedList = linkedList.add(2);
        assertEquals("Asserting index of 2 is 1", 1, linkedList.indexOf(2));
    }

    /**
     * To test index of functionality of an empty list
     */
    @Test
    public void testLinkedListIndexOfEmpty()
    {
        assertEquals("Asserting -1 index for empty list", -1, linkedList.indexOf(1));
    }

    /**
     * To test size functionality
     */
    @Test
    public void testLinkedListSize()
    {
        linkedList = linkedList.add(1);
        linkedList = linkedList.add(2);
        assertEquals("Asserting size 2", 2, linkedList.size());
    }

    /**
     * To test isEmpty functionality
     */
    @Test
    public void testLinkedListIsEmpty()
    {
        assertTrue("Asserting list is empty", linkedList.isEmpty());
        linkedList = linkedList.add(1);
        assertFalse("Asserting list is not empty", linkedList.isEmpty());
    }

    /**
     * To test contains functionality
     */
    @Test
    public void testLinkedListContains()
    {
        assertFalse("Asserting list does not contain", linkedList.contains(1));
        linkedList = linkedList.add(1);
        linkedList = linkedList.add(2);
        assertTrue("Asserting list contains 2", linkedList.contains(2));
    }

    /**
     * To test clearList functionality
     */
    @Test
    public void testLinkedListClearList()
    {
        linkedList = linkedList.add(1);
        linkedList = linkedList.add(2);
        linkedList = linkedList.clearList();
        assertEquals("Asserting size is 0 after clear", 0, linkedList.size());
    }

    /**
     * To test getHead functionality for empty list
     */
    @Test
    public void testLinkedListGetHeadEmpty() {
        assertNull("Asserting null if list is empty", linkedList.getHead());
    }

    /**
     * To test getHead functionality
     */
    @Test
    public void testLinkedListGetHead() {
        linkedList = linkedList.add(1);
        linkedList = linkedList.add(2);
        assertEquals("Asserting head's first value is 1", 1, linkedList.get(0));
        assertEquals("Asserting head's second value is 2", 2, linkedList.get(1));
    }

    /**
     * To test remove functionality
     */
    @Test
    public void testLinkedListRemove() throws ValueNotFoundException {
        linkedList = linkedList.add(1);
        linkedList = linkedList.add(2);
        linkedList = linkedList.add(3);
        linkedList = linkedList.add(4);
        linkedList = linkedList.remove(2);
        assertEquals("Asserting remove by toString", "[1, 3, 4]", linkedList.toString());
        assertEquals("Asserting remove by size",3, linkedList.size());
    }

    /**
     * To test remove functionality when removing a non existing object
     */
    @Test(expected = ValueNotFoundException.class)
    public void testLinkedListRemoveNotExist() throws ValueNotFoundException
    {
        linkedList = linkedList.add(1);
        linkedList = linkedList.add(2);
        linkedList = linkedList.remove(3);
    }

    /**
     * To test remove functionality when removing a non existing object from an empty list
     */
    @Test(expected = ValueNotFoundException.class)
    public void testLinkedListRemoveEmpty() throws ValueNotFoundException
    {
        linkedList = linkedList.remove(3);
    }

    /**
     * To test remove functionality when removing the only element in the list
     */
    @Test
    public void testLinkedListRemoveToEmpty() throws ValueNotFoundException
    {
        linkedList = linkedList.add(new Integer(1));
        linkedList = linkedList.remove(new Integer(1));
        assertEquals("Asserting list is empty by toString", "[]", linkedList.toString());
        assertEquals("Asserting size is 0", 0, linkedList.size());
    }



    /**
     * To test remove functionality when removing a non existing object
     */
    @Test
    public void testLinkedListRemoveAll() throws ValueNotFoundException {
        ArrayList list = new ArrayList();
        list.add(2);
        list.add(4);
        linkedList = linkedList.add(1);
        linkedList = linkedList.add(2);
        linkedList = linkedList.add(3);
        linkedList = linkedList.add(4);
        linkedList = linkedList.add(5);
        linkedList = linkedList.removeAll(list);
        assertEquals("Asserting removeAll by toString", "[1, 3, 5]", linkedList.toString());
        assertEquals("Asserting removeAll by size", 3, linkedList.size());
    }

    /**
     * To test toString functionality
     */
    @Test
    public void testLinkedListtoString(){
        linkedList = linkedList.add(1);
        linkedList = linkedList.add(2);
        linkedList = linkedList.add(3);
        linkedList = linkedList.add(4);
        linkedList = linkedList.add(5);
        assertEquals("Asserting toString","[1, 2, 3, 4, 5]", linkedList.toString());
    }

    /**
     * To test toString functionality for empty list
     */
    @Test
    public void testLinkedListToStringEmpty(){
        assertEquals("Asserting toString when list is empty","[]", linkedList.toString());
    }

    /**
     * To test copy functionality
     */
    @Test
    public void testLinkedListCopy(){
        linkedList = linkedList.add(1);
        linkedList = linkedList.add(2);

        LinkedList linkedList2 = linkedList.copy();
        assertEquals("Asserting copy by size", linkedList2.size(), linkedList.size());
        assertEquals("Asserting copy by toString", linkedList2.toString(), linkedList.toString());
    }

    /**
     * To test copy functionality for empty list
     */
    @Test
    public void testLinkedListCopyEmpty(){
        LinkedList linkedList2 = linkedList.copy();
        assertEquals("Asserting copy by size",  linkedList2.size(), linkedList.size());
        assertEquals("Asserting copy by toString", linkedList2.toString(), linkedList.toString());
    }

    /**
     * Just for coverage
     */
    @Test
    public void testLinkedListShowListEmpty(){
        linkedList.showList();
    }


    /**
     * Just for coverage
     */
    @Test
    public void testLinkedListShowList(){
        linkedList = linkedList.add(1);
        linkedList = linkedList.add(2);
        linkedList.showList();
    }

    /**
     * To test get functionality out of bounds
     */
    @Test(expected = IndexOutOfBoundsException.class)
    public void testLinkedListGetEmpty() throws IndexOutOfBoundsException
    {
        linkedList.get(1);
    }

    /**
     * To test get functionality out of bounds
     */
    @Test
    public void testLinkedListGet() throws IndexOutOfBoundsException
    {
        linkedList = linkedList.add(2);
        linkedList = linkedList.add(3);
        linkedList = linkedList.add(4);
        assertEquals("Asserting get is equal to 4", 4, linkedList.get(2));
    }

}