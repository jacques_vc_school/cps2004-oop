package immutableList;

import exceptions.ValueNotFoundException;

import java.util.Collection;

public class LinkedList<E> implements ImmutableList<E>
{

    //The node pointing to the head
    private Node head;

    //The variable containing the current size of the list
    private int size;

    // Default node constructor setting the nodes and the size
    public LinkedList(Node headToCopy, int newSize)
    {
        head = headToCopy;
        size = newSize;
    }

    //Empty constructor, setting head to null and size to 0
    public LinkedList()
    {
        head = null;
        size = 0;
    }

    /**
     * Method which returns a new copy of the LinkedList
     * @return a new LinkedList copy
     */
    public LinkedList copy()
    {
        //define a new LinkedList to return
        LinkedList copy;

        //If head is null, return a new empty list
        if(head == null)
            return new LinkedList();
        else
        {
            //Create a node equal to the original head
            Node next = head;
            /*Create a new linked list via the constructing by passing a new node with the same value as the original
            head's value, together with the original list's size*/
            copy = new LinkedList(new Node(next.getDataValue()), size);
            //Variable new_head which points to the newly created head
            Node new_head = copy.getHead();

            //While there is a next node in the original list
            while(next.getNextNode() != null)
            {
                //Set the next node to the value of the current node in the original list
                new_head.setNextNode(new Node(next.getNextNode().getDataValue()));

                //Go to the next node of the original list
                next = next.getNextNode();
                //Go to the next node of the newly created list
                new_head = new_head.getNextNode();
            }

            //Return the new list
            return copy;
        }
    }

    /**
     * Method which returns the size of the list
     * @return size of list
     */
    @Override
    public int size()
    {
        return size;
    }

    /**
     * Method which returns if he list is empty or not
     * @return true if empty, false if not
     */
    @Override
    public boolean isEmpty()
    {
        //If head is not null, return false
        if(head !=null)
            return false;

        //else return true
        return true;
    }

    /**
     * Method which checks if the list contains a specific object
     * @param o the object to check if it is in the list
     * @return
     */
    @Override
    public boolean contains(Object o)
    {
        //set node equal to current head
        Node next = head;

        //while is current node is not null
        while(next != null)
        {
            //if this node's value equals the value passed, return true
            if(next.getDataValue().equals(o))
                return true;

            //get next node
            next = next.getNextNode();
        }

        //if not found return false
        return false;
    }

    /**
     * Method which returns the ehad of the list
     * @return
     */
    public Node getHead()
    {
        return this.head;
    }

    /**
     * Method to add node to list
     * @param e an element to add
     * @return a new linked list with the node added
     */
    @Override
    public LinkedList add(E e)
    {
        //Copy the original list into a new linked list to return
        LinkedList list = this.copy();
        //get the head of the newly created list
        Node new_head = list.getHead();
        //set a variable to hold the newly created head
        Node next = new_head;

        //copy the original size
        int size = list.size();

        //If new_head is null, it means that the original list was empty
        if(new_head == null)
        {
            //set new_head
            new_head = new Node(e);
            //increment size
            size++;
            //return a new linked list with the added head and the updated size
            return new LinkedList(new_head, size);
        }
        else
        {

            //find the node with a free next node
            while(next.getNextNode() != null)
            {
                next = next.getNextNode();
            }

            //set the free node to a new node with the passed value
            next.setNextNode(new Node(e));
            //increment the size
            size++;
            //return a new linked list with the added head and the updated size
            return new LinkedList(new_head, size);
        }
    }

    /**
     * Method to add a collection of values to the linked list
     * @param c a collection of objects to add to the list
     * @return a new LinkedList with the new values added to the list
     */
    @Override
    public LinkedList addAll(Collection<? extends E> c)
    {
        // change the collection to an array
        Object[] to_add = c.toArray();
        // copy the current list
        LinkedList new_list = this.copy();

        //Loop through the elements in the array and add each element
        for(int i=0; i < to_add.length; i++)
            new_list = new_list.add((E)to_add[i]);

        // Return the new list with the added values
        return new_list;
    }

    /**
     * Method to remove an object from a the list
     * @param o object to be removed
     * @return  Return a list with the removed element
     */
    @Override
    public LinkedList remove(Object o) throws ValueNotFoundException {
        //Copy the original list into a new linked list to return
        LinkedList list = this.copy();
        //get the head of the newly created list
        Node new_head = list.getHead();
        //set a variable to hold the newly created head
        Node next = new_head;
        //copy the original size
        int size = list.size();

        //If the copied head is null, throw a null pointer exception
        if(new_head == null)
            throw new ValueNotFoundException("Element not found in list");

        //If the copied head's data matches the object value to be removed
        if(new_head.getDataValue().equals(o))
        {
            //set the head to next element
            new_head = new_head.getNextNode();
            //decrement size
            size--;
            //return new list
            return new LinkedList(new_head, size);
        }
        //else, if the head does not match
        else
        {
            //set next to the copied head
            next = new_head;

            //Loop through all the nodes until the value which matches the one passed is found
            while(next.getNextNode() != null)
            {
                //If it matches
                if(next.getNextNode().getDataValue().equals(o))
                {
                    //Get the node after the node to be removed
                    Node to_place = next.getNextNode().getNextNode();
                    //Get the node to be removed
                    Node to_remove = next.getNextNode();
                    //Set the next node to the one after the one to be removed, hence removing the next node
                    next.setNextNode(to_place);
                    //set the element to be removed to null to be garbage collected
                    to_remove = null;
                    size--;

                    //return the new list
                    return new LinkedList(new_head, size);
                }

                //Go to the next node
                next = next.getNextNode();
            }

            //If not found, throw a null pointer exception
            throw new ValueNotFoundException("Element not found in list");
        }

    }

    /**
     * Method to return a new LinkedList with the elements passed removed
     * @param collection, a collection to be removed
     * @return a LinkedList without the values passed
     */
    @Override
    public LinkedList removeAll(Collection<?> collection) throws ValueNotFoundException
    {
        // change the collection to an array
        Object[] to_remove = collection.toArray();
        // copy the current list
        LinkedList new_list = this.copy();

        //Loop through the elements in the array and remove each element
        for(int i=0; i < to_remove.length; i++)
            new_list = new_list.remove((E)to_remove[i]);

        // Return the new list with the added values
        return new_list;
    }

    /**
     * A method which returns teh value at the passed index
     * @param index the index of the element to get
     * @return The value found at that index
     */
    @Override
    public E get(int index) throws IndexOutOfBoundsException
    {
        //If bigger than size, throw index out of bounds exception
        if(index > size -1)
        {
            throw new IndexOutOfBoundsException();
        }


        //else, loop until you find the required element and get return its data
        Node current_node = head;

        for(int counter = 0; counter < index; counter++)
        {
            current_node = current_node.getNextNode();
        }

        return (E) current_node.getDataValue();

    }

    /**
     * Method to get the index of a specific value
     * @param o the object to check its index
     * @return an integer showing the index of the specific value
     */
    @Override
    public int indexOf(Object o)
    {
        //set a node variable pointing to the head
        Node next = head;

        //set a counter
        int counter = 0;
        //loop through the list until the value is found
        while(next != null)
        {
            //If equal return the counter
            if(next.getDataValue().equals(o))
                return counter;

            //set next to point to the next node
            next = next.getNextNode();
            //increment counter
            counter++;
        }

        //if not found, return -1
        return -1;
    }

    /**
     * Method to clear items from the list
     * @return new empty list
     */
    @Override
    public LinkedList clearList()
    {
        //set head to null to clear the items
        head = null;
        //set size to 0
        size = 0;
        return new LinkedList();
    }

    /**
     * Method to show print the list contents
     */
    public void showList()
    {
        //if head is null, print that the list is empty
        if(head == null)
        {
            System.out.println("List is empty");
        }
        else
        {
            //set node next to point to head
            Node next = head;

            //loop through all elements until no next node is found
            while(next != null)
            {
                //print value
                System.out.println(next.getDataValue());
                //set next to point to the next node
                next = next.getNextNode();
            }
        }
        System.out.println("List size: "+size);
        System.out.println("------- END OF LIST ----------");
    }

    /**
     * Method to change the linked lst into a string literal
     * @return A string literal representing the linked list
     */
    public String toString()
    {
        String to_return = "[";
        //if head is null, print that the list is empty
        if(head == null)
        {
            return "[]";
        }
        else
        {
            //set node next to point to head
            Node next = head;

            //loop through all elements until no next node is found
            while(next != null)
            {
                //Add value to string
                to_return += next.getDataValue() + ", ";
                //set next to point to the next node
                next = next.getNextNode();
            }
        }

        //Remove last comma and space
        to_return = to_return.substring(0, to_return.length()-2);
        //Close the brackets
        to_return += "]";
        //return the string
        return to_return;
    }



}
