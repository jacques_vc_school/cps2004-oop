package immutableList;

class Node
{
    //Next node of linkedList
    protected Node next;
    //Data of this node
    protected Object data;

    //constructor
    protected Node(Object val)
    {
        next = null;
        data = val;
    }

    //method to get next node
    protected Node getNextNode()
    {
        return next;
    }

    //method to set next node
    protected void setNextNode(Node nextNode)
    {
        next = nextNode;
    }

    //get a node's data
    protected Object getDataValue()
    {
        return data;
    }

}
