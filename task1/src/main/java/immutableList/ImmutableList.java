package immutableList;

import exceptions.ValueNotFoundException;

import java.util.Collection;

interface ImmutableList<E>
{
    /**
     * Method which adds an element to the list
     * @param e an element to add
     * @return true if successful
     */
    public ImmutableList add(E e);

    /**
     * Method to add a collection of objects to the list
     * @param c a collection of objects to add to the list
     * @return true if successfull
     */
    public ImmutableList addAll(Collection<? extends E> c);

    /**
     * Method to remove an object from the list
     * @param o object to be removed
     * @return true if successful
     */
    public ImmutableList remove(Object o) throws ValueNotFoundException;

    /**
     * A method which returns the value at the passed index
     * @param index the index of the element to get
     * @return The value found at that index
     */
    public E get(int index) throws IndexOutOfBoundsException;

    /**
     * Method to remove a collection of items from the list
     * @param collection, a collection to be removed
     * @return a new LinkedList with the elements passed removed
     */
    public ImmutableList removeAll(Collection<?> collection) throws ValueNotFoundException;

    /**
     * Method to remove all elements in the list
     * @return new empty list
     */
    public ImmutableList clearList();

    /**
     * Method to return the size of the list
     * @return the size of the list
     */
    public int size();

    /**
     * Method which returns if the list is empty
     * @return true if empty
     */
    public boolean isEmpty();

    /**
     * Method which checks if the passed object is present in the list
     * @param o the object to check if it is in the list
     * @return true if the list contains the object
     */
    public boolean contains(Object o);

    /**
     * Method which returns the index of the object passed
     * @param o the object to check its index
     * @return the index of the passed object
     */
    public int indexOf(Object o);

    /**
     * Method which copies the list
     * @return a copy of the immutable list
     */
    public ImmutableList copy();


}
