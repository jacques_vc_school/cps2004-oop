import immutableList.LinkedList;
import exceptions.ValueNotFoundException;

public class Launcher {

    public static void main(String args[]) throws InterruptedException, ValueNotFoundException
    {
        LinkedList<Integer> original = new LinkedList<Integer>();
        original = original.add(1).add(2);


        final LinkedList<Integer>[] linkedListThreads = new LinkedList[4];
        linkedListThreads[0] = original.copy();
        linkedListThreads[1] = original.copy();
        linkedListThreads[2] = original.copy();
        linkedListThreads[3] = original.copy();

        Thread thread1 = new Thread() {
            public void run() {
                linkedListThreads[0] = linkedListThreads[0].add(3).add(4).add(5);
                linkedListThreads[0].showList();
            }
        };

        Thread thread2 = new Thread() {
            public void run() {
                linkedListThreads[1] = linkedListThreads[1].add(5).add(6);
            }
        };

        Thread thread3 = new Thread() {
            public void run() {
                try {
                    linkedListThreads[2] = linkedListThreads[2].remove(1);
                }catch (ValueNotFoundException e)
                {
                    System.out.println("Cannot remove value which does not exist");
                }
            }
        };

        Thread thread4 = new Thread() {
            public void run() {
                linkedListThreads[3] = linkedListThreads[3].clearList();
            }
        };

        thread1.start();
        thread1.join();
        thread2.start();
        thread2.join();
        thread3.start();
        thread3.join();
        thread4.start();
        thread4.join();

        System.out.println("Original list should remain the same with values 1 and 2");
        System.out.println("Actual list result: "+original.toString());
        System.out.println("Thread 1 list should have added 3, 4 and 5 to 1 and 2");
        System.out.println("Actual Thread 1 list result: "+linkedListThreads[0].toString());
        System.out.println("Thread 2 list should have added 5 and 6 to 1 and 2");
        System.out.println("Actual Thread 2 list result: "+linkedListThreads[1].toString());
        System.out.println("Thread 3 list should have removed 1");
        System.out.println("Actual Thread 3 list result: "+linkedListThreads[2].toString());
        System.out.println("Thread 4 list should have no items");
        System.out.println("Actual Thread 4 list result: "+linkedListThreads[3].toString());

    }
}